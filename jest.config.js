module.exports = {
  preset: "jest-preset-angular",
  globalSetup: "jest-preset-angular/global-setup",
  moduleNameMapper: {
    "^@app/(.*)": "<rootDir>/src/app/$1",
    "^@model/(.*)": "<rootDir>/src/app/model/$1",
    "^@services/(.*)": "<rootDir>/src/app/services/$1",
    "^@util/(.*)": "<rootDir>/src/app/util/$1",
    "^@components/(.*)": "<rootDir>/src/app/components/$1",
    "^lodash-es$": "lodash",
  },
};
