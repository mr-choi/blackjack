import { PlayerHandLinkFactoryService } from './player-hand-link-factory.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { PlayersService } from '@services/config/players.service';
import { CPUPlayer, HumanPlayer, Player } from '@model/player';
import { PlayerHandLinkItem } from '@model/player-hand-link';

describe('PlayerHandLinkFactoryService', () => {
  MockInstance.scope();

  beforeEach(() => MockBuilder(PlayerHandLinkFactoryService));

  it('should create player-hand link chain correctly', () => {
    const player1 = new HumanPlayer('Player 1', 100);
    const player2 = new CPUPlayer();
    MockInstance(
      PlayersService,
      'getPlayers',
      jest.fn().mockReturnValue([player1, player2]),
    );
    const playerHandLinkFactoryService = ngMocks.get(
      PlayerHandLinkFactoryService,
    );
    const playerHandLink = playerHandLinkFactoryService.create();
    let playerHandLinkItem: PlayerHandLinkItem | null =
      playerHandLink.firstLinkItem;

    expect(playerHandLinkItem.getPlayer()).toBe(player1);

    playerHandLinkItem = playerHandLinkItem.getNext();
    expect(playerHandLinkItem).toBeTruthy();
    expect(playerHandLinkItem?.getPlayer()).toBe(player2);

    playerHandLinkItem = playerHandLinkItem?.getNext() ?? null;
    expect(playerHandLinkItem).toBeTruthy();
    expect(playerHandLinkItem?.getPlayer()).toBe(Player.BANK);

    expect(playerHandLinkItem?.getNext()).toBeNull();
    expect(playerHandLinkItem).toBe(playerHandLink.bankLinkItem);
  });
});
