import { Injectable } from '@angular/core';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Move } from '@model/move';
import { Hand } from '@model/hand';
import { HumanPlayer, Player } from '@model/player';

@Injectable({
  providedIn: 'root',
})
export class AllowedMovesService {
  constructor(private blackjackSettingsService: BlackjackSettingsService) {}

  getAllowedMoves(
    currentLinkItem: PlayerHandLinkItem,
    fullPlayerHandLink: PlayerHandLink,
  ): Set<Move> {
    const player = currentLinkItem.getPlayer();
    const hand = currentLinkItem.getHand();
    const bet = currentLinkItem.getBet();

    // For blackjack, only stand is allowed
    if (hand.isBlackjack()) {
      return new Set<Move>([Move.STAND]);
    }

    const allowedMoves = new Set<Move>([Move.HIT]); // Hit is always allowed otherwise (although not always smart)
    if (this.standAllowed(hand)) allowedMoves.add(Move.STAND);
    if (this.doubleDownAllowed(player, hand, bet))
      allowedMoves.add(Move.DOUBLE_DOWN);
    if (this.splitAllowed(player, hand, bet, fullPlayerHandLink))
      allowedMoves.add(Move.SPLIT);
    if (this.surrenderAllowed(hand)) allowedMoves.add(Move.SURRENDER);

    return allowedMoves;
  }

  private standAllowed(hand: Hand): boolean {
    return hand.getCards().length >= 2;
  }

  private doubleDownAllowed(player: Player, hand: Hand, bet: number): boolean {
    if (player instanceof HumanPlayer && player.balance < bet) return false;
    return this.blackjackSettingsService
      .getSettings()
      .doubleDownRule.doubleDownAllowed(hand);
  }

  private splitAllowed(
    player: Player,
    hand: Hand,
    bet: number,
    fullPlayerHandLink: PlayerHandLink,
  ): boolean {
    if (player instanceof HumanPlayer && player.balance < bet) return false;
    const numberOfHands: number =
      fullPlayerHandLink.getAllHandsByPlayer().get(player)?.length ?? 0;
    if (
      !numberOfHands ||
      numberOfHands >=
        this.blackjackSettingsService.getSettings().maxHandsPerPlayer
    ) {
      return false;
    }
    return this.blackjackSettingsService
      .getSettings()
      .splitRule.splitAllowed(hand);
  }

  private surrenderAllowed(hand: Hand) {
    return (
      this.blackjackSettingsService.getSettings().allowSurrender &&
      hand.getCards().length === 2 &&
      !hand.split
    );
  }
}
