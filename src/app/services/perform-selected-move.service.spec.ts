import { PerformSelectedMoveService } from './perform-selected-move.service';
import { MockBuilder, MockInstance, MockService, ngMocks } from 'ng-mocks';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Hand } from '@model/hand';
import { Player } from '@model/player';
import { CardDrawService } from '@services/card-draw.service';
import { runMarbleTest } from '@util/test/run-marble-test';
import { PlayCard } from '@model/play-card';
import { AllowedMovesService } from '@services/allowed-moves.service';
import { Move } from '@model/move';
import { DetermineMoveVisitorService } from '@services/determine-move/determine-move-visitor.service';

describe('PerformSelectedMove', () => {
  MockInstance.scope();

  let hand: Hand;
  let bankLinkItem: PlayerHandLinkItem;
  let entirePlayerHandLink: PlayerHandLink;

  beforeEach(() => {
    hand = new Hand(false);
    bankLinkItem = new PlayerHandLinkItem(Player.BANK, hand, null);
    entirePlayerHandLink = new PlayerHandLink(bankLinkItem, bankLinkItem);
    return MockBuilder(PerformSelectedMoveService);
  });

  it('should automatically perform move if it is the only valid option', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      const mockMove = MockService(Move, {
        perform$: jest
          .fn()
          .mockReturnValueOnce(cold('-a', { a: true }))
          .mockReturnValueOnce(cold('b', { b: false })),
      });
      MockInstance(
        AllowedMovesService,
        'getAllowedMoves',
        jest.fn().mockReturnValue(new Set([mockMove])),
      );
      MockInstance(CardDrawService, 'deckId$', cold('(a|)', { a: 'abc123' }));
      MockInstance(
        CardDrawService,
        'drawCardFromDeck$',
        jest.fn().mockReturnValue(
          cold('--(a|)', {
            a: PlayCard.createFromJson({ image: '2', value: '2' }),
          }),
        ),
      );

      const performSelectedMoveService = ngMocks.get(
        PerformSelectedMoveService,
      );

      const expected$ = cold('-(a|)', { a: true });
      const actual$ = performSelectedMoveService.perform$(
        bankLinkItem,
        entirePlayerHandLink,
      );

      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should perform determined move if there are multiple allowed moves', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      const mockMove = MockService(Move, {
        perform$: jest
          .fn()
          .mockReturnValueOnce(cold('--a', { a: true }))
          .mockReturnValueOnce(cold('b', { b: false })),
      });
      MockInstance(
        AllowedMovesService,
        'getAllowedMoves',
        jest.fn().mockReturnValue(new Set([mockMove, Move.HIT, Move.STAND])),
      );
      MockInstance(
        DetermineMoveVisitorService,
        'visitBank',
        jest.fn().mockReturnValue(cold('-a', { a: mockMove })),
      );
      MockInstance(CardDrawService, 'deckId$', cold('(a|)', { a: 'abc123' }));
      MockInstance(
        CardDrawService,
        'drawCardFromDeck$',
        jest.fn().mockReturnValue(
          cold('--(a|)', {
            a: PlayCard.createFromJson({ image: '2', value: '2' }),
          }),
        ),
      );

      const performSelectedMoveService = ngMocks.get(
        PerformSelectedMoveService,
      );

      const expected$ = cold('---a|', { a: true });
      const actual$ = performSelectedMoveService.perform$(
        bankLinkItem,
        entirePlayerHandLink,
      );

      expectObservable(actual$).toEqual(expected$);
    });
  });
});
