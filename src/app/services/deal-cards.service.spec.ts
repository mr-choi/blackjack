import { DealCardsService } from './deal-cards.service';
import { MockBuilder, MockInstance, MockService, ngMocks } from 'ng-mocks';
import { runMarbleTest } from '@util/test/run-marble-test';
import { CardDrawService } from '@services/card-draw.service';
import { PlayCard } from '@model/play-card';
import { BetsByPlayerService } from '@services/bets-by-player.service';
import { CPUPlayer, HumanPlayer, Player } from '@model/player';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Hand, MINIMUM_DELAY_PENDING_CARD } from '@model/hand';
import { EMPTY, map, of } from 'rxjs';
import { cloneDeep } from 'lodash-es';
import { RunHelpers } from 'rxjs/internal/testing/TestScheduler';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import { PlayersService } from '@services/config/players.service';

describe('DealCardsService', () => {
  MockInstance.scope();

  const player = MockService(HumanPlayer);

  let card1: PlayCard;
  let card2: PlayCard;
  let card3: PlayCard;
  let card4: PlayCard;
  let card5: PlayCard;
  let card6: PlayCard;

  let bankLink: PlayerHandLinkItem;
  let cpuLink: PlayerHandLinkItem;
  let playerLink: PlayerHandLinkItem;
  let playerHandLink: PlayerHandLink;

  beforeEach(() => {
    card1 = PlayCard.createFromJson({ image: 'ACE', value: 'ACE' });
    card2 = PlayCard.createFromJson({ image: '2', value: '2' });
    card3 = PlayCard.createFromJson({ image: '3', value: '3' });
    card4 = PlayCard.createFromJson({ image: '4', value: '4' });
    card5 = PlayCard.createFromJson({ image: '5', value: '5' });
    card6 = PlayCard.createFromJson({ image: '6', value: '6' });

    bankLink = new PlayerHandLinkItem(Player.BANK, new Hand(false), null);
    cpuLink = new PlayerHandLinkItem(
      new CPUPlayer(),
      new Hand(false),
      bankLink,
    );
    playerLink = new PlayerHandLinkItem(player, new Hand(false), cpuLink);
    playerHandLink = new PlayerHandLink(playerLink, bankLink);

    MockInstance(CardDrawService, 'deckId$', EMPTY);
    MockInstance(CardDrawService, 'drawCardFromDeck$', jest.fn());
    MockInstance(PlayersService, 'players$', of([player]));

    return MockBuilder(DealCardsService);
  });

  it('should allow card deal if all human players have a bet', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: new Map<HumanPlayer, number | null>([[player, 10]]) }),
      );

      const dealCardsService = ngMocks.get(DealCardsService);
      const expected$ = cold('a', {
        a: true,
      });
      const actual$ = dealCardsService.canDealCards$;
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should allow cpu players only', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: new Map<HumanPlayer, number | null>() }),
      );
      MockInstance(PlayersService, 'players$', of([new CPUPlayer()]));

      const dealCardsService = ngMocks.get(DealCardsService);
      const expected$ = cold('a', {
        a: true,
      });
      const actual$ = dealCardsService.canDealCards$;
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should not allow card deal if human player has no bet', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: new Map<HumanPlayer, number | null>([[player, null]]) }),
      );

      const dealCardsService = ngMocks.get(DealCardsService);
      const expected$ = cold('a', {
        a: false,
      });
      const actual$ = dealCardsService.canDealCards$;
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should not allow card deal if a there are no players', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: new Map<HumanPlayer, number | null>() }),
      );
      MockInstance(PlayersService, 'players$', of([]));

      const dealCardsService = ngMocks.get(DealCardsService);
      const expected$ = cold('a', {
        a: false,
      });
      const actual$ = dealCardsService.canDealCards$;
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should deal cards correctly, given no hole card for bank', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(CardDrawService, 'deckId$', cold('(a|)', { a: 'abc123' }));
      MockInstance(
        CardDrawService,
        'drawCardFromDeck$',
        jest
          .fn()
          .mockReturnValueOnce(cold('--(a|)', { a: card1 }))
          .mockReturnValueOnce(cold('----(a|)', { a: card2 }))
          .mockReturnValueOnce(
            cold(`${2 * MINIMUM_DELAY_PENDING_CARD}ms (a|)`, { a: card3 }),
          )
          .mockReturnValueOnce(cold('------(a|)', { a: card4 }))
          .mockReturnValueOnce(cold('-(a|)', { a: card5 })),
      );
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: new Map<HumanPlayer, number | null>([[player, 10]]) }),
      );
      MockInstance(
        BlackjackSettingsService,
        'getSettings',
        jest.fn().mockReturnValue({ dealerDrawsHoleCard: false }),
      );
      MockInstance(
        CardDrawService,
        'putCardsBackToDeck$',
        jest.fn().mockReturnValue(cold('(a|)')),
      );

      assertHumanPlayerHand(playerLink.getHand(), cold, expectObservable);
      assertCPUPlayerHand(cpuLink.getHand(), cold, expectObservable);
      assertBankHandNoHoleCard(bankLink.getHand(), cold, expectObservable);

      const dealCardsService = ngMocks.get(DealCardsService);
      const expected$ = cold(`${6 * MINIMUM_DELAY_PENDING_CARD}ms (a|)`, {
        a: playerHandLink,
      });
      const actual$ = dealCardsService.dealCards$(playerHandLink);
      expectObservable(actual$).toEqual(expected$);
    });

    const cardDrawService = ngMocks.get(CardDrawService);
    expect(cardDrawService.drawCardFromDeck$).toHaveBeenCalledTimes(5);
    expect(cardDrawService.drawCardFromDeck$).toHaveBeenCalledWith('abc123');
    expect(cardDrawService.putCardsBackToDeck$).toHaveBeenCalledWith('abc123');
  });

  it('should deal cards correctly, given bank draws hole card', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(CardDrawService, 'deckId$', cold('(a|)', { a: 'abc123' }));
      MockInstance(
        CardDrawService,
        'drawCardFromDeck$',
        jest
          .fn()
          .mockReturnValueOnce(cold('--(a|)', { a: card1 }))
          .mockReturnValueOnce(cold('----(a|)', { a: card2 }))
          .mockReturnValueOnce(
            cold(`${2 * MINIMUM_DELAY_PENDING_CARD}ms (a|)`, { a: card3 }),
          )
          .mockReturnValueOnce(cold('------(a|)', { a: card4 }))
          .mockReturnValueOnce(cold('-(a|)', { a: card5 }))
          .mockReturnValueOnce(
            cold(`${MINIMUM_DELAY_PENDING_CARD}ms (a|)`, { a: card6 }),
          ),
      );
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: new Map<HumanPlayer, number | null>([[player, 25]]) }),
      );
      MockInstance(
        BlackjackSettingsService,
        'getSettings',
        jest.fn().mockReturnValue({ dealerDrawsHoleCard: true }),
      );
      MockInstance(
        CardDrawService,
        'putCardsBackToDeck$',
        jest.fn().mockReturnValue(cold('(a|)')),
      );

      assertHumanPlayerHand(playerLink.getHand(), cold, expectObservable);
      assertCPUPlayerHand(cpuLink.getHand(), cold, expectObservable);
      assertBankHandWithHoleCard(bankLink.getHand(), cold, expectObservable);

      const dealCardsService = ngMocks.get(DealCardsService);
      const expected$ = cold(`${7 * MINIMUM_DELAY_PENDING_CARD}ms (a|)`, {
        a: playerHandLink,
      });
      const actual$ = dealCardsService.dealCards$(playerHandLink);
      expectObservable(actual$).toEqual(expected$);
    });

    const cardDrawService = ngMocks.get(CardDrawService);
    expect(cardDrawService.drawCardFromDeck$).toHaveBeenCalledTimes(6);
    expect(cardDrawService.drawCardFromDeck$).toHaveBeenCalledWith('abc123');
    expect(cardDrawService.putCardsBackToDeck$).toHaveBeenCalledWith('abc123');
  });

  function assertHumanPlayerHand(
    hand: Hand,
    cold: RunHelpers['cold'],
    expectObservable: RunHelpers['expectObservable'],
  ) {
    const expectedCardsPlayer$ = cold(
      `a ${MINIMUM_DELAY_PENDING_CARD - 1}ms b ${
        4 * MINIMUM_DELAY_PENDING_CARD - 1
      }ms c`,
      { a: [], b: [card1], c: [card1, card4] },
    );
    const actualCardsPlayer$ = hand.cards$.pipe(map(cloneDeep));
    expectObservable(actualCardsPlayer$).toEqual(expectedCardsPlayer$);

    // Note: rxjs treats (ba) as 4 frames instead of 1.
    const expectedPendingPlayer$ = cold(
      `(ba) ${MINIMUM_DELAY_PENDING_CARD - 4}ms b ${
        3 * MINIMUM_DELAY_PENDING_CARD - 1
      }ms a ${MINIMUM_DELAY_PENDING_CARD - 1}ms b`,
      { a: true, b: false },
    );
    const actualPendingPlayer$ = hand.newCardPending$;
    expectObservable(actualPendingPlayer$).toEqual(expectedPendingPlayer$);
  }

  function assertCPUPlayerHand(
    hand: Hand,
    cold: RunHelpers['cold'],
    expectObservable: RunHelpers['expectObservable'],
  ) {
    const expectedCardsCPU$ = cold(
      `a ${2 * MINIMUM_DELAY_PENDING_CARD - 1}ms b ${
        4 * MINIMUM_DELAY_PENDING_CARD - 1
      }ms c`,
      { a: [], b: [card2], c: [card2, card5] },
    );
    const actualCardsCPU$ = hand.cards$.pipe(map(cloneDeep));
    expectObservable(actualCardsCPU$).toEqual(expectedCardsCPU$);

    const expectedPendingCPU$ = cold(
      `b ${MINIMUM_DELAY_PENDING_CARD - 1}ms a ${
        MINIMUM_DELAY_PENDING_CARD - 1
      }ms b ${3 * MINIMUM_DELAY_PENDING_CARD - 1}ms a ${
        MINIMUM_DELAY_PENDING_CARD - 1
      }ms b`,
      { a: true, b: false },
    );
    const actualPendingCPU$ = hand.newCardPending$;
    expectObservable(actualPendingCPU$).toEqual(expectedPendingCPU$);
  }

  function assertBankHandNoHoleCard(
    hand: Hand,
    cold: RunHelpers['cold'],
    expectObservable: RunHelpers['expectObservable'],
  ) {
    const expectedCardsBank$ = cold(
      `a ${4 * MINIMUM_DELAY_PENDING_CARD - 1}ms b`,
      { a: [], b: [card3] },
    );
    const actualCardsBank$ = hand.cards$.pipe(map(cloneDeep));
    expectObservable(actualCardsBank$).toEqual(expectedCardsBank$);

    const expectedPendingBank$ = cold(
      `b ${2 * MINIMUM_DELAY_PENDING_CARD - 1}ms a ${
        2 * MINIMUM_DELAY_PENDING_CARD - 1
      }ms b`,
      { a: true, b: false },
    );
    const actualPendingBank$ = hand.newCardPending$;
    expectObservable(actualPendingBank$).toEqual(expectedPendingBank$);
  }

  function assertBankHandWithHoleCard(
    hand: Hand,
    cold: RunHelpers['cold'],
    expectObservable: RunHelpers['expectObservable'],
  ) {
    const expectedCardsBank$ = cold(
      `a ${4 * MINIMUM_DELAY_PENDING_CARD - 1}ms b`,
      { a: [], b: [card3] },
    );
    const actualCardsBank$ = hand.cards$.pipe(map(cloneDeep));
    expectObservable(actualCardsBank$).toEqual(expectedCardsBank$);

    const expectedHasHoleCard$ = cold(
      `b ${7 * MINIMUM_DELAY_PENDING_CARD - 1}ms a`,
      { a: true, b: false },
    );
    const actualHasHoleCard$ = hand.hasHoleCard$;
    expectObservable(actualHasHoleCard$).toEqual(expectedHasHoleCard$);

    const expectedPendingBank$ = cold(
      `b ${2 * MINIMUM_DELAY_PENDING_CARD - 1}ms a ${
        2 * MINIMUM_DELAY_PENDING_CARD - 1
      }ms b ${2 * MINIMUM_DELAY_PENDING_CARD - 1}ms a ${
        MINIMUM_DELAY_PENDING_CARD - 1
      }ms b`,
      { a: true, b: false },
    );
    const actualPendingBank$ = hand.newCardPending$;
    expectObservable(actualPendingBank$).toEqual(expectedPendingBank$);
  }
});
