import { InsuranceService } from './insurance.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Hand } from '@model/hand';
import { HumanPlayer, Player } from '@model/player';
import { runMarbleTest } from '@util/test/run-marble-test';
import { PlayCard } from '@model/play-card';
import { filter, map, switchMap, take, tap } from 'rxjs';
import { isNull } from 'lodash-es';

describe('InsuranceService', () => {
  MockInstance.scope();

  let bankLinkItem: PlayerHandLinkItem;
  let bankHand: Hand;
  let player: HumanPlayer;
  let playerLinkItem: PlayerHandLinkItem;
  let playerHandLink: PlayerHandLink;

  beforeEach(() => {
    bankHand = new Hand(false);
    bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);
    player = new HumanPlayer('Player 1', 1000);
    playerLinkItem = new PlayerHandLinkItem(
      player,
      new Hand(false),
      bankLinkItem,
    );
    playerHandLink = new PlayerHandLink(playerLinkItem, bankLinkItem);

    return MockBuilder(InsuranceService);
  });

  it('should not ask for insurance if the bank does not have ace', () => {
    bankHand.addCard(PlayCard.createFromJson({ value: '4', image: '4' }));
    runMarbleTest(({ cold, expectObservable }) => {
      const insuranceService = ngMocks.get(InsuranceService);

      const expectedDetermineInsuranceObject$ = cold('a', { a: null });
      const actualDetermineInsuranceObject$ =
        insuranceService.determineInsuranceObject$;

      const determineInsurance$ =
        insuranceService.determineInsuranceIfApplicable$(playerHandLink);

      expectObservable(determineInsurance$);
      expectObservable(actualDetermineInsuranceObject$).toEqual(
        expectedDetermineInsuranceObject$,
      );
    });

    expect(player.balance).toBe(1000);
    expect(playerLinkItem.isInsured()).toBe(false);
  });

  it('should not ask for insurance player cannot afford it', () => {
    bankHand.addCard(PlayCard.createFromJson({ value: 'ACE', image: 'ACE' }));
    player.balance = 9;
    playerLinkItem.setBet(20);

    runMarbleTest(({ cold, expectObservable }) => {
      const insuranceService = ngMocks.get(InsuranceService);

      const expectedDetermineInsuranceObject$ = cold('a', { a: null });
      const actualDetermineInsuranceObject$ =
        insuranceService.determineInsuranceObject$;

      const determineInsurance$ =
        insuranceService.determineInsuranceIfApplicable$(playerHandLink);

      expectObservable(determineInsurance$);
      expectObservable(actualDetermineInsuranceObject$).toEqual(
        expectedDetermineInsuranceObject$,
      );
    });

    expect(player.balance).toBe(9);
    expect(playerLinkItem.isInsured()).toBe(false);
  });

  it('should not insure if player says no', () => {
    bankHand.addCard(PlayCard.createFromJson({ value: 'ACE', image: 'ACE' }));
    playerLinkItem.setBet(10);

    runMarbleTest(({ cold, expectObservable }) => {
      const insuranceService = ngMocks.get(InsuranceService);

      const expectedPlayerFromDetermineInsuranceObject$ = cold('a-b', {
        a: player,
        b: undefined,
      });
      const actualPlayerFromDetermineInsuranceObject$ =
        insuranceService.determineInsuranceObject$.pipe(
          map((x) => x?.humanPlayer),
        );

      const answerInsuranceQuestion$ =
        insuranceService.determineInsuranceObject$.pipe(
          filter((x) => !isNull(x)),
          take(1),
          switchMap((determineInsuranceObject) =>
            cold('--a').pipe(
              tap(() => determineInsuranceObject?.setWantsInsurance(false)),
            ),
          ),
        );

      const determineInsurance$ =
        insuranceService.determineInsuranceIfApplicable$(playerHandLink);

      expectObservable(determineInsurance$);
      expectObservable(answerInsuranceQuestion$);
      expectObservable(actualPlayerFromDetermineInsuranceObject$).toEqual(
        expectedPlayerFromDetermineInsuranceObject$,
      );
    });

    expect(player.balance).toBe(1000);
    expect(playerLinkItem.isInsured()).toBe(false);
  });

  it('should insure if player says yes', () => {
    bankHand.addCard(PlayCard.createFromJson({ value: 'ACE', image: 'ACE' }));
    playerLinkItem.setBet(10);

    runMarbleTest(({ cold, expectObservable }) => {
      const insuranceService = ngMocks.get(InsuranceService);

      const expectedPlayerFromDetermineInsuranceObject$ = cold('a-b', {
        a: player,
        b: undefined,
      });
      const actualPlayerFromDetermineInsuranceObject$ =
        insuranceService.determineInsuranceObject$.pipe(
          map((x) => x?.humanPlayer),
        );

      const answerInsuranceQuestion$ =
        insuranceService.determineInsuranceObject$.pipe(
          filter((x) => !isNull(x)),
          take(1),
          switchMap((determineInsuranceObject) =>
            cold('--a').pipe(
              tap(() => determineInsuranceObject?.setWantsInsurance(true)),
            ),
          ),
        );

      const determineInsurance$ =
        insuranceService.determineInsuranceIfApplicable$(playerHandLink);

      expectObservable(determineInsurance$);
      expectObservable(answerInsuranceQuestion$);
      expectObservable(actualPlayerFromDetermineInsuranceObject$).toEqual(
        expectedPlayerFromDetermineInsuranceObject$,
      );
    });

    expect(player.balance).toBe(995);
    expect(playerLinkItem.isInsured()).toBe(true);
  });
});
