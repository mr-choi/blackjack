import { Injectable } from '@angular/core';
import { PlayersService } from '@services/config/players.service';
import { HumanPlayer } from '@model/player';
import { BehaviorSubject, filter, map, Observable, share } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BetsByPlayerService {
  private betsByPlayer$$ = new BehaviorSubject(
    new Map<HumanPlayer, number | null>(),
  );

  readonly betsByPLayer$: Observable<Map<HumanPlayer, number | null>> =
    this.playersService.humanPlayers$.pipe(
      map((players) => {
        const playerSet = new Set(players);
        const betsByPlayer = this.betsByPlayer$$.getValue();
        let betsUpdated = false;

        // Remove human players from the bets by player map that do not exist anymore.
        betsByPlayer.forEach((_, player) => {
          if (!playerSet.has(player)) {
            betsByPlayer.delete(player);
            betsUpdated = true;
          }
        });

        // Add new human players.
        playerSet.forEach((humanPlayer) => {
          if (!betsByPlayer.has(humanPlayer)) {
            betsByPlayer.set(humanPlayer, null);
            betsUpdated = true;
          }
        });
        return { betsByPlayer, betsUpdated };
      }),
      // We only let the betsByPlayer subject emit if players are added/removed.
      // Exception is the first emission
      filter(({ betsUpdated }, index) => betsUpdated || index === 0),
      map(({ betsByPlayer }) => betsByPlayer),
      share({
        connector: () => {
          let takeEmissions = false;
          return {
            next: (value) => {
              takeEmissions = true;
              this.betsByPlayer$$.next(value);
            },
            error: (err) => this.betsByPlayer$$.error(err),
            complete: () => this.betsByPlayer$$.complete(),
            subscribe: (observer) =>
              this.betsByPlayer$$
                .pipe(filter(() => takeEmissions))
                .subscribe(observer),
          };
        },
      }),
    );

  constructor(private playersService: PlayersService) {}

  changeBetForPlayer(humanPlayer: HumanPlayer, bet: number | null) {
    const betsByPlayer = this.betsByPlayer$$.getValue();
    if (betsByPlayer.has(humanPlayer)) {
      betsByPlayer.set(humanPlayer, bet);
      this.betsByPlayer$$.next(betsByPlayer);
    }
  }
}
