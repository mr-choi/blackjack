import { Injectable } from '@angular/core';
import { PlayerHandLinkFactoryService } from '@services/player-hand-link-factory.service';
import { DealCardsService } from '@services/deal-cards.service';
import { PerformSelectedMoveService } from '@services/perform-selected-move.service';
import { DetermineResultsAndPayoutService } from '@services/determine-results-and-payout.service';
import {
  BehaviorSubject,
  catchError,
  combineLatest,
  defaultIfEmpty,
  delayWhen,
  distinctUntilChanged,
  filter,
  ignoreElements,
  map,
  MonoTypeOperatorFunction,
  Observable,
  of,
  pipe,
  repeat,
  share,
  switchMap,
  take,
  takeLast,
  takeWhile,
  tap,
  timer,
} from 'rxjs';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { HumanPlayer } from '@model/player';
import { BetsByPlayerService } from '@services/bets-by-player.service';
import { isNull } from 'lodash-es';
import { InsuranceService } from '@services/insurance.service';

export type GamePhase = 'BETTING' | 'PLAYING' | 'FINISHING';

export const DELAY_AFTER_NON_HUMAN_LINK_ITEM = 500;

@Injectable({ providedIn: 'root' })
export class BlackjackGameManagementService {
  private playerHandLink$$ = new BehaviorSubject(
    this.playerHandLinkFactoryService.create(),
  );
  readonly playerHandLink$ = this.playerHandLink$$.asObservable();

  private currentLinkItem$$ = new BehaviorSubject<PlayerHandLinkItem | null>(
    null,
  );
  readonly currentLinkItem$ = this.currentLinkItem$$.pipe(
    distinctUntilChanged(),
  );

  private gamePhase$$ = new BehaviorSubject<GamePhase>('BETTING');
  readonly gamePhase$ = this.gamePhase$$.asObservable();

  game$: Observable<never> = combineLatest({
    gamePhase: this.gamePhase$,
    playerHandLink: this.playerHandLink$,
    canDealCards: this.dealCardsService.canDealCards$,
    betsByPlayer: this.betsByPlayerService.betsByPLayer$,
  })
    .pipe(
      take(1),
      filter(
        ({ canDealCards, gamePhase }) =>
          canDealCards && gamePhase === 'BETTING',
      ),
      tap(({ playerHandLink, betsByPlayer }) => {
        this.gamePhase$$.next('PLAYING');
        this.setBets(playerHandLink, betsByPlayer);
      }),
      switchMap(({ playerHandLink }) =>
        this.dealCardsService.dealCards$(playerHandLink),
      ),
      switchMap((playerHandLink) =>
        this.insuranceService.determineInsuranceIfApplicable$(playerHandLink),
      ),
      switchMap((playerHandLink) => {
        if (playerHandLink.bankLinkItem.getHand().isBlackjack(true)) {
          // This situation can only occur if bank has drawn a hole card.
          // In that case the came will immediately end.
          playerHandLink.bankLinkItem.getHand().revealHoleCard();
          return of(playerHandLink);
        }
        // Otherwise, continue to perform moves for each player.
        return of(playerHandLink).pipe(
          this.forEachLinkItem(
            (
              playerHandLink: PlayerHandLink,
              playerHandLinkItem: PlayerHandLinkItem,
            ) =>
              this.performSelectedMoveService.perform$(
                playerHandLinkItem,
                playerHandLink,
              ),
          ),
        );
      }),
      catchError((error) => this.restoreBalanceAndRethrowError(error)),
      switchMap((playerHandLink) => {
        this.gamePhase$$.next('FINISHING');
        return this.determineResultsAndPayoutService.results$(playerHandLink);
      }),
      ignoreElements(),
    ) // This pipe is cut in 2 because typescript will not work properly for pipes with 10 or more operators.
    .pipe(share({ resetOnComplete: true, resetOnRefCountZero: true }));

  constructor(
    private playerHandLinkFactoryService: PlayerHandLinkFactoryService,
    private dealCardsService: DealCardsService,
    private performSelectedMoveService: PerformSelectedMoveService,
    private determineResultsAndPayoutService: DetermineResultsAndPayoutService,
    private betsByPlayerService: BetsByPlayerService,
    private insuranceService: InsuranceService,
  ) {}

  resetGame() {
    this.currentLinkItem$$.next(null);
    this.playerHandLink$$.next(this.playerHandLinkFactoryService.create());
    this.gamePhase$$.next('BETTING');
  }

  private setBets(
    playerHandLink: PlayerHandLink,
    betsByPlayer: Map<HumanPlayer, number | null>,
  ) {
    let currentLink: PlayerHandLinkItem | null = playerHandLink.firstLinkItem;
    while (currentLink) {
      const player = currentLink.getPlayer();
      if (player instanceof HumanPlayer) {
        const bet = Math.min(betsByPlayer.get(player) ?? 0, player.balance);
        currentLink.setBet(bet);
        player.balance -= bet;
      }
      currentLink = currentLink.getNext();
    }
  }

  private forEachLinkItem(
    callbackObservable$: (
      playerHandLink: PlayerHandLink,
      playerHandLinkItem: PlayerHandLinkItem,
    ) => Observable<unknown>,
  ): MonoTypeOperatorFunction<PlayerHandLink> {
    return pipe(
      take(1),
      tap((playerHandLink) =>
        this.currentLinkItem$$.next(playerHandLink.firstLinkItem),
      ),
      switchMap((playerHandLink) =>
        this.currentLinkItem$
          .pipe(
            take(1),
            filter((x) => !isNull(x)),
            switchMap((currentLinkItem) =>
              callbackObservable$(
                playerHandLink,
                currentLinkItem as PlayerHandLinkItem,
              ),
            ),
            ignoreElements(),
            defaultIfEmpty(null),
            delayWhen(() => this.getDelay()),
            repeat(),
            takeWhile(() => {
              this.currentLinkItem$$.next(
                this.currentLinkItem$$.getValue()?.getNext() ?? null,
              );
              return !!this.currentLinkItem$$.getValue();
            }),
            takeLast(1),
          )
          .pipe(
            // This pipe is cut in 2 because typescript will not work properly for pipes with 10 or more operators.
            map(() => playerHandLink),
          ),
      ),
    );
  }

  private restoreBalanceAndRethrowError(error: unknown): never {
    this.gamePhase$$.next('FINISHING');
    this.currentLinkItem$$.next(null);
    const playerHandLink = this.playerHandLink$$.getValue();
    for (
      let linkItem: PlayerHandLinkItem | null = playerHandLink.firstLinkItem;
      linkItem;
      linkItem = linkItem.getNext()
    ) {
      const player = linkItem.getPlayer();
      if (player instanceof HumanPlayer) {
        player.balance +=
          linkItem.getBet() / (linkItem.hasSurrendered() ? 2 : 1);
        if (linkItem.isInsured()) {
          player.balance += linkItem.getBet() / 2;
        }
      }
    }
    throw error;
  }

  private getDelay(): Observable<0> {
    if (this.currentLinkItem$$.getValue()?.getPlayer().type === 'human') {
      return timer(0);
    }
    return timer(DELAY_AFTER_NON_HUMAN_LINK_ITEM);
  }
}
