import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Move } from '@model/move';
import { Observable } from 'rxjs';

export interface AbstractDetermineMoveService {
  determineMove(
    currentLinkItem: PlayerHandLinkItem,
    allowedMoves: Set<Move>,
    fullPlayerHandLink: PlayerHandLink,
  ): Observable<Move>;
}
