import { Injectable } from '@angular/core';
import { AbstractDetermineMoveService } from '@services/determine-move/abstract-determine-move.service';
import { PlayerHandLinkItem } from '@model/player-hand-link';
import { Move } from '@model/move';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { HumanPlayerDetermineMoveObject } from '@model/human-player-determine-move-object';

@Injectable({
  providedIn: 'root',
})
export class PlayerDetermineMoveService
  implements AbstractDetermineMoveService
{
  private determineMoveObject$$ =
    new BehaviorSubject<HumanPlayerDetermineMoveObject | null>(null);
  readonly determineMoveObject$: Observable<HumanPlayerDetermineMoveObject | null> =
    this.determineMoveObject$$.asObservable();

  determineMove(
    currentLinkItem: PlayerHandLinkItem,
    allowedMoves: Set<Move>,
  ): Observable<Move> {
    const determineMoveObject = new HumanPlayerDetermineMoveObject(
      currentLinkItem,
      allowedMoves,
    );
    this.determineMoveObject$$.next(determineMoveObject);
    return determineMoveObject.determinedMove$.pipe(
      tap(() => this.determineMoveObject$$.next(null)),
    );
  }
}
