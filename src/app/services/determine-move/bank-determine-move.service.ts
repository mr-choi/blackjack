import { Injectable } from '@angular/core';
import { AbstractDetermineMoveService } from '@services/determine-move/abstract-determine-move.service';
import { PlayerHandLinkItem } from '@model/player-hand-link';
import { Move } from '@model/move';
import { Observable, of } from 'rxjs';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import { TotalCardValue } from '@model/total-card-value';

@Injectable({
  providedIn: 'root',
})
export class BankDetermineMoveService implements AbstractDetermineMoveService {
  constructor(private blackjackSettingsService: BlackjackSettingsService) {}

  determineMove(currentLinkItem: PlayerHandLinkItem): Observable<Move> {
    const totalCardValue: TotalCardValue = currentLinkItem
      .getHand()
      .getTotalCardValue();
    if (totalCardValue.getTotal() < 17) return of(Move.HIT);
    if (
      this.blackjackSettingsService.getSettings().dealerHitsSoft17 &&
      totalCardValue.isSoft() &&
      totalCardValue.getTotal() === 17
    ) {
      return of(Move.HIT);
    }
    return of(Move.STAND);
  }
}
