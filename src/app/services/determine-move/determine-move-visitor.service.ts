import { Injectable } from '@angular/core';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Move } from '@model/move';
import { CPUPlayer, HumanPlayer, Player, PlayerVisitor } from '@model/player';
import { Observable } from 'rxjs';
import { BankDetermineMoveService } from '@services/determine-move/bank-determine-move.service';
import { CpuDetermineMoveService } from '@services/determine-move/cpu-determine-move.service';
import { PlayerDetermineMoveService } from '@services/determine-move/player-determine-move.service';

export interface DetermineMoveParameterData {
  currentLinkItem: PlayerHandLinkItem;
  fullPlayerHandLink: PlayerHandLink;
  allowedMoves: Set<Move>;
}

@Injectable({
  providedIn: 'root',
})
export class DetermineMoveVisitorService
  implements PlayerVisitor<DetermineMoveParameterData, Observable<Move>>
{
  constructor(
    private bankDetermineMoveService: BankDetermineMoveService,
    private cpuDetermineMoveService: CpuDetermineMoveService,
    private playerDetermineMoveService: PlayerDetermineMoveService,
  ) {}

  visitBank(
    _: typeof Player.BANK,
    parameter: DetermineMoveParameterData,
  ): Observable<Move> {
    return this.bankDetermineMoveService.determineMove(
      parameter.currentLinkItem,
    );
  }

  visitCPU(
    _: CPUPlayer,
    parameter: DetermineMoveParameterData,
  ): Observable<Move> {
    return this.cpuDetermineMoveService.determineMove(
      parameter.currentLinkItem,
      parameter.allowedMoves,
      parameter.fullPlayerHandLink,
    );
  }

  visitHuman(
    _: HumanPlayer,
    parameter: DetermineMoveParameterData,
  ): Observable<Move> {
    return this.playerDetermineMoveService.determineMove(
      parameter.currentLinkItem,
      parameter.allowedMoves,
    );
  }
}
