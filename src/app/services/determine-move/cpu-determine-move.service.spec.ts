import { CpuDetermineMoveService } from './cpu-determine-move.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { Hand } from '@model/hand';
import { PlayCard } from '@model/play-card';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { CPUPlayer, Player } from '@model/player';
import { Move } from '@model/move';
import { runMarbleTest } from '@util/test/run-marble-test';

describe('CpuDetermineMoveService', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(CpuDetermineMoveService);
  });

  it('should be able to determine correct move for hard total and bad bank hand', () => {
    const bankHand = new Hand(false);
    bankHand.addCard(PlayCard.createFromJson({ image: '6', value: '6' }));
    const bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);

    const cpuHand = new Hand(false);
    cpuHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    cpuHand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));
    const playerLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      cpuHand,
      bankLinkItem,
    );

    const entireLink = new PlayerHandLink(playerLinkItem, bankLinkItem);
    const allowedMoves = new Set<Move>([Move.HIT, Move.STAND]);

    const cpuDetermineMoveService = ngMocks.get(CpuDetermineMoveService);

    runMarbleTest(({ cold, expectObservable }) => {
      const expected$ = cold('(a|)', { a: Move.STAND });
      const actual$ = cpuDetermineMoveService.determineMove(
        playerLinkItem,
        allowedMoves,
        entireLink,
      );
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should be able to determine correct move for hard total and good bank hand', () => {
    const bankHand = new Hand(false);
    bankHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    const bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);

    const cpuHand = new Hand(false);
    cpuHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    cpuHand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));
    const playerLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      cpuHand,
      bankLinkItem,
    );

    const entireLink = new PlayerHandLink(playerLinkItem, bankLinkItem);
    const allowedMoves = new Set<Move>([Move.HIT, Move.STAND]);

    const cpuDetermineMoveService = ngMocks.get(CpuDetermineMoveService);

    runMarbleTest(({ cold, expectObservable }) => {
      const expected$ = cold('(a|)', { a: Move.HIT });
      const actual$ = cpuDetermineMoveService.determineMove(
        playerLinkItem,
        allowedMoves,
        entireLink,
      );
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should be able to determine correct move for soft total and bad bank hand', () => {
    const bankHand = new Hand(false);
    bankHand.addCard(PlayCard.createFromJson({ image: '6', value: '6' }));
    const bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);

    const cpuHand = new Hand(false);
    cpuHand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));
    cpuHand.addCard(PlayCard.createFromJson({ image: '7', value: '7' }));
    const playerLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      cpuHand,
      bankLinkItem,
    );

    const entireLink = new PlayerHandLink(playerLinkItem, bankLinkItem);
    const allowedMoves = new Set<Move>([Move.HIT, Move.STAND]);

    const cpuDetermineMoveService = ngMocks.get(CpuDetermineMoveService);

    runMarbleTest(({ cold, expectObservable }) => {
      const expected$ = cold('(a|)', { a: Move.STAND });
      const actual$ = cpuDetermineMoveService.determineMove(
        playerLinkItem,
        allowedMoves,
        entireLink,
      );
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should be able to determine correct move for soft total and good bank hand', () => {
    const bankHand = new Hand(false);
    bankHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    const bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);

    const cpuHand = new Hand(false);
    cpuHand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));
    cpuHand.addCard(PlayCard.createFromJson({ image: '7', value: '7' }));
    const playerLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      cpuHand,
      bankLinkItem,
    );

    const entireLink = new PlayerHandLink(playerLinkItem, bankLinkItem);
    const allowedMoves = new Set<Move>([Move.HIT, Move.STAND]);

    const cpuDetermineMoveService = ngMocks.get(CpuDetermineMoveService);

    runMarbleTest(({ cold, expectObservable }) => {
      const expected$ = cold('(a|)', { a: Move.HIT });
      const actual$ = cpuDetermineMoveService.determineMove(
        playerLinkItem,
        allowedMoves,
        entireLink,
      );
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should be able to determine to double down for hard totals', () => {
    const bankHand = new Hand(false);
    bankHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    const bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);

    const cpuHand = new Hand(false);
    cpuHand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));
    cpuHand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));
    const playerLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      cpuHand,
      bankLinkItem,
    );

    const entireLink = new PlayerHandLink(playerLinkItem, bankLinkItem);
    const allowedMoves = new Set<Move>([
      Move.HIT,
      Move.STAND,
      Move.SPLIT,
      Move.DOUBLE_DOWN,
    ]);

    const cpuDetermineMoveService = ngMocks.get(CpuDetermineMoveService);

    runMarbleTest(({ cold, expectObservable }) => {
      const expected$ = cold('(a|)', { a: Move.DOUBLE_DOWN });
      const actual$ = cpuDetermineMoveService.determineMove(
        playerLinkItem,
        allowedMoves,
        entireLink,
      );
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should be able to determine to double down for soft totals', () => {
    const bankHand = new Hand(false);
    bankHand.addCard(PlayCard.createFromJson({ image: '6', value: '6' }));
    const bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);

    const cpuHand = new Hand(false);
    cpuHand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));
    cpuHand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));
    const playerLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      cpuHand,
      bankLinkItem,
    );

    const entireLink = new PlayerHandLink(playerLinkItem, bankLinkItem);
    const allowedMoves = new Set<Move>([
      Move.HIT,
      Move.STAND,
      Move.DOUBLE_DOWN,
    ]);

    const cpuDetermineMoveService = ngMocks.get(CpuDetermineMoveService);

    runMarbleTest(({ cold, expectObservable }) => {
      const expected$ = cold('(a|)', { a: Move.DOUBLE_DOWN });
      const actual$ = cpuDetermineMoveService.determineMove(
        playerLinkItem,
        allowedMoves,
        entireLink,
      );
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should be able to determine to split', () => {
    const bankHand = new Hand(false);
    bankHand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));
    const bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);

    const cpuHand = new Hand(false);
    cpuHand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));
    cpuHand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));
    const playerLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      cpuHand,
      bankLinkItem,
    );

    const entireLink = new PlayerHandLink(playerLinkItem, bankLinkItem);
    const allowedMoves = new Set<Move>([Move.HIT, Move.STAND, Move.SPLIT]);

    const cpuDetermineMoveService = ngMocks.get(CpuDetermineMoveService);

    runMarbleTest(({ cold, expectObservable }) => {
      const expected$ = cold('(a|)', { a: Move.SPLIT });
      const actual$ = cpuDetermineMoveService.determineMove(
        playerLinkItem,
        allowedMoves,
        entireLink,
      );
      expectObservable(actual$).toEqual(expected$);
    });
  });
});
