import { Injectable } from '@angular/core';
import { AbstractDetermineMoveService } from '@services/determine-move/abstract-determine-move.service';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Move } from '@model/move';
import { defaultIfEmpty, filter, from, Observable, of, take } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CpuDetermineMoveService implements AbstractDetermineMoveService {
  determineMove(
    currentLinkItem: PlayerHandLinkItem,
    allowedMoves: Set<Move>,
    fullPlayerHandLink: PlayerHandLink,
  ): Observable<Move> {
    const bankCardValue = fullPlayerHandLink.bankLinkItem
      .getHand()
      .getCards()[0]
      .getCardValue()
      .getTotal();
    if (
      allowedMoves.has(Move.SPLIT) &&
      this.shouldSplit(
        currentLinkItem.getHand().getCards()[0].getCardValue().getTotal(),
        bankCardValue,
      )
    ) {
      return of(Move.SPLIT);
    }
    const playerTotalCardValue = currentLinkItem.getHand().getTotalCardValue();
    const possibleMoves: Move[] = playerTotalCardValue.isSoft()
      ? this.getPossibleMovesForSoftHand(
          playerTotalCardValue.getTotal(),
          bankCardValue,
        )
      : this.getPossibleMovesForHardHand(
          playerTotalCardValue.getTotal(),
          bankCardValue,
        );
    return from(possibleMoves).pipe(
      filter((move) => allowedMoves.has(move)),
      take(1),
      defaultIfEmpty(Move.STAND),
    );
  }

  private getPossibleMovesForHardHand(
    playerTotal: number,
    bankCardValue: number,
  ): Move[] {
    const moves: Move[] = [];
    if (
      (playerTotal === 9 && bankCardValue <= 6) ||
      ((playerTotal === 10 || playerTotal === 11) && bankCardValue <= 9)
    ) {
      moves.push(Move.DOUBLE_DOWN);
    }
    if (playerTotal < 12 || (bankCardValue > 6 && playerTotal < 17)) {
      moves.push(Move.HIT);
    } else {
      moves.push(Move.STAND);
    }
    return moves;
  }

  private getPossibleMovesForSoftHand(
    playerTotal: number,
    bankCardValue: number,
  ): Move[] {
    const moves: Move[] = [];
    if (playerTotal >= 16 && playerTotal <= 18 && bankCardValue <= 6) {
      moves.push(Move.DOUBLE_DOWN);
    }
    if (playerTotal < 18 || (bankCardValue > 6 && playerTotal < 19)) {
      moves.push(Move.HIT);
    } else {
      moves.push(Move.STAND);
    }
    return moves;
  }

  private shouldSplit(
    valueOfPlayerCard: number,
    bankCardValue: number,
  ): boolean {
    if (
      valueOfPlayerCard === 4 ||
      valueOfPlayerCard === 5 ||
      valueOfPlayerCard === 10
    ) {
      return false;
    }
    if (valueOfPlayerCard === 8 || valueOfPlayerCard === 11) {
      return true;
    }
    return bankCardValue <= 6;
  }
}
