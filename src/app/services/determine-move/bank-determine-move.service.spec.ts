import { BankDetermineMoveService } from './bank-determine-move.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { Hand } from '@model/hand';
import { PlayCard } from '@model/play-card';
import { PlayerHandLinkItem } from '@model/player-hand-link';
import { Player } from '@model/player';
import { runMarbleTest } from '@util/test/run-marble-test';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import { Move } from '@model/move';

describe('BankDetermineMoveService', () => {
  MockInstance.scope();

  let hand: Hand;

  beforeEach(() => {
    hand = new Hand(false);
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({ dealerHitsSoft17: true }),
    );
    return MockBuilder(BankDetermineMoveService);
  });

  it('should hit for totals lower then 17', () => {
    hand.addCard(PlayCard.createFromJson({ image: '8', value: '8' }));
    hand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));

    const playerHandLinkItem = new PlayerHandLinkItem(Player.BANK, hand, null);

    runMarbleTest(({ cold, expectObservable }) => {
      const bankDetermineMoveService = ngMocks.get(BankDetermineMoveService);
      const expected$ = cold('(a|)', { a: Move.HIT });
      const actual$ =
        bankDetermineMoveService.determineMove(playerHandLinkItem);

      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should stand for hard 17', () => {
    hand.addCard(PlayCard.createFromJson({ image: '7', value: '7' }));
    hand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));

    const playerHandLinkItem = new PlayerHandLinkItem(Player.BANK, hand, null);

    runMarbleTest(({ cold, expectObservable }) => {
      const bankDetermineMoveService = ngMocks.get(BankDetermineMoveService);
      const expected$ = cold('(a|)', { a: Move.STAND });
      const actual$ =
        bankDetermineMoveService.determineMove(playerHandLinkItem);

      expectObservable(actual$).toEqual(expected$);
    });
  });

  test.each([true, false])(
    'should decide correct move if hit at soft 17 is configured to %p',
    (hits: boolean) => {
      MockInstance(
        BlackjackSettingsService,
        'getSettings',
        jest.fn().mockReturnValue({ dealerHitsSoft17: hits }),
      );

      hand.addCard(PlayCard.createFromJson({ image: '6', value: '6' }));
      hand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));

      const playerHandLinkItem = new PlayerHandLinkItem(
        Player.BANK,
        hand,
        null,
      );

      runMarbleTest(({ cold, expectObservable }) => {
        const bankDetermineMoveService = ngMocks.get(BankDetermineMoveService);
        const expected$ = cold('(a|)', { a: hits ? Move.HIT : Move.STAND });
        const actual$ =
          bankDetermineMoveService.determineMove(playerHandLinkItem);

        expectObservable(actual$).toEqual(expected$);
      });
    },
  );
});
