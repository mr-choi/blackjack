import {
  DetermineMoveParameterData,
  DetermineMoveVisitorService,
} from './determine-move-visitor.service';
import { MockBuilder, MockInstance, MockService, ngMocks } from 'ng-mocks';
import { Observable } from 'rxjs';
import { Move } from '@model/move';
import { CPUPlayer, HumanPlayer, Player } from '@model/player';
import { BankDetermineMoveService } from '@services/determine-move/bank-determine-move.service';
import { CpuDetermineMoveService } from '@services/determine-move/cpu-determine-move.service';
import { PlayerDetermineMoveService } from '@services/determine-move/player-determine-move.service';

describe('DetermineMoveVisitorService', () => {
  MockInstance.scope();

  const expected$ = MockService<Observable<Move>>(Observable);
  const parameterData = MockService<DetermineMoveParameterData>({});

  beforeEach(() => {
    return MockBuilder(DetermineMoveVisitorService);
  });

  it('should be able to get determined move for bank', () => {
    const player = Player.BANK;
    MockInstance(
      BankDetermineMoveService,
      'determineMove',
      jest.fn().mockReturnValue(expected$),
    );

    const determineMoveVisitorService = ngMocks.get(
      DetermineMoveVisitorService,
    );
    const actual$ = player.accept(determineMoveVisitorService, parameterData);
    expect(actual$).toBe(expected$);

    const bankDetermineMoveService = ngMocks.get(BankDetermineMoveService);
    expect(bankDetermineMoveService.determineMove).toHaveBeenCalled();
  });

  it('should be able to get determined move for cpu', () => {
    const player = new CPUPlayer();
    MockInstance(
      CpuDetermineMoveService,
      'determineMove',
      jest.fn().mockReturnValue(expected$),
    );

    const determineMoveVisitorService = ngMocks.get(
      DetermineMoveVisitorService,
    );
    const actual$ = player.accept(determineMoveVisitorService, parameterData);
    expect(actual$).toBe(expected$);

    const cpuDetermineMoveService = ngMocks.get(CpuDetermineMoveService);
    expect(cpuDetermineMoveService.determineMove).toHaveBeenCalled();
  });

  it('should be able to get determined move for human player', () => {
    const player = new HumanPlayer('Player 1', 1000);
    MockInstance(
      PlayerDetermineMoveService,
      'determineMove',
      jest.fn().mockReturnValue(expected$),
    );

    const determineMoveVisitorService = ngMocks.get(
      DetermineMoveVisitorService,
    );
    const actual$ = player.accept(determineMoveVisitorService, parameterData);
    expect(actual$).toBe(expected$);

    const playerDetermineMoveService = ngMocks.get(PlayerDetermineMoveService);
    expect(playerDetermineMoveService.determineMove).toHaveBeenCalled();
  });
});
