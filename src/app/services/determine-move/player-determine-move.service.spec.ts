import { PlayerDetermineMoveService } from './player-determine-move.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { runMarbleTest } from '@util/test/run-marble-test';
import { HumanPlayer } from '@model/player';
import { Hand } from '@model/hand';
import { PlayerHandLinkItem } from '@model/player-hand-link';
import { Move } from '@model/move';
import { filter, map, switchMap, take, tap } from 'rxjs';
import { isNull } from 'lodash-es';

describe('PlayerDetermineMoveService', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(PlayerDetermineMoveService);
  });

  it('should determine the first legal move the player chooses', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      const humanPlayer = new HumanPlayer('Player 1', 1000);
      const hand = new Hand(false);
      const playerHandLinkItem = new PlayerHandLinkItem(
        humanPlayer,
        hand,
        null,
      );

      const allowedMoves = new Set<Move>([Move.HIT, Move.STAND]);

      const playerDetermineMoveService = ngMocks.get(
        PlayerDetermineMoveService,
      );
      const makeMove$ = playerDetermineMoveService.determineMoveObject$.pipe(
        filter((x) => !isNull(x)),
        take(1),
        switchMap((determineMoveObject) =>
          cold('-a-(b|)', { a: Move.DOUBLE_DOWN, b: Move.HIT }).pipe(
            tap((move) => determineMoveObject?.select(move)),
          ),
        ),
      );

      const expectedMove$ = cold('---(a|)', { a: Move.HIT });
      const actualMove$ = playerDetermineMoveService.determineMove(
        playerHandLinkItem,
        allowedMoves,
      );

      const expectedDetermineMoveObjectPresent$ = cold('b--a', {
        a: true,
        b: false,
      });
      const actualDetermineMoveObjectPresent$ =
        playerDetermineMoveService.determineMoveObject$.pipe(map(isNull));

      expectObservable(makeMove$);
      expectObservable(actualDetermineMoveObjectPresent$).toEqual(
        expectedDetermineMoveObjectPresent$,
      );
      expectObservable(actualMove$).toEqual(expectedMove$);
    });
  });
});
