import { Injectable } from '@angular/core';
import { PlayersService } from '@services/config/players.service';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Player } from '@model/player';
import { Hand } from '@model/hand';
import { clone } from 'lodash-es';

@Injectable({
  providedIn: 'root',
})
export class PlayerHandLinkFactoryService {
  constructor(private playersService: PlayersService) {}

  create(): PlayerHandLink {
    const playersReversed = clone(this.playersService.getPlayers()).reverse();
    const bankLink = new PlayerHandLinkItem(Player.BANK, new Hand(false), null);
    let nextLink = bankLink;
    for (const player of playersReversed) {
      nextLink = new PlayerHandLinkItem(player, new Hand(false), nextLink);
    }
    return new PlayerHandLink(nextLink, bankLink);
  }
}
