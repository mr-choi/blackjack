import { Injectable } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable()
export class UnsavedChangesService {
  private readonly unsavedSettingsChanges$$ = new BehaviorSubject(false);

  constructor(private confirmationService: ConfirmationService) {}

  setUnsavedSettingsChanges(value: boolean) {
    this.unsavedSettingsChanges$$.next(value);
  }

  canDeactivate(): Observable<boolean> {
    if (!this.unsavedSettingsChanges$$.getValue()) {
      return of(true);
    }
    return new Observable<boolean>((subscriber) => {
      this.confirmationService.confirm({
        message: 'Your unsaved changes will be lost.',
        header: 'Are you sure you want to go back?',
        accept: () => {
          subscriber.next(true);
          subscriber.complete();
        },
        reject: () => {
          subscriber.next(false);
          subscriber.complete();
        },
      });
    });
  }
}
