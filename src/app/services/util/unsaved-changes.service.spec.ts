import { UnsavedChangesService } from './unsaved-changes.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { ConfirmationService } from 'primeng/api';
import { runMarbleTest } from '@util/test/run-marble-test';
import { tap } from 'rxjs';
import Mock = jest.Mock;

describe('UnsavedChangesService', () => {
  MockInstance.scope();

  beforeEach(() => {
    MockInstance(ConfirmationService, 'confirm', jest.fn());
    return MockBuilder(UnsavedChangesService).mock(ConfirmationService);
  });

  it('should not prompt if there are no unsaved changes', () => {
    const unsavedChangesService = ngMocks.get(UnsavedChangesService);
    runMarbleTest(({ expectObservable }) => {
      expectObservable(unsavedChangesService.canDeactivate()).toBe('(a|)', {
        a: true,
      });
    });
    const confirmationService = ngMocks.get(ConfirmationService);
    expect(confirmationService.confirm).not.toHaveBeenCalled();
  });

  it('should prompt if there are unsaved changes and deactivate if user accepts', () => {
    const unsavedChangesService = ngMocks.get(UnsavedChangesService);
    unsavedChangesService.setUnsavedSettingsChanges(true);

    runMarbleTest(({ expectObservable, cold }) => {
      const confirmationResponse$ = cold('-(a|)').pipe(
        tap(() => {
          const confirmationService = ngMocks.get(ConfirmationService);
          (confirmationService.confirm as Mock).mock.calls[0][0].accept();
        }),
      );
      expectObservable(confirmationResponse$);
      expectObservable(unsavedChangesService.canDeactivate()).toBe('-(a|)', {
        a: true,
      });
    });
  });

  it('should prompt if there are unsaved changes and not deactivate if user rejects', () => {
    const unsavedChangesService = ngMocks.get(UnsavedChangesService);
    unsavedChangesService.setUnsavedSettingsChanges(true);

    runMarbleTest(({ expectObservable, cold }) => {
      const confirmationResponse$ = cold('-(a|)').pipe(
        tap(() => {
          const confirmationService = ngMocks.get(ConfirmationService);
          (confirmationService.confirm as Mock).mock.calls[0][0].reject();
        }),
      );
      expectObservable(confirmationResponse$);
      expectObservable(unsavedChangesService.canDeactivate()).toBe('-(a|)', {
        a: false,
      });
    });
  });
});
