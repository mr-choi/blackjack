import { Injectable } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { BlackjackGameManagementService } from '@services/blackjack-game-management.service';
import { Observable, of, Subscription, switchMap, tap } from 'rxjs';

@Injectable()
export class GameInProgressService {
  private subscription?: Subscription;

  constructor(
    private confirmationService: ConfirmationService,
    private blackjackGameManagementService: BlackjackGameManagementService,
    private messageService: MessageService,
  ) {}

  newGame() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = this.blackjackGameManagementService.game$.subscribe({
      error: () => {
        this.messageService.add({
          summary: 'An error occurred during the game',
          detail: 'Bets are refunded.',
          severity: 'error',
        });
      },
    });
  }

  canActivate(): boolean {
    this.blackjackGameManagementService.resetGame();
    return true;
  }

  canDeactivate(): Observable<boolean> {
    return this.blackjackGameManagementService.gamePhase$.pipe(
      // Close any confirm dialog when the game state changes
      tap(() => this.confirmationService.close()),
      switchMap((phase) => {
        if (phase !== 'PLAYING') {
          return of(true);
        }
        return new Observable<boolean>((subscriber) => {
          this.confirmationService.confirm({
            message: 'All bets will be lost.',
            header: 'Are you sure you want to abandon the game?',
            accept: () => {
              subscriber.next(true);
              subscriber.complete();
              this.subscription?.unsubscribe();
            },
            reject: () => {
              subscriber.next(false);
              subscriber.complete();
            },
          });
        });
      }),
    );
  }
}
