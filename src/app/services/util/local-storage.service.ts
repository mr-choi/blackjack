import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  getItem<T>(key: string): T | null {
    const stringJSON = localStorage.getItem(key);
    return stringJSON ? JSON.parse(stringJSON) : null;
  }

  setItem(key: string, value: unknown) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  removeItem(key: string) {
    localStorage.removeItem(key);
  }
}
