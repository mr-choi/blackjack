import { GameInProgressService } from './game-in-progress.service';
import { MockBuilder, MockInstance, MockService, ngMocks } from 'ng-mocks';
import { ConfirmationService, MessageService } from 'primeng/api';
import {
  BlackjackGameManagementService,
  GamePhase,
} from '@services/blackjack-game-management.service';
import { Observable, Subscription, tap } from 'rxjs';
import { runMarbleTest } from '@util/test/run-marble-test';
import Mock = jest.Mock;

describe('GameInProgressService', () => {
  MockInstance.scope();

  let subscription: Subscription;

  beforeEach(() => {
    subscription = MockService(Subscription, { unsubscribe: jest.fn() });
    MockInstance(
      BlackjackGameManagementService,
      'game$',
      MockService<Observable<never>>(Observable, {
        subscribe: jest.fn().mockReturnValue(subscription),
      }),
    );
    MockInstance(ConfirmationService, 'confirm', jest.fn());
    MockInstance(BlackjackGameManagementService, 'resetGame', jest.fn());
    return MockBuilder(GameInProgressService, [
      ConfirmationService,
      MessageService,
    ]);
  });

  it('start new game upon request and resubscribe when started again', () => {
    const gameInProgressService = ngMocks.get(GameInProgressService);
    gameInProgressService.newGame();

    const blackjackGameManagementService = ngMocks.get(
      BlackjackGameManagementService,
    );
    expect(blackjackGameManagementService.game$.subscribe).toHaveBeenCalled();
    expect(subscription.unsubscribe).not.toHaveBeenCalled();

    gameInProgressService.newGame();
    expect(subscription.unsubscribe).toHaveBeenCalled();
  });

  it('should reset game if route gets activated', () => {
    const gameInProgressService = ngMocks.get(GameInProgressService);
    expect(gameInProgressService.canActivate()).toBe(true);

    const blackjackGameManagementService = ngMocks.get(
      BlackjackGameManagementService,
    );
    expect(blackjackGameManagementService.resetGame).toHaveBeenCalled();
  });

  it('should prompt confirmation if user leaves while playing', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BlackjackGameManagementService,
        'gamePhase$',
        cold<GamePhase>('a', { a: 'PLAYING' }),
      );

      const confirmResponse$ = cold('-(a|)').pipe(
        tap(() => {
          const confirmationService = ngMocks.get(ConfirmationService);
          (confirmationService.confirm as Mock).mock.calls[0][0].accept();
        }),
      );

      const gameInProgressService = ngMocks.get(GameInProgressService);

      const expected$ = cold('-a', { a: true });
      const actual$ = gameInProgressService.canDeactivate();

      expectObservable(confirmResponse$);
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should not navigate away if user rejects confirmation', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BlackjackGameManagementService,
        'gamePhase$',
        cold<GamePhase>('a', { a: 'PLAYING' }),
      );

      const confirmResponse$ = cold('-(a|)').pipe(
        tap(() => {
          const confirmationService = ngMocks.get(ConfirmationService);
          (confirmationService.confirm as Mock).mock.calls[0][0].reject();
        }),
      );

      const gameInProgressService = ngMocks.get(GameInProgressService);

      const expected$ = cold('-a', { a: false });
      const actual$ = gameInProgressService.canDeactivate();

      expectObservable(confirmResponse$);
      expectObservable(actual$).toEqual(expected$);
    });
  });

  it('should not prompt confirmation if user leaves while not playing', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BlackjackGameManagementService,
        'gamePhase$',
        cold<GamePhase>('a', { a: 'FINISHING' }),
      );

      const gameInProgressService = ngMocks.get(GameInProgressService);

      const expected$ = cold('a', { a: true });
      const actual$ = gameInProgressService.canDeactivate();

      expectObservable(actual$).toEqual(expected$);
    });
  });
});
