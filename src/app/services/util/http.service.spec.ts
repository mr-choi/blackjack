import { HttpService } from './http.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { HttpClient } from '@angular/common/http';
import { runMarbleTest } from '@util/test/run-marble-test';

describe('HttpService', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(HttpService).mock(HttpClient);
  });

  it('should retry 3 times', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(HttpClient, 'get', jest.fn().mockReturnValue(cold('-#')));

      const httpService = ngMocks.get(HttpService);

      const expected$ = cold('---#');
      const actual$ = httpService.get('http://whatever');

      expectObservable(actual$).toEqual(expected$);
    });
  });
});
