import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { LocalStorageService } from '@services/util/local-storage.service';

describe('LocalStorageService', () => {
  MockInstance.scope();

  beforeEach(() => {
    jest.spyOn(Storage.prototype, 'getItem').mockImplementation(jest.fn());
    jest.spyOn(Storage.prototype, 'setItem').mockImplementation(jest.fn());
    jest.spyOn(Storage.prototype, 'removeItem').mockImplementation(jest.fn());
    return MockBuilder(LocalStorageService);
  });

  it('should get correctly', () => {
    const localStorageService = ngMocks.get(LocalStorageService);
    const example_key = 'example_key';
    localStorageService.getItem(example_key);
    expect(Storage.prototype.getItem).toHaveBeenCalledWith(example_key);
  });

  it('should set correctly', () => {
    const localStorageService = ngMocks.get(LocalStorageService);
    const example_key = 'example_key';
    const example_value = { foo: 1, bar: 'baz' };
    localStorageService.setItem(example_key, example_value);
    expect(Storage.prototype.setItem).toHaveBeenCalledWith(
      example_key,
      JSON.stringify(example_value),
    );
  });

  it('should remove correctly', () => {
    const localStorageService = ngMocks.get(LocalStorageService);
    const example_key = 'example_key';
    localStorageService.removeItem(example_key);
    expect(Storage.prototype.removeItem).toHaveBeenCalledWith(example_key);
  });

  afterEach(() => jest.clearAllMocks());
});
