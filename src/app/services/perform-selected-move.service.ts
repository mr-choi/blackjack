import { Injectable } from '@angular/core';
import { AllowedMovesService } from '@services/allowed-moves.service';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { Move } from '@model/move';
import {
  defer,
  identity,
  Observable,
  repeat,
  switchMap,
  take,
  takeWhile,
} from 'rxjs';
import { CardDrawService } from '@services/card-draw.service';
import { DetermineMoveVisitorService } from '@services/determine-move/determine-move-visitor.service';

@Injectable({
  providedIn: 'root',
})
export class PerformSelectedMoveService {
  constructor(
    private allowedMovesService: AllowedMovesService,
    private cardDrawService: CardDrawService,
    private determineMoveVisitorService: DetermineMoveVisitorService,
  ) {}

  perform$(
    currentLinkItem: PlayerHandLinkItem,
    fullPlayerHandLink: PlayerHandLink,
  ): Observable<boolean> {
    // Unrevealed hole cards will not be considered to determine allowed moves, so we reveal it first to avoid wrong
    // decisions.
    currentLinkItem.getHand().revealHoleCard();
    return defer(() => {
      const allowedMoves: Set<Move> = this.allowedMovesService.getAllowedMoves(
        currentLinkItem,
        fullPlayerHandLink,
      );
      const cardSource$ = this.cardDrawService.deckId$.pipe(
        switchMap((deckId) => this.cardDrawService.drawCardFromDeck$(deckId)),
      );

      if (allowedMoves.size === 1) {
        return Array.from(allowedMoves)[0].perform$(
          currentLinkItem,
          cardSource$,
        );
      }
      return this.getMove(
        currentLinkItem,
        fullPlayerHandLink,
        allowedMoves,
      ).pipe(
        take(1),
        switchMap((move) => move.perform$(currentLinkItem, cardSource$)),
      );
    }).pipe(take(1), repeat(), takeWhile(identity));
  }

  private getMove(
    currentLinkItem: PlayerHandLinkItem,
    fullPlayerHandLink: PlayerHandLink,
    allowedMoves: Set<Move>,
  ): Observable<Move> {
    return currentLinkItem
      .getPlayer()
      .accept(this.determineMoveVisitorService, {
        currentLinkItem: currentLinkItem,
        fullPlayerHandLink: fullPlayerHandLink,
        allowedMoves: allowedMoves,
      });
  }
}
