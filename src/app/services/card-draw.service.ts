import { Injectable } from '@angular/core';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import {
  distinctUntilChanged,
  map,
  Observable,
  shareReplay,
  switchAll,
  take,
} from 'rxjs';
import { CardJson, PlayCard } from '@model/play-card';
import { HttpService } from '@services/util/http.service';

export function getNewShuffledDeckBaseURL(numberOfDecks: number): string {
  return `https://www.deckofcardsapi.com/api/deck/new/shuffle/?deck_count=${numberOfDecks}`;
}

export function getDrawSingleCardBaseUrl(deckId: string) {
  return `https://www.deckofcardsapi.com/api/deck/${deckId}/draw/?count=1`;
}

export function getPutAllCardsBackToDeckBaseUrl(deckId: string) {
  return `https://www.deckofcardsapi.com/api/deck/${deckId}/return/`;
}

@Injectable({
  providedIn: 'root',
})
export class CardDrawService {
  constructor(
    private httpService: HttpService,
    private blackJackSettingsService: BlackjackSettingsService,
  ) {}

  deckId$: Observable<string> = this.blackJackSettingsService.settings$.pipe(
    map((settings) => settings.numberOfDecks),
    distinctUntilChanged(),
    map((numberOfDecks) => {
      const url = getNewShuffledDeckBaseURL(numberOfDecks);
      return this.httpService.get<{ deck_id: string }>(url).pipe(
        map((response) => response.deck_id),
        shareReplay({ bufferSize: 1, refCount: false }),
      );
    }),
    shareReplay({ bufferSize: 1, refCount: false }),
    take(1),
    switchAll(),
  );

  drawCardFromDeck$(deckId: string): Observable<PlayCard> {
    const url = getDrawSingleCardBaseUrl(deckId);
    return this.httpService
      .get<{ cards: [CardJson] }>(url)
      .pipe(map((response) => PlayCard.createFromJson(response.cards[0])));
  }

  putCardsBackToDeck$(deckId: string): Observable<void> {
    const url = getPutAllCardsBackToDeckBaseUrl(deckId);
    return this.httpService.get<void>(url);
  }
}
