import {
  CardDrawService,
  getDrawSingleCardBaseUrl,
  getNewShuffledDeckBaseURL,
  getPutAllCardsBackToDeckBaseUrl,
} from './card-draw.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import { runMarbleTest } from '@util/test/run-marble-test';
import { CardJson, PlayCard } from '@model/play-card';
import { BlackjackSettings } from '@model/blackjack-settings';
import { HttpService } from '@services/util/http.service';
import { EMPTY } from 'rxjs';

describe('CardDrawService', () => {
  MockInstance.scope();

  beforeEach(() => {
    MockInstance(BlackjackSettingsService, 'settings$', EMPTY);
    return MockBuilder(CardDrawService);
  });

  it('should be able to get new shuffled deck every time the number of decks configured changes', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        HttpService,
        'get',
        jest
          .fn()
          .mockReturnValueOnce(cold('-(a|)', { a: { deck_id: 'abc123' } }))
          .mockReturnValue(cold('-(a|)', { a: { deck_id: 'xyz789' } })),
      );
      MockInstance(
        BlackjackSettingsService,
        'settings$',
        cold('a-b-c-', {
          a: { numberOfDecks: 6 } as BlackjackSettings,
          b: { numberOfDecks: 6 } as BlackjackSettings,
          c: { numberOfDecks: 4 } as BlackjackSettings,
        }),
      );
      const cardDrawService = ngMocks.get(CardDrawService);

      const expected1$ = cold('-(a|)', { a: 'abc123' });
      const expected2$ = cold('(a|)', { a: 'abc123' });
      const expected3$ = cold('-(a|)', { a: 'xyz789' });
      const expected4$ = cold('(a|)', { a: 'xyz789' });

      const actual$ = cardDrawService.deckId$;

      expectObservable(actual$, '^------').toEqual(expected1$);
      expectObservable(actual$, '-^-----').toEqual(expected2$);
      expectObservable(actual$, '---^---').toEqual(expected2$);
      expectObservable(actual$, '-----^-').toEqual(expected3$);
      expectObservable(actual$, '------^').toEqual(expected4$);
    });

    const httpService = ngMocks.get(HttpService);
    expect(httpService.get).toHaveBeenCalledTimes(2);
    expect(httpService.get).toHaveBeenNthCalledWith(
      1,
      getNewShuffledDeckBaseURL(6),
    );
    expect(httpService.get).toHaveBeenNthCalledWith(
      2,
      getNewShuffledDeckBaseURL(4),
    );
  });

  it('should be able to draw a card', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      const cardJson: CardJson = { image: 'url/to/image.png', value: 'KING' };
      MockInstance(
        HttpService,
        'get',
        jest.fn().mockReturnValue(cold('-(a|)', { a: { cards: [cardJson] } })),
      );
      const cardDrawService = ngMocks.get(CardDrawService);

      const expected$ = cold('-(a|)', { a: PlayCard.createFromJson(cardJson) });
      const actual$ = cardDrawService.drawCardFromDeck$('abc123');

      expectObservable(actual$).toEqual(expected$);
    });

    const httpService = ngMocks.get(HttpService);
    expect(httpService.get).toHaveBeenCalledWith(
      getDrawSingleCardBaseUrl('abc123'),
    );
  });

  it('should be able put all cards back into the deck', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        HttpService,
        'get',
        jest.fn().mockReturnValue(cold('-(a|)', { a: undefined })),
      );
      const cardDrawService = ngMocks.get(CardDrawService);

      const expected$ = cold('-(a|)', { a: undefined });
      const actual$ = cardDrawService.putCardsBackToDeck$('abc123');

      expectObservable(actual$).toEqual(expected$);
    });

    const httpService = ngMocks.get(HttpService);
    expect(httpService.get).toHaveBeenCalledWith(
      getPutAllCardsBackToDeckBaseUrl('abc123'),
    );
  });
});
