import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  concatMap,
  endWith,
  filter,
  from,
  identity,
  ignoreElements,
  Observable,
  of,
  tap,
} from 'rxjs';
import { HumanPlayerDetermineInsuranceObject } from '@model/human-player-determine-insurance-object';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { isNull } from 'lodash-es';
import { HumanPlayer } from '@model/player';

@Injectable({
  providedIn: 'root',
})
export class InsuranceService {
  private determineInsuranceObject$$ =
    new BehaviorSubject<HumanPlayerDetermineInsuranceObject | null>(null);
  readonly determineInsuranceObject$ =
    this.determineInsuranceObject$$.asObservable();

  determineInsuranceIfApplicable$(
    playerHandLink: PlayerHandLink,
  ): Observable<PlayerHandLink> {
    // Check if asking for insurance is applicable
    if (playerHandLink.bankLinkItem.getHand().getCards()[0].rank !== 'ACE') {
      return of(playerHandLink);
    }

    const humanPlayerLinkItems: PlayerHandLinkItem[] = [];
    for (
      let playerHandLinkItem: PlayerHandLinkItem | null =
        playerHandLink.firstLinkItem;
      !isNull(playerHandLinkItem);
      playerHandLinkItem = playerHandLinkItem.getNext()
    ) {
      if (playerHandLinkItem.getPlayer() instanceof HumanPlayer) {
        humanPlayerLinkItems.push(playerHandLinkItem);
      }
    }

    return from(humanPlayerLinkItems).pipe(
      concatMap((humanPlayerLinkItem) =>
        this.determineInsuranceSinglePlayer$(humanPlayerLinkItem).pipe(
          filter(identity),
          tap(() => {
            (humanPlayerLinkItem.getPlayer() as HumanPlayer).balance -=
              humanPlayerLinkItem.getBet() / 2;
            humanPlayerLinkItem.insure();
          }),
        ),
      ),
      ignoreElements(),
      endWith(playerHandLink),
    );
  }

  private determineInsuranceSinglePlayer$(
    humanPlayerHandLinkItem: PlayerHandLinkItem,
  ): Observable<boolean> {
    const humanPlayer = humanPlayerHandLinkItem.getPlayer() as HumanPlayer;

    // Should not allow insurance if player cannot afford it:
    if (humanPlayerHandLinkItem.getBet() > 2 * humanPlayer.balance) {
      return of(false);
    }

    const determineInsuranceObject = new HumanPlayerDetermineInsuranceObject(
      humanPlayer,
    );
    this.determineInsuranceObject$$.next(determineInsuranceObject);
    return determineInsuranceObject.wantsInsurance$.pipe(
      tap(() => this.determineInsuranceObject$$.next(null)),
    );
  }
}
