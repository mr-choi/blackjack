import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
  BlackjackSettings,
  BlackJackSettingsJson,
} from '@model/blackjack-settings';
import { DoubleDownTotalRule } from '@model/double-down-total-rule';
import { SplitRule } from '@model/split-rule';
import { DoubleDownRule } from '@model/double-down-rule';
import { LocalStorageService } from '@services/util/local-storage.service';

export const BLACKJACK_SETTINGS_LOCAL_STORAGE_KEY = 'blackjack_settings';
export const DEFAULT_BLACKJACK_SETTINGS: BlackjackSettings = {
  minimumBet: 1,
  numberOfDecks: 1,
  dealerDrawsHoleCard: true,
  dealerHitsSoft17: false,
  allowSurrender: true,
  doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ALWAYS, true),
  splitRule: SplitRule.SAME_VALUE,
  maxHandsPerPlayer: Number.MAX_SAFE_INTEGER,
};

@Injectable({
  providedIn: 'root',
})
export class BlackjackSettingsService {
  private blackjackSettings$$ = new BehaviorSubject<BlackjackSettings>(
    DEFAULT_BLACKJACK_SETTINGS,
  );
  settings$ = this.blackjackSettings$$.asObservable();

  constructor(private localStorageService: LocalStorageService) {
    const json = this.localStorageService.getItem<BlackJackSettingsJson>(
      BLACKJACK_SETTINGS_LOCAL_STORAGE_KEY,
    );
    if (json) this.setSettingsFromJson(json);
  }

  getSettings(): BlackjackSettings {
    return this.blackjackSettings$$.getValue();
  }

  setSettings(blackjackSettings: BlackjackSettings) {
    this.blackjackSettings$$.next(blackjackSettings);
    this.localStorageService.setItem(
      BLACKJACK_SETTINGS_LOCAL_STORAGE_KEY,
      this.getSettingsAsJson(),
    );
  }

  resetSettings() {
    this.blackjackSettings$$.next(DEFAULT_BLACKJACK_SETTINGS);
    this.localStorageService.removeItem(BLACKJACK_SETTINGS_LOCAL_STORAGE_KEY);
  }

  private setSettingsFromJson(json: BlackJackSettingsJson) {
    this.blackjackSettings$$.next({
      minimumBet: json.minimumBet,
      numberOfDecks: json.numberOfDecks,
      dealerDrawsHoleCard: json.dealerDrawsHoleCard,
      dealerHitsSoft17: json.dealerHitsSoft17,
      allowSurrender: json.allowSurrender,
      doubleDownRule: new DoubleDownRule(
        DoubleDownTotalRule.allByName.get(
          json.doubleDownRule.doubleDownTotalRule,
        ) ?? DoubleDownTotalRule.ALWAYS,
        json.doubleDownRule.doubleDownAfterSplitAllowed,
      ),
      splitRule:
        SplitRule.allByName.get(json.splitRule) ?? SplitRule.SAME_VALUE,
      maxHandsPerPlayer: json.maxHandsPerPlayer,
    });
  }

  private getSettingsAsJson(): BlackJackSettingsJson {
    const obj = this.getSettings();
    return {
      minimumBet: obj.minimumBet,
      numberOfDecks: obj.numberOfDecks,
      dealerDrawsHoleCard: obj.dealerDrawsHoleCard,
      dealerHitsSoft17: obj.dealerHitsSoft17,
      allowSurrender: obj.allowSurrender,
      doubleDownRule: {
        doubleDownTotalRule: obj.doubleDownRule.doubleDownTotalRule.name,
        doubleDownAfterSplitAllowed:
          obj.doubleDownRule.doubleDownAfterSplitAllowed,
      },
      splitRule: obj.splitRule.name,
      maxHandsPerPlayer: obj.maxHandsPerPlayer,
    };
  }
}
