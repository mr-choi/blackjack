import { Injectable } from '@angular/core';
import { LocalStorageService } from '@services/util/local-storage.service';
import { CPUPlayer, HumanPlayer, Player, PlayerJson } from '@model/player';
import {
  BehaviorSubject,
  debounceTime,
  distinctUntilChanged,
  map,
  merge,
  Observable,
  shareReplay,
  skip,
  Subscription,
  tap,
} from 'rxjs';
import { isEqual } from 'lodash-es';

export const PLAYERS_LOCAL_STORAGE_KEY = 'players';
export const STARTING_BALANCE = 1000;

@Injectable({
  providedIn: 'root',
})
export class PlayersService {
  private subscription?: Subscription;

  private players$$ = new BehaviorSubject<Player[]>([]);

  players$: Observable<Player[]> = this.players$$.asObservable();
  humanPlayers$: Observable<HumanPlayer[]> = this.players$.pipe(
    map(
      (players) =>
        players.filter((player) => player.type === 'human') as HumanPlayer[],
    ),
    distinctUntilChanged(isEqual),
    tap((humanPlayers) => this.trackAndSaveBalanceChanges(humanPlayers)),
    shareReplay({ bufferSize: 1, refCount: false }),
  );

  constructor(private localStorageService: LocalStorageService) {
    const jsons = this.localStorageService.getItem<PlayerJson[]>(
      PLAYERS_LOCAL_STORAGE_KEY,
    );

    if (jsons) this.loadFromLocalStorageJson(jsons);
  }

  getPlayers(): Player[] {
    return this.players$$.getValue();
  }

  setPlayers(players: Player[]) {
    this.players$$.next(players);
    this.localStorageService.setItem(
      PLAYERS_LOCAL_STORAGE_KEY,
      this.getPlayersAsJson(),
    );
  }

  private trackAndSaveBalanceChanges(humanPlayers: HumanPlayer[]) {
    this.subscription?.unsubscribe();
    this.subscription = merge(
      ...humanPlayers.map((player) => player.balance$.pipe(skip(1))),
    )
      .pipe(debounceTime(0))
      .subscribe(() =>
        this.localStorageService.setItem(
          PLAYERS_LOCAL_STORAGE_KEY,
          this.getPlayersAsJson(),
        ),
      );
  }

  private getPlayersAsJson(): PlayerJson[] {
    return this.getPlayers().map((obj) => ({
      isCPU: obj.type === 'cpu',
      name: (obj as HumanPlayer)?.name,
      balance: (obj as HumanPlayer)?.balance,
    }));
  }

  private loadFromLocalStorageJson(jsons: PlayerJson[]) {
    this.players$$.next(
      jsons.map((json, index) =>
        json.isCPU
          ? new CPUPlayer()
          : new HumanPlayer(
              json.name ?? `Player ${index + 1}`,
              json.balance ?? STARTING_BALANCE,
            ),
      ),
    );
  }
}
