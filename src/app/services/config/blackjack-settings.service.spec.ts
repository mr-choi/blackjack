import {
  BLACKJACK_SETTINGS_LOCAL_STORAGE_KEY,
  BlackjackSettingsService,
  DEFAULT_BLACKJACK_SETTINGS,
} from './blackjack-settings.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { DoubleDownRule } from '@model/double-down-rule';
import { DoubleDownTotalRule } from '@model/double-down-total-rule';
import { SplitRule } from '@model/split-rule';
import {
  BlackjackSettings,
  BlackJackSettingsJson,
} from '@model/blackjack-settings';
import { LocalStorageService } from '@services/util/local-storage.service';
import { runMarbleTest } from '@util/test/run-marble-test';
import { tap } from 'rxjs';

describe('BlackjackSettingsService', () => {
  MockInstance.scope();

  const MODIFIED_SETTINGS: BlackjackSettings = {
    minimumBet: 5,
    numberOfDecks: 4,
    dealerDrawsHoleCard: false,
    dealerHitsSoft17: true,
    allowSurrender: false,
    doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ONLY_10_11, false),
    splitRule: SplitRule.SAME_RANK,
    maxHandsPerPlayer: 4,
  };
  const MODIFIED_SETTINGS_JSON: BlackJackSettingsJson = {
    minimumBet: 5,
    numberOfDecks: 4,
    dealerDrawsHoleCard: false,
    dealerHitsSoft17: true,
    allowSurrender: false,
    doubleDownRule: {
      doubleDownTotalRule: 'ONLY_10_11',
      doubleDownAfterSplitAllowed: false,
    },
    splitRule: 'SAME_RANK',
    maxHandsPerPlayer: 4,
  };

  beforeEach(() => {
    MockInstance(
      LocalStorageService,
      'getItem',
      jest.fn().mockReturnValue(null),
    );
    MockInstance(LocalStorageService, 'setItem', jest.fn());
    MockInstance(LocalStorageService, 'removeItem', jest.fn());
    return MockBuilder(BlackjackSettingsService);
  });

  it('should have default settings if nothing saved in local storage', () => {
    const actualSettings = ngMocks.get(BlackjackSettingsService).getSettings();
    expect(actualSettings).toEqual(DEFAULT_BLACKJACK_SETTINGS);
  });

  it('should init settings correctly depending on local storage', () => {
    MockInstance(
      LocalStorageService,
      'getItem',
      jest.fn().mockReturnValue(MODIFIED_SETTINGS_JSON),
    );
    const actualSettings = ngMocks.get(BlackjackSettingsService).getSettings();
    expect(actualSettings).toEqual(MODIFIED_SETTINGS);
  });

  it('should set settings correctly and save to local storage', () => {
    const blackjackSettingsService = ngMocks.get(BlackjackSettingsService);
    const localStorageService = ngMocks.get(LocalStorageService);

    blackjackSettingsService.setSettings(MODIFIED_SETTINGS);
    const actualSettings = blackjackSettingsService.getSettings();

    expect(actualSettings).toEqual(MODIFIED_SETTINGS);
    expect(localStorageService.setItem).toHaveBeenCalledWith(
      BLACKJACK_SETTINGS_LOCAL_STORAGE_KEY,
      MODIFIED_SETTINGS_JSON,
    );
  });

  it('should use default settings and remove local storage upon reset', () => {
    const blackjackSettingsService = ngMocks.get(BlackjackSettingsService);
    const localStorageService = ngMocks.get(LocalStorageService);

    blackjackSettingsService.setSettings(MODIFIED_SETTINGS);
    blackjackSettingsService.resetSettings();
    const actualSettings = blackjackSettingsService.getSettings();

    expect(actualSettings).toEqual(DEFAULT_BLACKJACK_SETTINGS);
    expect(localStorageService.removeItem).toHaveBeenCalledWith(
      BLACKJACK_SETTINGS_LOCAL_STORAGE_KEY,
    );
  });

  it('should emit latest settings', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      const blackjackSettingsService = ngMocks.get(BlackjackSettingsService);

      const setSettings$ = cold('--a', { a: MODIFIED_SETTINGS }).pipe(
        tap((settings) => blackjackSettingsService.setSettings(settings)),
      );
      const expected$ = cold('a-b', {
        a: DEFAULT_BLACKJACK_SETTINGS,
        b: MODIFIED_SETTINGS,
      });
      const actual$ = blackjackSettingsService.settings$;

      expectObservable(actual$).toEqual(expected$);
      expectObservable(setSettings$);
    });
  });
});
