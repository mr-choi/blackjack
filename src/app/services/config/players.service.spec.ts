import { PLAYERS_LOCAL_STORAGE_KEY, PlayersService } from './players.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { CPUPlayer, HumanPlayer, Player, PlayerJson } from '@model/player';
import { LocalStorageService } from '@services/util/local-storage.service';
import { runMarbleTest } from '@util/test/run-marble-test';
import { tap } from 'rxjs';

describe('PlayersService', () => {
  MockInstance.scope();

  const EXAMPLE_HUMAN_PLAYER = new HumanPlayer('Player 1', 500);
  const EXAMPLE_PLAYERS: Player[] = [EXAMPLE_HUMAN_PLAYER, new CPUPlayer()];
  const EXAMPLE_PLAYERS_JSON: PlayerJson[] = [
    { isCPU: false, name: 'Player 1', balance: 500 },
    { isCPU: true },
  ];

  beforeEach(() => {
    MockInstance(
      LocalStorageService,
      'getItem',
      jest.fn().mockReturnValue(null),
    );
    MockInstance(LocalStorageService, 'setItem', jest.fn());
    return MockBuilder(PlayersService);
  });

  it('should init with empty list if nothing saved in local storage', () => {
    const actualPlayers = ngMocks.get(PlayersService).getPlayers();
    expect(actualPlayers).toEqual([]);
  });

  it('should load players from local storage', () => {
    MockInstance(
      LocalStorageService,
      'getItem',
      jest.fn().mockReturnValue(EXAMPLE_PLAYERS_JSON),
    );
    const actualPlayers = ngMocks.get(PlayersService).getPlayers();
    expect(actualPlayers).toEqual(EXAMPLE_PLAYERS);
  });

  it('should save players correctly and store it in local storage', () => {
    const playersService = ngMocks.get(PlayersService);
    const localStorageService = ngMocks.get(LocalStorageService);

    playersService.setPlayers(EXAMPLE_PLAYERS);
    const actualPlayers = playersService.getPlayers();

    expect(actualPlayers).toEqual(EXAMPLE_PLAYERS);
    expect(localStorageService.setItem).toHaveBeenCalledWith(
      PLAYERS_LOCAL_STORAGE_KEY,
      EXAMPLE_PLAYERS_JSON,
    );
  });

  it('should track human players balance store it in local storage', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      const playersService = ngMocks.get(PlayersService);
      playersService.setPlayers(EXAMPLE_PLAYERS);
      const humanPlayers$ = playersService.humanPlayers$;
      const updateBalance$ = cold('-a').pipe(
        tap(() => {
          EXAMPLE_HUMAN_PLAYER.balance = 400;
          EXAMPLE_HUMAN_PLAYER.balance = 300;
          EXAMPLE_HUMAN_PLAYER.balance = 250;
        }),
      );
      expectObservable(humanPlayers$);
      expectObservable(updateBalance$);
    });
    const localStorageService = ngMocks.get(LocalStorageService);
    const updatedJson: PlayerJson[] = [
      { isCPU: false, name: 'Player 1', balance: 250 },
      { isCPU: true },
    ];
    expect(localStorageService.setItem).toHaveBeenCalledTimes(2);
    expect(localStorageService.setItem).toHaveBeenLastCalledWith(
      PLAYERS_LOCAL_STORAGE_KEY,
      updatedJson,
    );
  });

  it('should emit players correctly', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      const playersService = ngMocks.get(PlayersService);
      const newHumanPlayer = new HumanPlayer('Player 2', 200);
      const newCPU = new CPUPlayer();

      const addPlayers$ = cold('-a-').pipe(
        tap(() => playersService.setPlayers(EXAMPLE_PLAYERS)),
      );
      const addNewHumanPlayer$ = cold('--a').pipe(
        tap(() => {
          playersService.setPlayers([...EXAMPLE_PLAYERS, newHumanPlayer]);
        }),
      );
      const addNewCPUPlayer$ = cold('--a').pipe(
        tap(() => {
          playersService.setPlayers([
            ...EXAMPLE_PLAYERS,
            newHumanPlayer,
            newCPU,
          ]);
        }),
      );

      const expectedPlayers$ = cold('ab(cd)', {
        a: [],
        b: EXAMPLE_PLAYERS,
        c: [...EXAMPLE_PLAYERS, newHumanPlayer],
        d: [...EXAMPLE_PLAYERS, newHumanPlayer, newCPU],
      });
      const expectedHumanPlayers$ = cold('abc', {
        a: [],
        b: [EXAMPLE_HUMAN_PLAYER],
        c: [EXAMPLE_HUMAN_PLAYER, newHumanPlayer],
      });
      const actualPlayers$ = playersService.players$;
      const actualHumanPlayers$ = playersService.humanPlayers$;

      expectObservable(addPlayers$);
      expectObservable(addNewHumanPlayer$);
      expectObservable(addNewCPUPlayer$);
      expectObservable(actualPlayers$).toEqual(expectedPlayers$);
      expectObservable(actualHumanPlayers$).toEqual(expectedHumanPlayers$);
    });
  });
});
