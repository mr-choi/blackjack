import { Injectable } from '@angular/core';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { endWith, from, ignoreElements, map, Observable } from 'rxjs';
import { Result } from '@model/result';
import { Hand } from '@model/hand';
import { HumanPlayer } from '@model/player';

@Injectable({
  providedIn: 'root',
})
export class DetermineResultsAndPayoutService {
  results$(fullPlayerHandLink: PlayerHandLink): Observable<PlayerHandLink> {
    const linkItems: PlayerHandLinkItem[] = [];
    let currentLink: PlayerHandLinkItem = fullPlayerHandLink.firstLinkItem;
    while (currentLink.getNext()) {
      // This way, the bank will be skipped, which is intentional
      linkItems.push(currentLink);
      // currentLink should not be null at that point
      currentLink = currentLink.getNext() as PlayerHandLinkItem;
    }
    const bankHand: Hand = fullPlayerHandLink.bankLinkItem.getHand();
    return from(linkItems).pipe(
      map((linkItem) => this.determineSingleHand(linkItem, bankHand)),
      map((linkItem) => this.performPayout(linkItem, bankHand.isBlackjack())),
      ignoreElements(),
      endWith(fullPlayerHandLink),
    );
  }

  private determineSingleHand(
    playerHandLinkItem: PlayerHandLinkItem,
    bankHand: Hand,
  ): PlayerHandLinkItem {
    if (
      playerHandLinkItem.hasSurrendered() ||
      playerHandLinkItem.getHand().getTotalCardValue().isBusted()
    ) {
      playerHandLinkItem.setResult(Result.LOSE);
      return playerHandLinkItem;
    }
    if (bankHand.isBlackjack()) {
      playerHandLinkItem.setResult(
        playerHandLinkItem.getHand().isBlackjack() ? Result.DRAW : Result.LOSE,
      );
      return playerHandLinkItem;
    }
    if (
      bankHand.getTotalCardValue().isBusted() ||
      playerHandLinkItem.getHand().isBlackjack()
    ) {
      playerHandLinkItem.setResult(Result.WIN);
      return playerHandLinkItem;
    }
    const playerValue = playerHandLinkItem
      .getHand()
      .getTotalCardValue()
      .getTotal();
    const bankValue = bankHand.getTotalCardValue().getTotal();

    playerHandLinkItem.setResult(
      playerValue > bankValue
        ? Result.WIN
        : playerValue === bankValue
        ? Result.DRAW
        : Result.LOSE,
    );

    return playerHandLinkItem;
  }

  private performPayout(
    playerHandLinkItem: PlayerHandLinkItem,
    bankHasBlackjack: boolean,
  ): PlayerHandLinkItem {
    if (!(playerHandLinkItem.getPlayer() instanceof HumanPlayer)) {
      return playerHandLinkItem;
    }
    const humanPlayer = playerHandLinkItem.getPlayer() as HumanPlayer;
    if (bankHasBlackjack && playerHandLinkItem.isInsured()) {
      humanPlayer.balance += (3 * playerHandLinkItem.getBet()) / 2;
      return playerHandLinkItem;
    }
    if (playerHandLinkItem.getResult() === Result.WIN) {
      const payout = playerHandLinkItem.getHand().isBlackjack() ? 3 / 2 : 1;
      humanPlayer.balance += (1 + payout) * playerHandLinkItem.getBet();
    }
    if (playerHandLinkItem.getResult() === Result.DRAW) {
      humanPlayer.balance += playerHandLinkItem.getBet();
    }
    return playerHandLinkItem;
  }
}
