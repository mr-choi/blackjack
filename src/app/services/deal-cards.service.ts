import { Injectable } from '@angular/core';
import { CardDrawService } from '@services/card-draw.service';
import {
  combineLatest,
  endWith,
  ignoreElements,
  map,
  MonoTypeOperatorFunction,
  Observable,
  pipe,
  shareReplay,
  switchMap,
  take,
} from 'rxjs';
import { BetsByPlayerService } from '@services/bets-by-player.service';
import { PlayerHandLink } from '@model/player-hand-link';
import { Hand } from '@model/hand';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import { PlayersService } from '@services/config/players.service';

@Injectable({
  providedIn: 'root',
})
export class DealCardsService {
  canDealCards$: Observable<boolean> = combineLatest({
    betsByPlayer: this.betsByPlayerService.betsByPLayer$,
    players: this.playersService.players$,
  }).pipe(
    map(
      ({ betsByPlayer, players }) =>
        players.length > 0 && !new Set(betsByPlayer.values()).has(null),
    ),
    shareReplay({ bufferSize: 1, refCount: false }),
  );

  deckId$: Observable<string> = this.cardDrawService.deckId$;

  constructor(
    private cardDrawService: CardDrawService,
    private betsByPlayerService: BetsByPlayerService,
    private playersService: PlayersService,
    private blackjackSettingsService: BlackjackSettingsService,
  ) {}

  private drawAndAddToHand(hand: Hand): MonoTypeOperatorFunction<string> {
    return switchMap((deckId) =>
      this.cardDrawService.drawCardFromDeck$(deckId).pipe(
        hand.addDrawnCardToHand(),
        map(() => deckId),
      ),
    );
  }

  private drawHoleCardToHand(hand: Hand): MonoTypeOperatorFunction<string> {
    return switchMap((deckId) =>
      this.cardDrawService.drawCardFromDeck$(deckId).pipe(
        hand.addDrawnCardAsHoleCard(),
        map(() => deckId),
      ),
    );
  }

  dealCards$(playerHandLink: PlayerHandLink): Observable<PlayerHandLink> {
    const hands: Hand[] = playerHandLink.getAllHandsInOrder();
    const drawCardOperators: MonoTypeOperatorFunction<string>[] = [
      ...hands,
      ...hands,
    ] // Each player draws two cards
      .slice(0, -1) // The bank however, draws only one card (and a hole card if configured)
      .map((hand) => this.drawAndAddToHand(hand));

    let dealCards$: Observable<string> = drawCardOperators.reduce(
      (acc$, operator) => acc$.pipe(operator),
      this.deckId$.pipe(this.putBackPreviouslyDealtCards()),
    );

    if (this.blackjackSettingsService.getSettings().dealerDrawsHoleCard) {
      dealCards$ = dealCards$.pipe(
        this.drawHoleCardToHand(playerHandLink.bankLinkItem.getHand()),
      );
    }

    return dealCards$.pipe(ignoreElements(), endWith(playerHandLink));
  }

  private putBackPreviouslyDealtCards(): MonoTypeOperatorFunction<string> {
    return pipe(
      take(1),
      switchMap((deckId) =>
        this.cardDrawService
          .putCardsBackToDeck$(deckId)
          .pipe(map(() => deckId)),
      ),
    );
  }
}
