import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { HumanPlayer } from '@model/player';
import { runMarbleTest } from '@util/test/run-marble-test';
import { PlayersService } from '@services/config/players.service';
import { BetsByPlayerService } from '@services/bets-by-player.service';
import { map, tap } from 'rxjs';
import { cloneDeep } from 'lodash-es';

describe('BetsByPlayerService', () => {
  MockInstance.scope();

  const player1 = new HumanPlayer('Player 1', 1000);
  const player2 = new HumanPlayer('Player 2', 2000);

  beforeEach(() => MockBuilder(BetsByPlayerService));

  it('should emits bets by player correctly', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        PlayersService,
        'humanPlayers$',
        cold('abcd-', {
          a: [],
          b: [player1, player2],
          c: [player2, player1],
          d: [player1],
        }),
      );
      const betsByPlayerService = ngMocks.get(BetsByPlayerService);

      const updateBet$ = cold('----a').pipe(
        tap(() => {
          betsByPlayerService.changeBetForPlayer(player1, 10);
          betsByPlayerService.changeBetForPlayer(player2, 20); // Does not exist at that time, should do nothing.
        }),
      );

      const expected$ = cold('ab-cd', {
        a: new Map<HumanPlayer, number | null>(),
        b: new Map<HumanPlayer, number | null>([
          [player1, null],
          [player2, null],
        ]),
        c: new Map<HumanPlayer, number | null>([[player1, null]]),
        d: new Map<HumanPlayer, number | null>([[player1, 10]]),
      });

      // The same instance of the map is emitted, so we have to deeply clone it.
      // Otherwise, all emitted values will be the latest map value.
      const actual$ = betsByPlayerService.betsByPLayer$.pipe(map(cloneDeep));
      const actualSecondSubscriber$ = betsByPlayerService.betsByPLayer$.pipe(
        map(cloneDeep),
      );

      expectObservable(updateBet$);
      expectObservable(actual$).toEqual(expected$);
      expectObservable(actualSecondSubscriber$).toEqual(expected$);
    });
  });
});
