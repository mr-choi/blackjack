import { DetermineResultsAndPayoutService } from './determine-results-and-payout.service';
import { MockInstance, ngMocks } from 'ng-mocks';
import { HumanPlayer, Player } from '@model/player';
import { Hand } from '@model/hand';
import { PlayCard } from '@model/play-card';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { runMarbleTest } from '@util/test/run-marble-test';
import { Result } from '@model/result';

describe('DetermineResultsAndPayoutService', () => {
  MockInstance.scope();
  let humanPlayer: HumanPlayer;
  let playerHand: Hand;
  let playerLinkItem: PlayerHandLinkItem;
  let bankHand: Hand;
  let entireLink: PlayerHandLink;

  beforeEach(() => {
    bankHand = new Hand(false);
    const bankLinkItem = new PlayerHandLinkItem(Player.BANK, bankHand, null);

    humanPlayer = new HumanPlayer('Player 1', 90);
    playerHand = new Hand(false);
    playerLinkItem = new PlayerHandLinkItem(
      humanPlayer,
      playerHand,
      bankLinkItem,
    );
    playerLinkItem.setBet(10);

    entireLink = new PlayerHandLink(playerLinkItem, bankLinkItem);

    return MockInstance(DetermineResultsAndPayoutService);
  });

  it('should win with 1:1 payout for scoring more than bank', () => {
    playerHand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));
    playerHand.addCard(
      PlayCard.createFromJson({ image: 'KING', value: 'KING' }),
    );

    bankHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    bankHand.addCard(PlayCard.createFromJson({ image: '8', value: '8' }));

    runResultsObservableAndAssert();

    expect(playerLinkItem.getResult()).toBe(Result.WIN);
    expect(humanPlayer.balance).toEqual(110);
  });

  it('should win with 3:2 payout for blackjack', () => {
    playerHand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));
    playerHand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));

    bankHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    bankHand.addCard(PlayCard.createFromJson({ image: '8', value: '8' }));
    bankHand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));

    runResultsObservableAndAssert();

    expect(playerLinkItem.getResult()).toBe(Result.WIN);
    expect(humanPlayer.balance).toEqual(115);
  });

  it('should push back bet in case of draw', () => {
    playerHand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));
    playerHand.addCard(PlayCard.createFromJson({ image: '7', value: '7' }));

    bankHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    bankHand.addCard(PlayCard.createFromJson({ image: '8', value: '8' }));

    runResultsObservableAndAssert();

    expect(playerLinkItem.getResult()).toBe(Result.DRAW);
    expect(humanPlayer.balance).toEqual(100);
  });

  it('should not change balance after loss', () => {
    playerHand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));
    playerHand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));

    bankHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    bankHand.addCard(PlayCard.createFromJson({ image: '8', value: '8' }));

    runResultsObservableAndAssert();

    expect(playerLinkItem.getResult()).toBe(Result.LOSE);
    expect(humanPlayer.balance).toEqual(90);
  });

  it('should lose bet if both player and bank bust', () => {
    playerHand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));
    playerHand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));
    playerHand.addCard(
      PlayCard.createFromJson({ image: 'KING', value: 'KING' }),
    );

    bankHand.addCard(PlayCard.createFromJson({ image: '9', value: '9' }));
    bankHand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));
    bankHand.addCard(PlayCard.createFromJson({ image: '8', value: '8' }));

    runResultsObservableAndAssert();

    expect(playerLinkItem.getResult()).toBe(Result.LOSE);
    expect(humanPlayer.balance).toEqual(90);
  });

  it('should payout insurance if bank has blackjack', () => {
    playerHand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));
    playerHand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));
    playerLinkItem.insure();

    bankHand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));
    bankHand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));

    runResultsObservableAndAssert();

    expect(playerLinkItem.getResult()).toBe(Result.LOSE);
    expect(humanPlayer.balance).toEqual(105);
  });

  function runResultsObservableAndAssert() {
    runMarbleTest(({ expectObservable, cold }) => {
      const determineResultsAndPayoutService = ngMocks.get(
        DetermineResultsAndPayoutService,
      );
      const expected$ = cold('(a|)', { a: entireLink });
      const actual$ = determineResultsAndPayoutService.results$(entireLink);
      expectObservable(actual$).toEqual(expected$);
    });
  }
});
