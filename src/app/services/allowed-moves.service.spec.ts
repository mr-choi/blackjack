import { AllowedMovesService } from './allowed-moves.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import { BlackjackSettings } from '@model/blackjack-settings';
import { DoubleDownRule } from '@model/double-down-rule';
import { DoubleDownTotalRule } from '@model/double-down-total-rule';
import { SplitRule } from '@model/split-rule';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { CPUPlayer, HumanPlayer, Player } from '@model/player';
import { Hand } from '@model/hand';
import { PlayCard } from '@model/play-card';
import { Move } from '@model/move';

describe('AllowedMovesService', () => {
  MockInstance.scope();

  const bankLinkItem = new PlayerHandLinkItem(
    Player.BANK,
    new Hand(false),
    null,
  );

  beforeEach(() => {
    return MockBuilder(AllowedMovesService);
  });

  it('should only allow hit if hand has only one card', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ALWAYS, false),
        maxHandsPerPlayer: 4,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: false,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: 'KING', value: 'KING' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves.size).toBe(1);
    expect(allowedMoves).toContain(Move.HIT);
  });

  it('should only allow stand if player has blackjack', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ALWAYS, false),
        maxHandsPerPlayer: 4,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: false,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: 'KING', value: 'KING' }));
    hand.addCard(PlayCard.createFromJson({ image: 'ACE', value: 'ACE' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves.size).toBe(1);
    expect(allowedMoves).toContain(Move.STAND);
  });

  it('should allow hit and stand if player has two cards', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(
          DoubleDownTotalRule.ONLY_10_11,
          false,
        ),
        maxHandsPerPlayer: 4,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: false,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: 'KING', value: 'KING' }));
    hand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves).toContain(Move.HIT);
    expect(allowedMoves).toContain(Move.STAND);
  });

  it('should allow double down and split if player is cpu and rules allow it', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ALWAYS, false),
        maxHandsPerPlayer: 4,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: false,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: '6', value: '6' }));
    hand.addCard(PlayCard.createFromJson({ image: '6', value: '6' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves).toContain(Move.DOUBLE_DOWN);
    expect(allowedMoves).toContain(Move.SPLIT);
  });

  it('should not allow double down and split if player cannot afford it', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ALWAYS, false),
        maxHandsPerPlayer: 4,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: false,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));
    hand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new HumanPlayer('Bankrupt', 0),
      hand,
      bankLinkItem,
    );
    currentLinkItem.setBet(10);
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves).not.toContain(Move.DOUBLE_DOWN);
    expect(allowedMoves).not.toContain(Move.SPLIT);
  });

  it('should not allow split if it would exceed maximum number of hands', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(
          DoubleDownTotalRule.ONLY_10_11,
          false,
        ),
        maxHandsPerPlayer: 1,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: false,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: '8', value: '8' }));
    hand.addCard(PlayCard.createFromJson({ image: '8', value: '8' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves).not.toContain(Move.SPLIT);
  });

  test.each([true, false])(
    'split different rank, but same value allowed is %p',
    (sameValueAllowed) => {
      MockInstance(
        BlackjackSettingsService,
        'getSettings',
        jest.fn().mockReturnValue({
          doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ALWAYS, false),
          maxHandsPerPlayer: 4,
          splitRule: sameValueAllowed
            ? SplitRule.SAME_VALUE
            : SplitRule.SAME_RANK,
          allowSurrender: false,
        } as BlackjackSettings),
      );
      const allowedMovesService = ngMocks.get(AllowedMovesService);

      const hand = new Hand(false);
      hand.addCard(PlayCard.createFromJson({ image: 'KING', value: 'KING' }));
      hand.addCard(PlayCard.createFromJson({ image: '10', value: '10' }));

      const currentLinkItem = new PlayerHandLinkItem(
        new CPUPlayer(),
        hand,
        bankLinkItem,
      );
      const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

      const allowedMoves = allowedMovesService.getAllowedMoves(
        currentLinkItem,
        entireLink,
      );
      sameValueAllowed
        ? expect(allowedMoves).toContain(Move.SPLIT)
        : expect(allowedMoves).not.toContain(Move.SPLIT);
    },
  );

  it('should not allow double down if hand is split and rule does not permit it', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ALWAYS, false),
        maxHandsPerPlayer: 4,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: false,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(true);
    hand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));
    hand.addCard(PlayCard.createFromJson({ image: '5', value: '5' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves).not.toContain(Move.DOUBLE_DOWN);
  });

  it('should allow surrender if conditions are met', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(
          DoubleDownTotalRule.ONLY_10_11,
          false,
        ),
        maxHandsPerPlayer: 1,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: true,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: '2', value: '2' }));
    hand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves).toContain(Move.SURRENDER);
  });

  it('should not allow surrender if player has 3 cards', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(
          DoubleDownTotalRule.ONLY_10_11,
          false,
        ),
        maxHandsPerPlayer: 1,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: true,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: '2', value: '2' }));
    hand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));
    hand.addCard(PlayCard.createFromJson({ image: '3', value: '3' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves).not.toContain(Move.SURRENDER);
  });

  it('should not allow surrender if player has split the hand', () => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        doubleDownRule: new DoubleDownRule(
          DoubleDownTotalRule.ONLY_10_11,
          false,
        ),
        maxHandsPerPlayer: 1,
        splitRule: SplitRule.SAME_VALUE,
        allowSurrender: true,
      } as BlackjackSettings),
    );
    const allowedMovesService = ngMocks.get(AllowedMovesService);

    const hand = new Hand(true);
    hand.addCard(PlayCard.createFromJson({ image: '2', value: '2' }));
    hand.addCard(PlayCard.createFromJson({ image: '4', value: '4' }));

    const currentLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      hand,
      bankLinkItem,
    );
    const entireLink = new PlayerHandLink(currentLinkItem, bankLinkItem);

    const allowedMoves = allowedMovesService.getAllowedMoves(
      currentLinkItem,
      entireLink,
    );
    expect(allowedMoves).not.toContain(Move.SURRENDER);
  });
});
