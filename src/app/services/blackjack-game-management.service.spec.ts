import {
  BlackjackGameManagementService,
  DELAY_AFTER_NON_HUMAN_LINK_ITEM,
  GamePhase,
} from './blackjack-game-management.service';
import { MockBuilder, MockInstance, ngMocks } from 'ng-mocks';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { HumanPlayer, Player } from '@model/player';
import { Hand } from '@model/hand';
import { PlayCard } from '@model/play-card';
import { PlayerHandLinkFactoryService } from '@services/player-hand-link-factory.service';
import { runMarbleTest } from '@util/test/run-marble-test';
import { BetsByPlayerService } from '@services/bets-by-player.service';
import { DealCardsService } from '@services/deal-cards.service';
import { InsuranceService } from '@services/insurance.service';
import { PerformSelectedMoveService } from '@services/perform-selected-move.service';
import { DetermineResultsAndPayoutService } from '@services/determine-results-and-payout.service';
import { concatWith, tap } from 'rxjs';

describe('BlackjackGameManagementService', () => {
  MockInstance.scope();

  let playerHandLink: PlayerHandLink;
  let playerLink: PlayerHandLinkItem;
  let player: HumanPlayer;
  let bankLink: PlayerHandLinkItem;
  let bankHand: Hand;
  let betsByPlayer: Map<HumanPlayer, number | null>;

  beforeEach(() => {
    bankHand = new Hand(false);
    bankHand.addCard(PlayCard.createFromJson({ value: 'KING', image: 'KING' }));
    bankLink = new PlayerHandLinkItem(Player.BANK, bankHand, null);
    player = new HumanPlayer('Player 1', 100);
    playerLink = new PlayerHandLinkItem(player, new Hand(false), bankLink);
    playerHandLink = new PlayerHandLink(playerLink, bankLink);
    betsByPlayer = new Map<HumanPlayer, number | null>([[player, 10]]);

    MockInstance(
      PlayerHandLinkFactoryService,
      'create',
      jest.fn().mockReturnValue(playerHandLink),
    );
    return MockBuilder(BlackjackGameManagementService);
  });

  it('should be able to perform game one at the time and reset', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: betsByPlayer }),
      );
      MockInstance(DealCardsService, 'canDealCards$', cold('a', { a: true }));
      MockInstance(
        DealCardsService,
        'dealCards$',
        jest.fn().mockReturnValue(cold('-(a|)', { a: playerHandLink })),
      );
      MockInstance(
        InsuranceService,
        'determineInsuranceIfApplicable$',
        jest.fn().mockReturnValue(cold('-(a|)', { a: playerHandLink })),
      );
      MockInstance(
        PerformSelectedMoveService,
        'perform$',
        jest.fn().mockReturnValue(cold('-(a|)')),
      );
      MockInstance(
        DetermineResultsAndPayoutService,
        'results$',
        jest.fn().mockReturnValue(cold('(a|)', { a: playerHandLink })),
      );

      const blackjackGameManagementService = ngMocks.get(
        BlackjackGameManagementService,
      );

      const expectedGame$ = cold<never>(
        `${DELAY_AFTER_NON_HUMAN_LINK_ITEM + 4}ms |`,
      );
      const expectedGameSubscribed1msLater$ = cold<never>(
        `${DELAY_AFTER_NON_HUMAN_LINK_ITEM + 3}ms |`,
      );
      const actualGame$ = blackjackGameManagementService.game$;

      const resetAfterGameFinish$ = blackjackGameManagementService.game$.pipe(
        concatWith(
          cold('-a').pipe(
            tap(() => blackjackGameManagementService.resetGame()),
          ),
        ),
      );

      // Note: (ab) is treated as 4 frames instead of 1 in rxjs marble testing
      const expectedGamePhase$ = cold<GamePhase>(
        `(ab) ${DELAY_AFTER_NON_HUMAN_LINK_ITEM}ms ca`,
        {
          a: 'BETTING',
          b: 'PLAYING',
          c: 'FINISHING',
        },
      );
      const actualGamePhase$ = blackjackGameManagementService.gamePhase$;

      const expectedCurrentLinkItem$ = cold(
        `a-bc ${DELAY_AFTER_NON_HUMAN_LINK_ITEM}ms a`,
        {
          a: null,
          b: playerLink,
          c: bankLink,
        },
      );
      const actualCurrentLinkItem$ =
        blackjackGameManagementService.currentLinkItem$;

      expectObservable(actualGame$).toEqual(expectedGame$);
      expectObservable(actualGame$, '-^').toEqual(
        expectedGameSubscribed1msLater$,
      );

      expectObservable(resetAfterGameFinish$);
      expectObservable(actualGamePhase$).toEqual(expectedGamePhase$);

      expectObservable(actualCurrentLinkItem$).toEqual(
        expectedCurrentLinkItem$,
      );
    });

    const dealCardsService = ngMocks.get(DealCardsService);
    const insuranceService = ngMocks.get(InsuranceService);
    const performSelectedMoveService = ngMocks.get(PerformSelectedMoveService);
    const determineResultsAndPayoutService = ngMocks.get(
      DetermineResultsAndPayoutService,
    );

    expect(player.balance).toBe(90);

    expect(dealCardsService.dealCards$).toHaveBeenCalledTimes(1);
    expect(
      insuranceService.determineInsuranceIfApplicable$,
    ).toHaveBeenCalledTimes(1);
    expect(performSelectedMoveService.perform$).toHaveBeenCalledTimes(2);
    expect(performSelectedMoveService.perform$).toHaveBeenNthCalledWith(
      1,
      playerLink,
      playerHandLink,
    );
    expect(performSelectedMoveService.perform$).toHaveBeenNthCalledWith(
      2,
      bankLink,
      playerHandLink,
    );
    expect(determineResultsAndPayoutService.results$).toHaveBeenCalledTimes(1);
  });

  it('should behave correctly if an error occurred during the game', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: betsByPlayer }),
      );
      MockInstance(DealCardsService, 'canDealCards$', cold('a', { a: true }));
      MockInstance(
        DealCardsService,
        'dealCards$',
        jest.fn().mockReturnValue(cold('-(a|)', { a: playerHandLink })),
      );
      MockInstance(
        InsuranceService,
        'determineInsuranceIfApplicable$',
        jest.fn().mockReturnValue(cold('-(a|)', { a: playerHandLink })),
      );
      MockInstance(
        PerformSelectedMoveService,
        'perform$',
        jest.fn().mockReturnValue(cold('--#')),
      );
      MockInstance(
        DetermineResultsAndPayoutService,
        'results$',
        jest.fn().mockReturnValue(cold('(a|)', { a: playerHandLink })),
      );

      const blackjackGameManagementService = ngMocks.get(
        BlackjackGameManagementService,
      );

      const expectedGame$ = cold<never>('4ms #');
      const expectedGameSubscribed1msLater$ = cold<never>('3ms #');
      const actualGame$ = blackjackGameManagementService.game$;

      // Note: (ab) is treated as 4 frames instead of 1 in rxjs marble testing
      const expectedGamePhase$ = cold<GamePhase>('(ab)c', {
        a: 'BETTING',
        b: 'PLAYING',
        c: 'FINISHING',
      });
      const actualGamePhase$ = blackjackGameManagementService.gamePhase$;

      const expectedCurrentLinkItem$ = cold('a-b-a', {
        a: null,
        b: playerLink,
      });
      const actualCurrentLinkItem$ =
        blackjackGameManagementService.currentLinkItem$;

      expectObservable(actualGame$).toEqual(expectedGame$);
      expectObservable(actualGame$, '-^').toEqual(
        expectedGameSubscribed1msLater$,
      );

      expectObservable(actualGamePhase$).toEqual(expectedGamePhase$);

      expectObservable(actualCurrentLinkItem$).toEqual(
        expectedCurrentLinkItem$,
      );
    });

    const dealCardsService = ngMocks.get(DealCardsService);
    const insuranceService = ngMocks.get(InsuranceService);
    const performSelectedMoveService = ngMocks.get(PerformSelectedMoveService);
    const determineResultsAndPayoutService = ngMocks.get(
      DetermineResultsAndPayoutService,
    );

    expect(player.balance).toBe(100);

    expect(dealCardsService.dealCards$).toHaveBeenCalledTimes(1);
    expect(
      insuranceService.determineInsuranceIfApplicable$,
    ).toHaveBeenCalledTimes(1);
    expect(performSelectedMoveService.perform$).toHaveBeenCalledTimes(1);
    expect(performSelectedMoveService.perform$).toHaveBeenNthCalledWith(
      1,
      playerLink,
      playerHandLink,
    );
    expect(determineResultsAndPayoutService.results$).not.toHaveBeenCalled();
  });

  it('should not execute game if not able to deal cards', () => {
    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: betsByPlayer }),
      );
      MockInstance(DealCardsService, 'canDealCards$', cold('a', { a: false }));
      MockInstance(
        DealCardsService,
        'dealCards$',
        jest.fn().mockReturnValue(cold('-(a|)', { a: playerHandLink })),
      );
      MockInstance(
        InsuranceService,
        'determineInsuranceIfApplicable$',
        jest.fn().mockReturnValue(cold('-(a|)', { a: playerHandLink })),
      );
      MockInstance(
        PerformSelectedMoveService,
        'perform$',
        jest.fn().mockReturnValue(cold('-(a|)')),
      );
      MockInstance(
        DetermineResultsAndPayoutService,
        'results$',
        jest.fn().mockReturnValue(cold('(a|)', { a: playerHandLink })),
      );

      const blackjackGameManagementService = ngMocks.get(
        BlackjackGameManagementService,
      );

      const expectedGame$ = cold<never>('|');
      const actualGame$ = blackjackGameManagementService.game$;

      expectObservable(actualGame$).toEqual(expectedGame$);
    });

    const dealCardsService = ngMocks.get(DealCardsService);
    const insuranceService = ngMocks.get(InsuranceService);
    const performSelectedMoveService = ngMocks.get(PerformSelectedMoveService);
    const determineResultsAndPayoutService = ngMocks.get(
      DetermineResultsAndPayoutService,
    );

    expect(player.balance).toBe(100);

    expect(dealCardsService.dealCards$).not.toHaveBeenCalled();
    expect(
      insuranceService.determineInsuranceIfApplicable$,
    ).not.toHaveBeenCalled();
    expect(performSelectedMoveService.perform$).not.toHaveBeenCalled();
    expect(determineResultsAndPayoutService.results$).not.toHaveBeenCalled();
  });

  it('should skip performing moves if dealer has hole card blackjack', () => {
    const blackjackBankHand = new Hand(false);
    blackjackBankHand.addCard(
      PlayCard.createFromJson({ value: 'ACE', image: 'ACE' }),
    );
    blackjackBankHand.addHoleCard(
      PlayCard.createFromJson({ value: '10', image: '10' }),
    );
    bankLink.setHand(blackjackBankHand);

    runMarbleTest(({ cold, expectObservable }) => {
      MockInstance(
        BetsByPlayerService,
        'betsByPLayer$',
        cold('a', { a: betsByPlayer }),
      );
      MockInstance(DealCardsService, 'canDealCards$', cold('a', { a: true }));
      MockInstance(
        DealCardsService,
        'dealCards$',
        jest.fn().mockReturnValue(cold('--(a|)', { a: playerHandLink })),
      );
      MockInstance(
        InsuranceService,
        'determineInsuranceIfApplicable$',
        jest.fn().mockReturnValue(cold('--(a|)', { a: playerHandLink })),
      );
      MockInstance(
        PerformSelectedMoveService,
        'perform$',
        jest.fn().mockReturnValue(cold('--(a|)')),
      );
      MockInstance(
        DetermineResultsAndPayoutService,
        'results$',
        jest.fn().mockReturnValue(cold('(a|)', { a: playerHandLink })),
      );

      const blackjackGameManagementService = ngMocks.get(
        BlackjackGameManagementService,
      );

      const expectedGame$ = cold<never>('4ms |');
      const expectedGameSubscribed1msLater$ = cold<never>('3ms |');
      const actualGame$ = blackjackGameManagementService.game$;

      const resetAfterGameFinish$ = blackjackGameManagementService.game$.pipe(
        concatWith(
          cold('-a').pipe(
            tap(() => blackjackGameManagementService.resetGame()),
          ),
        ),
      );

      // Note: (ab) is treated as 4 frames instead of 1 in rxjs marble testing
      const expectedGamePhase$ = cold<GamePhase>('(ab)ca', {
        a: 'BETTING',
        b: 'PLAYING',
        c: 'FINISHING',
      });
      const actualGamePhase$ = blackjackGameManagementService.gamePhase$;

      const expectedCurrentLinkItem$ = cold('a', {
        a: null,
      });
      const actualCurrentLinkItem$ =
        blackjackGameManagementService.currentLinkItem$;

      expectObservable(actualGame$).toEqual(expectedGame$);
      expectObservable(actualGame$, '-^').toEqual(
        expectedGameSubscribed1msLater$,
      );

      expectObservable(resetAfterGameFinish$);
      expectObservable(actualGamePhase$).toEqual(expectedGamePhase$);

      expectObservable(actualCurrentLinkItem$).toEqual(
        expectedCurrentLinkItem$,
      );
    });

    const dealCardsService = ngMocks.get(DealCardsService);
    const insuranceService = ngMocks.get(InsuranceService);
    const performSelectedMoveService = ngMocks.get(PerformSelectedMoveService);
    const determineResultsAndPayoutService = ngMocks.get(
      DetermineResultsAndPayoutService,
    );

    expect(player.balance).toBe(90);

    expect(dealCardsService.dealCards$).toHaveBeenCalledTimes(1);
    expect(
      insuranceService.determineInsuranceIfApplicable$,
    ).toHaveBeenCalledTimes(1);
    expect(performSelectedMoveService.perform$).not.toHaveBeenCalled();
    expect(determineResultsAndPayoutService.results$).toHaveBeenCalledTimes(1);
  });
});
