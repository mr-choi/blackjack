import { PlayerHandLinkItem } from '@model/player-hand-link';
import { defer, map, Observable, of, tap } from 'rxjs';
import { PlayCard } from '@model/play-card';
import { HumanPlayer } from '@model/player';
import { Hand } from '@model/hand';

export abstract class Move {
  // The name field is there to ensure that assertEquals treats different moves different.
  constructor(private readonly name: string) {}
  /**
   * Returns observable that performs the move upon subscription.
   * @param playerHandLinkItem The player and hand for which the move should be performed
   * @param cardSource$ A source observable to draw cards from.
   * @returns An observable that emits a boolean when the move is finished. Emits true if the player can do another
   * move, false otherwise.
   */
  abstract perform$(
    playerHandLinkItem: PlayerHandLinkItem,
    cardSource$: Observable<PlayCard>,
  ): Observable<boolean>;

  static readonly STAND = new (class extends Move {
    perform$(): Observable<boolean> {
      return of(false);
    }
  })('STAND');

  static readonly HIT = new (class extends Move {
    override perform$(
      playerHandLinkItem: PlayerHandLinkItem,
      cardSource$: Observable<PlayCard>,
    ): Observable<boolean> {
      return cardSource$.pipe(
        playerHandLinkItem.getHand().addDrawnCardToHand(),
        map(() => !playerHandLinkItem.getHand().getTotalCardValue().isBusted()),
      );
    }
  })('HIT');

  static readonly DOUBLE_DOWN = new (class extends Move {
    perform$(
      playerHandLinkItem: PlayerHandLinkItem,
      cardSource$: Observable<PlayCard>,
    ): Observable<boolean> {
      return cardSource$.pipe(
        tap({
          subscribe: () => {
            const player = playerHandLinkItem.getPlayer();
            if (player instanceof HumanPlayer) {
              player.balance -= playerHandLinkItem.getBet();
              playerHandLinkItem.doubleBet();
            }
          },
        }),
        playerHandLinkItem.getHand().addDrawnCardToHand(),
        map(() => false),
      );
    }
  })('DOUBLE_DOWN');

  static readonly SPLIT = new (class extends Move {
    override perform$(
      playerHandLinkItem: PlayerHandLinkItem,
      cardSource$: Observable<PlayCard>,
    ): Observable<boolean> {
      return defer(() => {
        const cards: PlayCard[] = playerHandLinkItem.getHand().getCards();
        const hand1 = new Hand(true);
        hand1.addCard(cards[0]);
        const hand2 = new Hand(true);
        hand2.addCard(cards[1]);

        playerHandLinkItem.setHand(hand1);
        const newLink = playerHandLinkItem.insertNewHand(hand2);

        const player = playerHandLinkItem.getPlayer();
        if (player instanceof HumanPlayer) {
          player.balance -= playerHandLinkItem.getBet();
          newLink.setBet(playerHandLinkItem.getBet());
        }
        return cardSource$;
      }).pipe(
        playerHandLinkItem.getHand().addDrawnCardToHand(),
        map(() => true),
      );
    }
  })('SPLIT');

  static readonly SURRENDER = new (class extends Move {
    override perform$(
      playerHandLinkItem: PlayerHandLinkItem,
    ): Observable<boolean> {
      return of(false).pipe(
        tap(() => {
          playerHandLinkItem.surrender();
          const player = playerHandLinkItem.getPlayer();
          if (player instanceof HumanPlayer) {
            player.balance += playerHandLinkItem.getBet() / 2;
          }
        }),
      );
    }
  })('SURRENDER');
}
