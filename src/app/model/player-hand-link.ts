import { Player } from '@model/player';
import { Hand } from '@model/hand';
import { BehaviorSubject, merge, Observable, Subject } from 'rxjs';
import { Result } from '@model/result';

export class PlayerHandLink {
  readonly newHandInserted$: Observable<void>;

  constructor(
    public readonly firstLinkItem: PlayerHandLinkItem,
    public readonly bankLinkItem: PlayerHandLinkItem,
  ) {
    this.newHandInserted$ = this.getNewHandInserted$(firstLinkItem);
  }

  getAllHandsInOrder(): Hand[] {
    let linkItem: PlayerHandLinkItem | null = this.firstLinkItem;
    const result: Hand[] = [];
    while (linkItem) {
      result.push(linkItem.getHand());
      linkItem = linkItem.getNext();
    }
    return result;
  }

  getAllHandsByPlayer(): Map<Player, Hand[]> {
    let linkItem: PlayerHandLinkItem | null = this.firstLinkItem;
    const result = new Map<Player, Hand[]>();
    while (linkItem) {
      if (!result.has(linkItem.getPlayer()))
        result.set(linkItem.getPlayer(), []);
      result.get(linkItem.getPlayer())?.push(linkItem.getHand());
      linkItem = linkItem.getNext();
    }
    return result;
  }

  private getNewHandInserted$(firstLinkItem: PlayerHandLinkItem) {
    const newHandInsertedObservables: Observable<void>[] = [];
    for (
      let linkItem: PlayerHandLinkItem | null = firstLinkItem;
      linkItem;
      linkItem = linkItem?.getNext()
    ) {
      newHandInsertedObservables.push(linkItem.newHandInserted$);
    }
    return merge(...newHandInsertedObservables);
  }
}

export class PlayerHandLinkItem {
  private newHandInserted$$ = new Subject<void>();
  readonly newHandInserted$ = this.newHandInserted$$.asObservable();

  private bet$$ = new BehaviorSubject(0);
  readonly bet$ = this.bet$$.asObservable();

  private surrendered$$ = new BehaviorSubject(false);
  readonly surrendered$ = this.surrendered$$.asObservable();

  private insured$$ = new BehaviorSubject(false);
  readonly insured$ = this.insured$$.asObservable();

  // Hand will be set in constructor and should be non-null when trying to query hand after initialization.
  private hand$$ = new BehaviorSubject<Hand | null>(
    null,
  ) as BehaviorSubject<Hand>;
  readonly hand$ = this.hand$$.asObservable();

  private result$$ = new BehaviorSubject<Result | null>(null);
  readonly result$ = this.result$$.asObservable();

  constructor(
    private player: Player,
    hand: Hand,
    private next: PlayerHandLinkItem | null,
  ) {
    this.hand$$.next(hand);
  }

  getBet(): number {
    return this.bet$$.getValue();
  }

  setBet(bet: number) {
    this.bet$$.next(bet);
  }

  doubleBet() {
    this.bet$$.next(2 * this.bet$$.getValue());
  }

  surrender() {
    this.surrendered$$.next(true);
  }

  hasSurrendered(): boolean {
    return this.surrendered$$.getValue();
  }

  insure() {
    this.insured$$.next(true);
  }

  isInsured(): boolean {
    return this.insured$$.getValue();
  }

  insertNewHand(hand: Hand): PlayerHandLinkItem {
    this.next = new PlayerHandLinkItem(this.player, hand, this.next);
    this.newHandInserted$$.next();
    return this.next;
  }

  getPlayer(): Player {
    return this.player;
  }

  getHand(): Hand {
    return this.hand$$.getValue();
  }

  setHand(hand: Hand) {
    return this.hand$$.next(hand);
  }

  getNext(): PlayerHandLinkItem | null {
    return this.next;
  }

  getResult(): Result | null {
    return this.result$$.getValue();
  }

  setResult(result: Result) {
    this.result$$.next(result);
  }
}
