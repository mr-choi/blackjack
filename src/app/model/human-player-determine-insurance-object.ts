import { Observable, Subject, take } from 'rxjs';
import { HumanPlayer } from '@model/player';

export class HumanPlayerDetermineInsuranceObject {
  private readonly wantsInsurance$$ = new Subject<boolean>();
  readonly wantsInsurance$: Observable<boolean> = this.wantsInsurance$$.pipe(
    take(1),
  );

  constructor(public readonly humanPlayer: HumanPlayer) {}

  setWantsInsurance(value: boolean) {
    this.wantsInsurance$$.next(value);
  }
}
