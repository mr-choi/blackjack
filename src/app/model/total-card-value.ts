import { PlayCard } from '@model/play-card';

export class TotalCardValue {
  constructor(
    private total: number,
    private soft: boolean = false,
  ) {}

  getTotal(): number {
    return this.total;
  }

  isSoft(): boolean {
    return this.soft;
  }

  isBusted(): boolean {
    return this.total > 21;
  }

  addCardToTotal(card: PlayCard) {
    this.total += card.getCardValue().total;
    this.soft ||= card.getCardValue().soft;
    if (this.total > 21 && this.soft) {
      this.total -= 10;
      this.soft = false;
    }
  }
}
