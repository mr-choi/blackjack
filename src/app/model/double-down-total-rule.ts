import { Hand } from './hand';

export abstract class DoubleDownTotalRule {
  static readonly ALWAYS = new (class extends DoubleDownTotalRule {
    override doubleDownAllowed(hand: Hand): boolean {
      return hand.getCards().length === 2;
    }
  })('ALWAYS', 'Any total');
  static readonly ONLY_9_10_11 = new (class extends DoubleDownTotalRule {
    override doubleDownAllowed(hand: Hand): boolean {
      if (hand.getCards().length !== 2) return false;
      const total = hand.getTotalCardValue().getTotal();
      return total === 9 || total === 10 || total === 11;
    }
  })('ONLY_9_10_11', 'Only for 9, 10 and 11');
  static readonly ONLY_10_11 = new (class extends DoubleDownTotalRule {
    override doubleDownAllowed(hand: Hand): boolean {
      if (hand.getCards().length !== 2) return false;
      const total = hand.getTotalCardValue().getTotal();
      return total === 10 || total === 11;
    }
  })('ONLY_10_11', 'Only for 10 and 11');

  static readonly allByName = new Map<string, DoubleDownTotalRule>([
    ['ALWAYS', DoubleDownTotalRule.ALWAYS],
    ['ONLY_9_10_11', DoubleDownTotalRule.ONLY_9_10_11],
    ['ONLY_10_11', DoubleDownTotalRule.ONLY_10_11],
  ]);

  constructor(
    public readonly name: string,
    public readonly description: string,
  ) {}

  abstract doubleDownAllowed(hand: Hand): boolean;
}
