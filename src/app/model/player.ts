import { BehaviorSubject } from 'rxjs';
import { STARTING_BALANCE } from '@services/config/players.service';

export abstract class Player {
  protected constructor(public readonly type: 'human' | 'cpu' | 'bank') {}

  abstract accept<PARAMETER_TYPE, RETURN_TYPE>(
    visitor: PlayerVisitor<PARAMETER_TYPE, RETURN_TYPE>,
    parameter: PARAMETER_TYPE,
  ): RETURN_TYPE;

  static readonly BANK = new (class extends Player {
    accept<PARAMETER_TYPE, RETURN_TYPE>(
      visitor: PlayerVisitor<PARAMETER_TYPE, RETURN_TYPE>,
      parameter: PARAMETER_TYPE,
    ): RETURN_TYPE {
      return visitor.visitBank(this, parameter);
    }
  })('bank');
}

export interface PlayerVisitor<PARAMETER_TYPE, RETURN_TYPE> {
  visitHuman(humanPlayer: HumanPlayer, parameter: PARAMETER_TYPE): RETURN_TYPE;
  visitCPU(cpuPlayer: CPUPlayer, parameter: PARAMETER_TYPE): RETURN_TYPE;
  visitBank(bank: typeof Player.BANK, parameter: PARAMETER_TYPE): RETURN_TYPE;
}

export class HumanPlayer extends Player {
  private balance$$ = new BehaviorSubject<number>(STARTING_BALANCE);
  balance$ = this.balance$$.asObservable();

  get balance() {
    return this.balance$$.getValue();
  }

  set balance(balance: number) {
    this.balance$$.next(balance);
  }

  constructor(
    public name: string,
    balance: number,
  ) {
    super('human');
    this.balance$$.next(balance);
  }

  accept<PARAMETER_TYPE, RETURN_TYPE>(
    visitor: PlayerVisitor<PARAMETER_TYPE, RETURN_TYPE>,
    parameter: PARAMETER_TYPE,
  ): RETURN_TYPE {
    return visitor.visitHuman(this, parameter);
  }
}

export class CPUPlayer extends Player {
  constructor() {
    super('cpu');
  }

  accept<PARAMETER_TYPE, RETURN_TYPE>(
    visitor: PlayerVisitor<PARAMETER_TYPE, RETURN_TYPE>,
    parameter: PARAMETER_TYPE,
  ): RETURN_TYPE {
    return visitor.visitCPU(this, parameter);
  }
}

export interface PlayerJson {
  isCPU: boolean;
  name?: string;
  balance?: number;
}
