import { DoubleDownTotalRule } from '@model/double-down-total-rule';
import { Hand } from '@model/hand';

export class DoubleDownRule {
  constructor(
    public readonly doubleDownTotalRule: DoubleDownTotalRule,
    public readonly doubleDownAfterSplitAllowed: boolean,
  ) {}

  doubleDownAllowed(hand: Hand): boolean {
    return (
      this.doubleDownTotalRule.doubleDownAllowed(hand) &&
      (this.doubleDownAfterSplitAllowed || !hand.split)
    );
  }
}
