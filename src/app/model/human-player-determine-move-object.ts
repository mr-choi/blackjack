import { filter, Observable, Subject, take } from 'rxjs';
import { Move } from '@model/move';
import { PlayerHandLinkItem } from '@model/player-hand-link';

export class HumanPlayerDetermineMoveObject {
  private readonly selectedMove$$ = new Subject<Move>();

  readonly determinedMove$: Observable<Move> = this.selectedMove$$.pipe(
    filter((move) => this.allowedMoves.has(move)),
    take(1),
  );

  constructor(
    public readonly playerHandLinkItem: PlayerHandLinkItem,
    public readonly allowedMoves: Set<Move>,
  ) {}

  select(move: Move) {
    this.selectedMove$$.next(move);
  }
}
