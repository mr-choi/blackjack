export class Result {
  static readonly WIN = new (class extends Result {})('WIN!');
  static readonly DRAW = new (class extends Result {})('DRAW');
  static readonly LOSE = new (class extends Result {})('LOSE!');

  private constructor(public readonly message: string) {}
}
