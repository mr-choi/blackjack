import { Hand } from '@model/hand';

export abstract class SplitRule {
  static readonly SAME_VALUE = new (class extends SplitRule {
    override splitAllowed(hand: Hand): boolean {
      const cards = hand.getCards();
      return (
        cards.length === 2 &&
        cards[0].getCardValue().getTotal() ===
          cards[1].getCardValue().getTotal()
      );
    }
  })('SAME_VALUE', 'Same value');
  static readonly SAME_RANK = new (class extends SplitRule {
    override splitAllowed(hand: Hand): boolean {
      const cards = hand.getCards();
      return cards.length === 2 && cards[0].rank === cards[1].rank;
    }
  })('SAME_RANK', 'Same rank');

  static readonly allByName = new Map<string, SplitRule>([
    ['SAME_VALUE', SplitRule.SAME_VALUE],
    ['SAME_RANK', SplitRule.SAME_RANK],
  ]);

  constructor(
    public readonly name: string,
    public readonly description: string,
  ) {}

  abstract splitAllowed(hand: Hand): boolean;
}
