import { SplitRule } from '@model/split-rule';
import { DoubleDownRule } from '@model/double-down-rule';

export interface BlackjackSettings {
  minimumBet: number;
  numberOfDecks: number;
  dealerDrawsHoleCard: boolean;
  dealerHitsSoft17: boolean;
  allowSurrender: boolean;
  doubleDownRule: DoubleDownRule;
  splitRule: SplitRule;
  maxHandsPerPlayer: number;
}

export interface BlackJackSettingsJson {
  minimumBet: number;
  numberOfDecks: number;
  dealerDrawsHoleCard: boolean;
  dealerHitsSoft17: boolean;
  allowSurrender: boolean;
  doubleDownRule: {
    doubleDownTotalRule: string;
    doubleDownAfterSplitAllowed: boolean;
  };
  splitRule: string;
  maxHandsPerPlayer: number;
}
