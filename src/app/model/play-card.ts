import { TotalCardValue } from '@model/total-card-value';

type CardRank =
  | 'ACE'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9'
  | '10'
  | 'JACK'
  | 'QUEEN'
  | 'KING';

const RANK_VALUES_MAP = new Map<CardRank, TotalCardValue>([
  ['ACE', new TotalCardValue(11, true)],
  ['2', new TotalCardValue(2)],
  ['3', new TotalCardValue(3)],
  ['4', new TotalCardValue(4)],
  ['5', new TotalCardValue(5)],
  ['6', new TotalCardValue(6)],
  ['7', new TotalCardValue(7)],
  ['8', new TotalCardValue(8)],
  ['9', new TotalCardValue(9)],
  ['10', new TotalCardValue(10)],
  ['JACK', new TotalCardValue(10)],
  ['QUEEN', new TotalCardValue(10)],
  ['KING', new TotalCardValue(10)],
]);

export class PlayCard {
  private constructor(
    public readonly image: string,
    public readonly rank: CardRank,
  ) {}

  static createFromJson(json: CardJson) {
    return new PlayCard(json.image, json.value);
  }

  getCardValue(): TotalCardValue {
    return RANK_VALUES_MAP.get(this.rank) ?? new TotalCardValue(0);
  }
}

export interface CardJson {
  image: string;
  value: CardRank;
}
