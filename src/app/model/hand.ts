import { PlayCard } from '@model/play-card';
import { TotalCardValue } from '@model/total-card-value';
import {
  BehaviorSubject,
  map,
  MonoTypeOperatorFunction,
  Observable,
  pipe,
  take,
  tap,
} from 'rxjs';
import { minimumDelay } from '@util/minimum-delay';

export const MINIMUM_DELAY_PENDING_CARD = 1000;

export class Hand {
  private cards$$ = new BehaviorSubject<PlayCard[]>([]);
  readonly cards$ = this.cards$$.asObservable();

  private totalCardValue$$ = new BehaviorSubject(new TotalCardValue(0));
  readonly totalCardValue$ = this.totalCardValue$$.asObservable();

  private newCardPending$$ = new BehaviorSubject(false);
  readonly newCardPending$ = this.newCardPending$$.asObservable();

  private holeCard$$ = new BehaviorSubject<PlayCard | null>(null);
  readonly hasHoleCard$: Observable<boolean> = this.holeCard$$.pipe(
    map((holeCard) => !!holeCard),
  );

  constructor(public readonly split: boolean) {}

  getCards(): PlayCard[] {
    return this.cards$$.getValue();
  }

  getTotalCardValue(): TotalCardValue {
    return this.totalCardValue$$.getValue();
  }

  isBlackjack(lookAtHoleCard = false): boolean {
    if (
      lookAtHoleCard &&
      this.cards$$.getValue().length === 1 &&
      this.holeCard$$.getValue()
    ) {
      return (
        this.totalCardValue$$.getValue().getTotal() +
          (this.holeCard$$.getValue()?.getCardValue().getTotal() ?? 0) ===
        21
      );
    }
    return (
      !this.split &&
      this.cards$$.getValue().length === 2 &&
      this.totalCardValue$$.getValue().getTotal() === 21
    );
  }

  addCard(card: PlayCard) {
    this.cards$$.getValue().push(card);
    this.totalCardValue$$.getValue().addCardToTotal(card);
    this.cards$$.next(this.cards$$.getValue());
    this.totalCardValue$$.next(this.totalCardValue$$.getValue());
  }

  addHoleCard(card: PlayCard) {
    this.holeCard$$.next(card);
  }

  revealHoleCard() {
    const holeCard = this.holeCard$$.getValue();
    if (holeCard) {
      this.holeCard$$.next(null);
      this.addCard(holeCard);
    }
  }

  addDrawnCardAsHoleCard(): MonoTypeOperatorFunction<PlayCard> {
    return pipe(
      take(1),
      minimumDelay(MINIMUM_DELAY_PENDING_CARD),
      tap({
        subscribe: () => this.newCardPending$$.next(true),
        next: (card) => {
          this.holeCard$$.next(card);
          this.newCardPending$$.next(false);
        },
      }),
    );
  }

  addDrawnCardToHand(): MonoTypeOperatorFunction<PlayCard> {
    return pipe(
      take(1),
      minimumDelay(MINIMUM_DELAY_PENDING_CARD),
      tap({
        subscribe: () => this.newCardPending$$.next(true),
        next: (card) => {
          this.addCard(card);
          this.newCardPending$$.next(false);
        },
      }),
    );
  }
}
