import { PlayersFormComponent } from './players-form.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import {
  PlayersService,
  STARTING_BALANCE,
} from '@services/config/players.service';
import { PlayersFormModule } from '@components/pages/blackjack-settings-page/players-form/players-form/players-form.module';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { CPUPlayer, HumanPlayer } from '@model/player';
import { Button } from 'primeng/button';
import Mock = jest.Mock;

describe('PlayersFormComponent', () => {
  MockInstance.scope();

  const humanPlayer = new HumanPlayer('Player 1', 123);
  const cpuPlayer = new CPUPlayer();

  beforeEach(() => {
    MockInstance(
      PlayersService,
      'getPlayers',
      jest.fn().mockReturnValue([humanPlayer, cpuPlayer]),
    );
    MockInstance(PlayersService, 'setPlayers', jest.fn());
    MockInstance(ConfirmationService, 'confirm', jest.fn());
    MockInstance(DialogService, 'open', jest.fn());
    return MockBuilder(PlayersFormComponent, [
      PlayersFormModule,
      ConfirmationService,
      DialogService,
    ]);
  });

  it('should show italic text of no players if no players are created', () => {
    MockInstance(PlayersService, 'getPlayers', jest.fn().mockReturnValue([]));
    MockRender(PlayersFormComponent);

    ngMocks.find('#no-players-configured-text');
  });

  it('should open dialog when adding players', () => {
    MockRender(PlayersFormComponent);

    const addPlayerButton = ngMocks.findInstance(
      '#players-form-add-player',
      Button,
    );
    addPlayerButton.onClick.emit();

    const dialogService = ngMocks.get(DialogService);
    expect(dialogService.open).toHaveBeenCalled();

    const config = (dialogService.open as Mock).mock.calls[0][1];
    expect(config.header).toBe('New player');
    expect(config.data.type).toBe(null);
    expect(config.data.name).toBe(null);
    expect(config.data.balance).toBe(STARTING_BALANCE);
    expect(config.data.controlIndex).toBeUndefined();
  });

  it('should be able to move players correctly', () => {
    const fixture = MockRender(PlayersFormComponent);

    const moveUpButtons = ngMocks.findAll('.players-form-move-up');
    const moveDownButtons = ngMocks.findAll('.players-form-move-down');
    const playerNameLabels = ngMocks.findAll('.players-form-player-name-label');

    expect(moveUpButtons).toHaveLength(2);
    expect(moveDownButtons).toHaveLength(2);
    expect(playerNameLabels).toHaveLength(2);

    const humanPlayerLabel = ngMocks.formatText(playerNameLabels[0]);
    const cpuPlayerLabel = ngMocks.formatText(playerNameLabels[1]);

    expect(humanPlayerLabel).toBe('Player 1');
    expect(cpuPlayerLabel).toBe('CPU');

    expect(moveUpButtons[0].nativeElement.disabled).toBe(true);
    expect(moveDownButtons[1].nativeElement.disabled).toBe(true);

    ngMocks.click(moveUpButtons[1]);
    fixture.detectChanges();

    let labelElementsAfterSwitch = ngMocks.findAll(
      '.players-form-player-name-label',
    );
    expect(ngMocks.formatText(labelElementsAfterSwitch[0])).toBe(
      cpuPlayerLabel,
    );
    expect(ngMocks.formatText(labelElementsAfterSwitch[1])).toBe(
      humanPlayerLabel,
    );

    const playersService = ngMocks.get(PlayersService);
    expect(playersService.setPlayers).toHaveBeenLastCalledWith([
      cpuPlayer,
      humanPlayer,
    ]);

    ngMocks.click(ngMocks.find('.players-form-move-down'));
    fixture.detectChanges();

    labelElementsAfterSwitch = ngMocks.findAll(
      '.players-form-player-name-label',
    );
    expect(ngMocks.formatText(labelElementsAfterSwitch[0])).toBe(
      humanPlayerLabel,
    );
    expect(ngMocks.formatText(labelElementsAfterSwitch[1])).toBe(
      cpuPlayerLabel,
    );
    expect(playersService.setPlayers).toHaveBeenLastCalledWith([
      humanPlayer,
      cpuPlayer,
    ]);
  });

  it('should open dialog when editing players', () => {
    MockRender(PlayersFormComponent);

    const firstEditPlayerButton = ngMocks.find('.players-form-edit');
    ngMocks.click(firstEditPlayerButton);

    const dialogService = ngMocks.get(DialogService);
    expect(dialogService.open).toHaveBeenCalled();

    const config = (dialogService.open as Mock).mock.calls[0][1];
    expect(config.header).toBe('Edit player');
    expect(config.data.type).toBe('human');
    expect(config.data.name).toBe('Player 1');
    expect(config.data.balance).toBe(123);
    expect(config.data.controlIndex).toBe(0);
  });

  it('should ask for confirmation when deleting players', () => {
    const fixture = MockRender(PlayersFormComponent);

    const firstDeletePlayerButton = ngMocks.find('.players-form-delete');
    ngMocks.click(firstDeletePlayerButton);

    const confirmationService = ngMocks.get(ConfirmationService);
    expect(confirmationService.confirm).toHaveBeenCalled();

    const confirmation = (confirmationService.confirm as Mock).mock.calls[0][0];
    expect(confirmation.header).toBe(
      "Are you sure you want to delete 'Player 1'?",
    );

    confirmation.accept();
    fixture.detectChanges();

    const playerNameLabels = ngMocks.findAll('.players-form-player-name-label');
    expect(playerNameLabels).toHaveLength(1);
    expect(ngMocks.formatText(playerNameLabels[0])).toBe('CPU');

    const playersService = ngMocks.get(PlayersService);
    expect(playersService.setPlayers).toHaveBeenLastCalledWith([cpuPlayer]);
  });
});
