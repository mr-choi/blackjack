import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DestroyRef,
  OnInit,
} from '@angular/core';
import {
  ControlsOf,
  FormArray,
  FormControl,
  FormGroup,
} from '@ngneat/reactive-forms';
import {
  PlayersService,
  STARTING_BALANCE,
} from '@services/config/players.service';
import { CPUPlayer, HumanPlayer, Player } from '@model/player';
import { ValidationErrors, Validators } from '@angular/forms';
import {
  faAdd,
  faArrowDown,
  faArrowUp,
  faPencil,
  faRobot,
  faTrash,
  faUser,
} from '@fortawesome/free-solid-svg-icons';
import { isNull } from 'lodash-es';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { SinglePlayerDialogFormComponent } from '@components/pages/blackjack-settings-page/players-form/players-form/single-player-dialog-form/single-player-dialog-form.component';
import { map, share } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

export type PlayerFormItem = {
  type: 'human' | 'cpu' | null;
  name?: string | null;
  balance?: number;
};

@Component({
  selector: 'app-players-form',
  templateUrl: './players-form.component.html',
  styleUrls: ['./players-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService], // There is also one in the root injector, however this component needs to be injected into the dialog.
})
export class PlayersFormComponent implements OnInit {
  readonly form = new FormGroup({
    players: new FormArray<PlayerFormItem>([]),
  });

  readonly addNewPlayerIcon = faAdd;
  readonly moveUpIcon = faArrowUp;
  readonly moveDownIcon = faArrowDown;
  readonly editIcon = faPencil;
  readonly deleteIcon = faTrash;
  readonly humanPlayerIcon = faUser;
  readonly cpuPlayerIcon = faRobot;

  constructor(
    private playersService: PlayersService,
    private confirmationService: ConfirmationService,
    private changeDetectorRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private destroyRef: DestroyRef,
  ) {}

  ngOnInit() {
    this.updatePlayers();
  }

  updatePlayers() {
    const players: Player[] = this.playersService.getPlayers();
    this.form.controls.players.clear();
    for (const player of players) {
      if (player instanceof CPUPlayer) {
        const control = this.createPlayerFormGroup('cpu');
        this.form.controls.players.push(control);
      }
      if (player instanceof HumanPlayer) {
        this.form.controls.players.push(
          this.createPlayerFormGroup('human', player.name, player.balance),
        );
      }
    }
  }

  submitPlayers() {
    const players: Player[] = this.form.value.players.map((formValue) => {
      if (formValue.type === 'human') {
        // For human players, name and balance should be defined
        return new HumanPlayer(formValue.name!.trim(), formValue.balance!);
      }
      return new CPUPlayer();
    });
    this.playersService.setPlayers(players);

    // When this method is called from the dialog component, it also changed the state of this component.
    // This makes sure the changes are visible.
    this.changeDetectorRef.markForCheck();
  }

  addPlayer() {
    this.dialogService.open(SinglePlayerDialogFormComponent, {
      width: '720px',
      header: 'New player',
      contentStyle: { 'overflow-y': 'visible' },
      data: {
        type: null,
        name: null,
        balance: STARTING_BALANCE,
      },
    });
  }

  editPlayer(index: number) {
    const playerFormValue = this.form.controls.players.at(index).value;
    this.dialogService.open(SinglePlayerDialogFormComponent, {
      width: '720px',
      header: 'Edit player',
      contentStyle: { 'overflow-y': 'visible' },
      data: {
        type: playerFormValue.type,
        name: playerFormValue.name,
        balance: playerFormValue.balance,
        controlIndex: index,
      },
    });
  }

  movePlayerUp(index: number) {
    if (index < 1 || index >= this.form.controls.players.controls.length) {
      return;
    }
    const controlToMove = this.form.controls.players.controls[index];
    this.form.controls.players.removeAt(index);
    this.form.controls.players.insert(index - 1, controlToMove);

    this.submitPlayers();
  }

  movePlayerDown(index: number) {
    this.movePlayerUp(index + 1);
  }

  deletePlayer(index: number) {
    const name =
      this.form.value.players[index].type === 'cpu'
        ? 'CPU'
        : this.form.value.players[index].name;
    this.confirmationService.confirm({
      header: `Are you sure you want to delete '${name}'?`,
      message: 'This action cannot be undone.',
      accept: () => {
        this.form.controls.players.removeAt(index);
        this.submitPlayers();
        this.changeDetectorRef.markForCheck();
      },
    });
  }

  createPlayerFormGroup(
    type: 'human' | 'cpu' | null,
    name: string | null = null,
    balance: number = STARTING_BALANCE,
    indexOfControlToIgnore?: number,
  ): FormGroup<ControlsOf<PlayerFormItem>> {
    const typeControl = new FormControl(type, [Validators.required]);
    const nameControl = new FormControl(name, [
      Validators.required,
      Validators.pattern('.*\\S+.*'),
      (control) => this.nameIsUnique(control.value, indexOfControlToIgnore),
    ]);
    const balanceControl = new FormControl(balance, [Validators.required]);

    const nameAndBalanceEnabled$ = typeControl.value$.pipe(
      takeUntilDestroyed(this.destroyRef),
      map((type) => type === 'human'),
      share(),
    );

    nameControl.enabledWhile(nameAndBalanceEnabled$);
    balanceControl.enabledWhile(nameAndBalanceEnabled$);

    return new FormGroup<ControlsOf<PlayerFormItem>>({
      type: typeControl,
      name: nameControl,
      balance: balanceControl,
    });
  }

  private getExistingHumanPlayerNamesInForm(
    indexOfControlToIgnore?: number,
  ): Set<string | null | undefined> {
    const existingNames = this.form.value.players
      .filter(
        (formValue, index) =>
          formValue.type == 'human' && index !== indexOfControlToIgnore,
      )
      .map((formValue) => formValue.name?.trim());

    return new Set(existingNames);
  }

  private nameIsUnique(
    name: string | null,
    indexOfControlToIgnore?: number,
  ): ValidationErrors | null {
    if (isNull(name)) {
      return null;
    }
    return this.getExistingHumanPlayerNamesInForm(indexOfControlToIgnore).has(
      name.trim(),
    )
      ? {
          nameIsUnique: true,
        }
      : null;
  }
}
