import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayersFormComponent } from './players-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { StyleClassModule } from 'primeng/styleclass';
import { TooltipModule } from 'primeng/tooltip';
import { DividerModule } from 'primeng/divider';
import { SinglePlayerDialogFormComponent } from '@components/pages/blackjack-settings-page/players-form/players-form/single-player-dialog-form/single-player-dialog-form.component';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { IfInvalidChangedTouchedDirective } from '@components/common/directives/if-invalid-changed-touched.directive';
import { InputNumberModule } from 'primeng/inputnumber';

@NgModule({
  declarations: [PlayersFormComponent, SinglePlayerDialogFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ButtonModule,
    FontAwesomeModule,
    StyleClassModule,
    TooltipModule,
    DividerModule,
    DropdownModule,
    InputTextModule,
    IfInvalidChangedTouchedDirective,
    InputNumberModule,
  ],
  exports: [PlayersFormComponent],
})
export class PlayersFormModule {}
