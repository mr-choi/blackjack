import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ControlsOf, FormGroup } from '@ngneat/reactive-forms';
import {
  PlayerFormItem,
  PlayersFormComponent,
} from '@components/pages/blackjack-settings-page/players-form/players-form/players-form.component';
import { faBan, faFloppyDisk } from '@fortawesome/free-solid-svg-icons';
import { isUndefined } from 'lodash-es';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-single-player-dialog-form',
  templateUrl: './single-player-dialog-form.component.html',
  styleUrls: ['./single-player-dialog-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SinglePlayerDialogFormComponent {
  readonly form: FormGroup<ControlsOf<PlayerFormItem>>;

  readonly typeOptions: { label: string; value: 'human' | 'cpu' }[] = [
    {
      label: 'Human',
      value: 'human',
    },
    {
      label: 'CPU',
      value: 'cpu',
    },
  ];

  readonly controlIndex?: number;

  readonly saveIcon = faFloppyDisk;
  readonly cancelIcon = faBan;

  constructor(
    private dynamicDialogRef: DynamicDialogRef,
    private playersFormComponent: PlayersFormComponent,
    private messageService: MessageService,
    dynamicDialogConfig: DynamicDialogConfig,
  ) {
    this.controlIndex = dynamicDialogConfig.data.controlIndex;
    this.form = playersFormComponent.createPlayerFormGroup(
      dynamicDialogConfig.data.type,
      dynamicDialogConfig.data.name,
      dynamicDialogConfig.data.balance,
      this.controlIndex,
    );
  }

  cancel() {
    this.dynamicDialogRef.close();
  }

  savePlayer() {
    const trimmedName = this.form.value.name?.trim() ?? null;
    if (trimmedName !== null) {
      this.form.controls.name?.setValue(trimmedName);
    }
    if (isUndefined(this.controlIndex)) {
      this.playersFormComponent.form.controls.players.push(this.form);
    } else {
      this.playersFormComponent.form.controls.players.removeAt(
        this.controlIndex,
      );
      this.playersFormComponent.form.controls.players.insert(
        this.controlIndex,
        this.form,
      );
    }
    this.playersFormComponent.submitPlayers();
    this.messageService.add({
      summary: 'Player successfully saved',
      severity: 'success',
    });
    this.dynamicDialogRef.close();
  }
}
