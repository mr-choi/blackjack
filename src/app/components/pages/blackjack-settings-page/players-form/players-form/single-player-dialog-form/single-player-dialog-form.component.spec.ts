import { SinglePlayerDialogFormComponent } from './single-player-dialog-form.component';
import {
  MockBuilder,
  MockInstance,
  MockProvider,
  MockRender,
  ngMocks,
} from 'ng-mocks';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import {
  PlayerFormItem,
  PlayersFormComponent,
} from '@components/pages/blackjack-settings-page/players-form/players-form/players-form.component';
import {
  ControlsOf,
  FormArray,
  FormControl,
  FormGroup,
} from '@ngneat/reactive-forms';
import { STARTING_BALANCE } from '@services/config/players.service';
import { MessageService } from 'primeng/api';
import { ChangeDetectorRef } from '@angular/core';
import { Button } from 'primeng/button';

describe('SinglePlayerDialogFormComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    MockInstance(DynamicDialogRef, 'close', jest.fn());
    MockInstance(
      PlayersFormComponent,
      'createPlayerFormGroup',
      jest.fn(
        (type, name, balance) =>
          new FormGroup<ControlsOf<PlayerFormItem>>({
            type: new FormControl(type),
            name: new FormControl(name),
            balance: new FormControl(balance),
          }),
      ),
    );
    MockInstance(PlayersFormComponent, 'submitPlayers', jest.fn());
    MockInstance(
      PlayersFormComponent,
      'form',
      new FormGroup({
        players: new FormArray([]),
      }),
    );
    MockInstance(MessageService, 'add', jest.fn());
    MockInstance(DynamicDialogConfig, 'data', {
      type: null,
      name: null,
      balance: STARTING_BALANCE,
    });
    return MockBuilder(SinglePlayerDialogFormComponent, [
      MessageService,
      DynamicDialogRef,
      DynamicDialogConfig,
    ]).provide(MockProvider(PlayersFormComponent));
  });

  it('should show additional inputs if type is human', () => {
    const fixture = MockRender(SinglePlayerDialogFormComponent);

    expect(ngMocks.findAll('.form-field-row')).toHaveLength(1);

    ngMocks.change(ngMocks.find('#single-player-form-type'), 'human');
    const changeDetectorRef = ngMocks.get(fixture.point, ChangeDetectorRef);
    changeDetectorRef.markForCheck();
    fixture.detectChanges();

    expect(ngMocks.findAll('.form-field-row')).toHaveLength(3);
  });

  it('should be able to save a new player', () => {
    const fixture = MockRender(SinglePlayerDialogFormComponent);

    ngMocks.change(ngMocks.find('#single-player-form-type'), 'human');

    const changeDetectorRef = ngMocks.get(fixture.point, ChangeDetectorRef);
    changeDetectorRef.markForCheck();
    fixture.detectChanges();

    ngMocks.change(ngMocks.find('#single-player-form-name'), 'Player 1');
    ngMocks.change(ngMocks.find('#single-player-form-balance'), 123);

    changeDetectorRef.markForCheck();
    fixture.detectChanges();

    const saveButton = ngMocks.find<Button>('#single-player-form-save');
    expect(saveButton.componentInstance.disabled).toBe(false);

    saveButton.componentInstance.onClick.emit();

    const playersFormComponent = ngMocks.get(PlayersFormComponent);
    const messageService = ngMocks.get(MessageService);
    const dynamicDialogRef = ngMocks.get(DynamicDialogRef);

    expect(playersFormComponent.submitPlayers).toHaveBeenCalled();
    expect(messageService.add).toHaveBeenCalled();
    expect(dynamicDialogRef.close).toHaveBeenCalled();

    expect(playersFormComponent.form.controls.players.controls).toHaveLength(1);

    const newPlayerControl =
      playersFormComponent.form.controls.players.controls[0];
    expect(newPlayerControl.value.type).toBe('human');
    expect(newPlayerControl.value.name).toBe('Player 1');
    expect(newPlayerControl.value.balance).toBe(123);
  });

  it('should be able to edit an existing player', () => {
    MockInstance(DynamicDialogConfig, 'data', {
      type: 'human',
      name: 'Player 1',
      balance: 500,
      controlIndex: 0,
    });
    MockInstance(
      PlayersFormComponent,
      'form',
      new FormGroup({
        players: new FormArray([
          new FormGroup<ControlsOf<PlayerFormItem>>({
            type: new FormControl('human'),
            name: new FormControl('Player 1'),
            balance: new FormControl(500),
          }),
        ]),
      }),
    );

    const fixture = MockRender(SinglePlayerDialogFormComponent);

    expect(fixture.point.componentInstance.form.value.type).toBe('human');
    expect(fixture.point.componentInstance.form.value.name).toBe('Player 1');
    expect(fixture.point.componentInstance.form.value.balance).toBe(500);

    ngMocks.change(ngMocks.find('#single-player-form-name'), 'Player 2');
    ngMocks.change(ngMocks.find('#single-player-form-balance'), 321);

    const changeDetectorRef = ngMocks.get(fixture.point, ChangeDetectorRef);
    changeDetectorRef.markForCheck();
    fixture.detectChanges();

    const saveButton = ngMocks.find<Button>('#single-player-form-save');
    expect(saveButton.componentInstance.disabled).toBe(false);

    saveButton.componentInstance.onClick.emit();

    const playersFormComponent = ngMocks.get(PlayersFormComponent);
    const messageService = ngMocks.get(MessageService);
    const dynamicDialogRef = ngMocks.get(DynamicDialogRef);

    expect(playersFormComponent.submitPlayers).toHaveBeenCalled();
    expect(messageService.add).toHaveBeenCalled();
    expect(dynamicDialogRef.close).toHaveBeenCalled();

    expect(playersFormComponent.form.controls.players.controls).toHaveLength(1);

    const newPlayerControl =
      playersFormComponent.form.controls.players.controls[0];
    expect(newPlayerControl.value.type).toBe('human');
    expect(newPlayerControl.value.name).toBe('Player 2');
    expect(newPlayerControl.value.balance).toBe(321);
  });
});
