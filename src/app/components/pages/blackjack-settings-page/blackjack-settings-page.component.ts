import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LeftButtonTopBar } from '@components/common/top-bar/top-bar.component';
import { faArrowLeft, faWarning } from '@fortawesome/free-solid-svg-icons';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-blackjack-settings-page',
  templateUrl: './blackjack-settings-page.component.html',
  styleUrls: ['./blackjack-settings-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [MessageService],
})
export class BlackjackSettingsPageComponent {
  readonly LEFT_BUTTON_TOP_BAR: LeftButtonTopBar = {
    label: 'Back',
    icon: faArrowLeft,
    urlToNavigate: '/',
  };
  protected readonly faWarning = faWarning;
}
