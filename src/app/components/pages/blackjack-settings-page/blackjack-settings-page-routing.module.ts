import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlackjackSettingsPageComponent } from './blackjack-settings-page.component';
import { UnsavedChangesService } from '@services/util/unsaved-changes.service';

const routes: Routes = [
  {
    title: 'Blackjack - Settings',
    path: '',
    component: BlackjackSettingsPageComponent,
    canDeactivate: [() => inject(UnsavedChangesService).canDeactivate()],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlackjackSettingsPageRoutingModule {}
