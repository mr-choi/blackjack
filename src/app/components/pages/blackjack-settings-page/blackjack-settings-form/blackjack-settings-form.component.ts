import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  OnInit,
} from '@angular/core';
import {
  BlackjackSettingsService,
  DEFAULT_BLACKJACK_SETTINGS,
} from '@services/config/blackjack-settings.service';
import { FormControl, FormGroup, ValuesOf } from '@ngneat/reactive-forms';
import { Validators } from '@angular/forms';
import { BlackjackSettings } from '@model/blackjack-settings';
import { DoubleDownTotalRule } from '@model/double-down-total-rule';
import { SplitRule } from '@model/split-rule';
import {
  faBan,
  faFloppyDisk,
  faRotateLeft,
} from '@fortawesome/free-solid-svg-icons';
import { DoubleDownRule } from '@model/double-down-rule';
import { ConfirmationService, MessageService } from 'primeng/api';
import { debounceTime, filter, startWith } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { UnsavedChangesService } from '@services/util/unsaved-changes.service';
import { isEqual } from 'lodash-es';

@Component({
  selector: 'app-blackjack-settings-form',
  templateUrl: './blackjack-settings-form.component.html',
  styleUrls: ['./blackjack-settings-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlackjackSettingsFormComponent implements OnInit {
  readonly minNumberOfDecks = 1;
  readonly maxNumberOfDecks = 8;
  readonly allDoubleDownTotalRules = [
    DoubleDownTotalRule.ALWAYS,
    DoubleDownTotalRule.ONLY_9_10_11,
    DoubleDownTotalRule.ONLY_10_11,
  ];
  readonly allSplitRules = [SplitRule.SAME_VALUE, SplitRule.SAME_RANK];

  readonly form = new FormGroup({
    minimumBet: new FormControl<number>(DEFAULT_BLACKJACK_SETTINGS.minimumBet, [
      Validators.required,
      Validators.min(1),
    ]),
    numberOfDecks: new FormControl<number>(
      DEFAULT_BLACKJACK_SETTINGS.numberOfDecks,
      [
        Validators.required,
        Validators.min(this.minNumberOfDecks),
        Validators.max(this.maxNumberOfDecks),
      ],
    ),
    dealerDrawsHoleCard: new FormControl<boolean>(
      DEFAULT_BLACKJACK_SETTINGS.dealerDrawsHoleCard,
      [Validators.required],
    ),
    dealerHitsSoft17: new FormControl<boolean>(
      DEFAULT_BLACKJACK_SETTINGS.dealerHitsSoft17,
      [Validators.required],
    ),
    allowSurrender: new FormControl<boolean>(
      DEFAULT_BLACKJACK_SETTINGS.allowSurrender,
      [Validators.required],
    ),
    doubleDownTotalRule: new FormControl<DoubleDownTotalRule>(
      DEFAULT_BLACKJACK_SETTINGS.doubleDownRule.doubleDownTotalRule,
      [Validators.required],
    ),
    doubleDownAfterSplitAllowed: new FormControl<boolean>(
      DEFAULT_BLACKJACK_SETTINGS.doubleDownRule.doubleDownAfterSplitAllowed,
      [Validators.required],
    ),
    splitRule: new FormControl<SplitRule>(
      DEFAULT_BLACKJACK_SETTINGS.splitRule,
      [Validators.required],
    ),
    maxHandsPerPlayer: new FormControl<number | null>(
      DEFAULT_BLACKJACK_SETTINGS.maxHandsPerPlayer,
      [
        Validators.required,
        Validators.min(2),
        Validators.max(Number.MAX_SAFE_INTEGER),
      ],
    ),
  });

  readonly cancelIcon = faBan;
  readonly restoreIcon = faRotateLeft;
  readonly saveIcon = faFloppyDisk;

  private latestPristineFormValue?: ValuesOf<typeof this.form.controls>;

  constructor(
    private blackjackSettingsService: BlackjackSettingsService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private destroyRef: DestroyRef,
    private unsavedChangesService: UnsavedChangesService,
  ) {}

  ngOnInit() {
    this.updateFormValues();
    this.form.dirty$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        filter((dirty) => !dirty),
        startWith(false),
      )
      .subscribe(() => {
        this.latestPristineFormValue = this.form.value;
      });
    this.form.value$
      .pipe(takeUntilDestroyed(this.destroyRef), debounceTime(0))
      .subscribe((value) => {
        this.unsavedChangesService.setUnsavedSettingsChanges(
          !isEqual(this.latestPristineFormValue, value),
        );
      });
  }

  saveSettings() {
    this.blackjackSettingsService.setSettings({
      minimumBet: this.form.value.minimumBet,
      numberOfDecks: this.form.value.numberOfDecks,
      dealerDrawsHoleCard: this.form.value.dealerDrawsHoleCard,
      dealerHitsSoft17: this.form.value.dealerHitsSoft17,
      allowSurrender: this.form.value.allowSurrender,
      doubleDownRule: new DoubleDownRule(
        this.form.value.doubleDownTotalRule,
        this.form.value.doubleDownAfterSplitAllowed,
      ),
      splitRule: this.form.value.splitRule,
      maxHandsPerPlayer:
        this.form.value.maxHandsPerPlayer ??
        DEFAULT_BLACKJACK_SETTINGS.maxHandsPerPlayer,
    });
    this.reset();
    this.messageService.add({
      summary: 'Settings successfully saved',
      severity: 'success',
    });
  }

  restoreDefaultSettings() {
    this.confirmationService.confirm({
      message: 'This action cannot be undone.',
      header: 'Are you sure you want to restore the default settings?',
      accept: () => {
        this.blackjackSettingsService.resetSettings();
        this.form.markAllAsDirty();
        this.reset();
      },
    });
  }

  reset() {
    this.updateFormValues();
    this.form.markAsPristine();
    this.form.markAsUntouched();
  }

  private updateFormValues() {
    const blackjackSettings: BlackjackSettings =
      this.blackjackSettingsService.getSettings();
    this.form.setValue({
      minimumBet: blackjackSettings.minimumBet,
      numberOfDecks: blackjackSettings.numberOfDecks,
      dealerDrawsHoleCard: blackjackSettings.dealerDrawsHoleCard,
      dealerHitsSoft17: blackjackSettings.dealerHitsSoft17,
      allowSurrender: blackjackSettings.allowSurrender,
      doubleDownTotalRule: blackjackSettings.doubleDownRule.doubleDownTotalRule,
      doubleDownAfterSplitAllowed:
        blackjackSettings.doubleDownRule.doubleDownAfterSplitAllowed,
      splitRule: blackjackSettings.splitRule,
      maxHandsPerPlayer: blackjackSettings.maxHandsPerPlayer,
    });
  }
}
