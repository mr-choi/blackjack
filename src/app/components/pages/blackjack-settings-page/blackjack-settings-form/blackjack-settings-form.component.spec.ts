import { BlackjackSettingsFormComponent } from './blackjack-settings-form.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { DoubleDownTotalRule } from '@model/double-down-total-rule';
import { SplitRule } from '@model/split-rule';
import { Button } from 'primeng/button';
import {
  BlackjackSettingsService,
  DEFAULT_BLACKJACK_SETTINGS,
} from '@services/config/blackjack-settings.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DoubleDownRule } from '@model/double-down-rule';
import { AppModule } from '@app/app.module';
import { BlackjackSettingsFormModule } from '@components/pages/blackjack-settings-page/blackjack-settings-form/blackjack-settings-form.module';
import { BlackjackSettingsPageModule } from '@components/pages/blackjack-settings-page/blackjack-settings-page.module';
import { UnsavedChangesService } from '@services/util/unsaved-changes.service';
import { fakeAsync, tick } from '@angular/core/testing';
import Mock = jest.Mock;

describe('BlackjackSettingsFormComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue(DEFAULT_BLACKJACK_SETTINGS),
    );
    MockInstance(BlackjackSettingsService, 'setSettings', jest.fn());
    MockInstance(BlackjackSettingsService, 'resetSettings', jest.fn());
    MockInstance(MessageService, 'add', jest.fn());
    MockInstance(ConfirmationService, 'confirm', jest.fn());
    MockInstance(UnsavedChangesService, 'setUnsavedSettingsChanges', jest.fn());
    return MockBuilder(BlackjackSettingsFormComponent, [
      AppModule,
      BlackjackSettingsFormModule,
      BlackjackSettingsPageModule,
    ]);
  });

  it('should be able to submit the form', () => {
    MockRender(BlackjackSettingsFormComponent);

    ngMocks.change(ngMocks.find('#settings-form-minimum-bet'), 5);
    ngMocks.change(ngMocks.find('#settings-form-number-of-decks'), 8);
    ngMocks.change(
      ngMocks.find('#settings-form-dealer-draws-hole-card'),
      false,
    );
    ngMocks.change(ngMocks.find('#settings-form-dealer-hits-soft-17'), true);
    ngMocks.change(ngMocks.find('#settings-form-allow-surrender'), false);
    ngMocks.change(
      ngMocks.find('#settings-form-double-down-total-rule'),
      DoubleDownTotalRule.ONLY_10_11,
    );
    ngMocks.change(
      ngMocks.find('#settings-form-double-down-after-split-allowed'),
      false,
    );
    ngMocks.change(
      ngMocks.find('#settings-form-split-rule'),
      SplitRule.SAME_RANK,
    );
    ngMocks.change(ngMocks.find('#settings-form-max-hands-per-player'), 2);

    const saveButton = ngMocks.findInstance('#settings-form-save', Button);
    saveButton.onClick.emit();

    const blackjackSettingsService = ngMocks.get(BlackjackSettingsService);
    expect(blackjackSettingsService.setSettings).toHaveBeenCalledWith({
      minimumBet: 5,
      numberOfDecks: 8,
      dealerDrawsHoleCard: false,
      dealerHitsSoft17: true,
      allowSurrender: false,
      doubleDownRule: new DoubleDownRule(DoubleDownTotalRule.ONLY_10_11, false),
      splitRule: SplitRule.SAME_RANK,
      maxHandsPerPlayer: 2,
    });

    const messageService = ngMocks.get(MessageService);
    expect(messageService.add).toHaveBeenCalled();
  });

  it('should be able to restore to default values', () => {
    MockRender(BlackjackSettingsFormComponent);

    const restoreDefaultsButton = ngMocks.findInstance(
      '#settings-form-restore-defaults',
      Button,
    );
    restoreDefaultsButton.onClick.emit();

    const confirmationService = ngMocks.get(ConfirmationService);
    expect(confirmationService.confirm).toHaveBeenCalled();
    (confirmationService.confirm as Mock).mock.calls[0][0].accept();

    const blackjackSettingsService = ngMocks.get(BlackjackSettingsService);
    expect(blackjackSettingsService.resetSettings).toHaveBeenCalled();
  });

  it('should be able to cancel form changes', () => {
    const fixture = MockRender(BlackjackSettingsFormComponent);

    ngMocks.change(ngMocks.find('#settings-form-minimum-bet'), 5);
    ngMocks.change(ngMocks.find('#settings-form-number-of-decks'), 8);
    ngMocks.change(
      ngMocks.find('#settings-form-dealer-draws-hole-card'),
      false,
    );
    ngMocks.change(ngMocks.find('#settings-form-dealer-hits-soft-17'), true);
    ngMocks.change(ngMocks.find('#settings-form-allow-surrender'), false);
    ngMocks.change(
      ngMocks.find('#settings-form-double-down-total-rule'),
      DoubleDownTotalRule.ONLY_10_11,
    );
    ngMocks.change(
      ngMocks.find('#settings-form-double-down-after-split-allowed'),
      false,
    );
    ngMocks.change(
      ngMocks.find('#settings-form-split-rule'),
      SplitRule.SAME_RANK,
    );
    ngMocks.change(ngMocks.find('#settings-form-max-hands-per-player'), 2);

    const cancelButton = ngMocks.findInstance('#settings-form-cancel', Button);
    cancelButton.onClick.emit();

    const blackjackSettingsService = ngMocks.get(BlackjackSettingsService);
    // One time during component initialization, the other after pressing the reset button:
    expect(blackjackSettingsService.getSettings).toHaveBeenCalledTimes(2);

    const formValue = fixture.point.componentInstance.form.value;
    expect(formValue.numberOfDecks).toBe(
      DEFAULT_BLACKJACK_SETTINGS.numberOfDecks,
    );
    expect(formValue.dealerDrawsHoleCard).toBe(true);
    expect(formValue.dealerHitsSoft17).toBe(false);
    expect(formValue.allowSurrender).toBe(true);
    expect(formValue.doubleDownTotalRule).toBe(DoubleDownTotalRule.ALWAYS);
    expect(formValue.doubleDownAfterSplitAllowed).toBe(true);
    expect(formValue.splitRule).toBe(SplitRule.SAME_VALUE);
    expect(formValue.maxHandsPerPlayer).toBe(Number.MAX_SAFE_INTEGER);
  });

  it('should correctly track if there are unsaved changes', fakeAsync(() => {
    MockRender(BlackjackSettingsFormComponent);

    const unsavedChangesService = ngMocks.get(UnsavedChangesService);
    tick();
    expect(
      unsavedChangesService.setUnsavedSettingsChanges,
    ).toHaveBeenLastCalledWith(false);

    const numberOfDecksFormFieldElement = ngMocks.find(
      '#settings-form-number-of-decks',
    );
    ngMocks.change(numberOfDecksFormFieldElement, 2);
    tick();
    expect(
      unsavedChangesService.setUnsavedSettingsChanges,
    ).toHaveBeenLastCalledWith(true);

    ngMocks.change(numberOfDecksFormFieldElement, 1);
    tick();
    expect(
      unsavedChangesService.setUnsavedSettingsChanges,
    ).toHaveBeenLastCalledWith(false);
  }));
});
