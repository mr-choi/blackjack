import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderModule } from 'primeng/slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputSwitchModule } from 'primeng/inputswitch';
import { DropdownModule } from 'primeng/dropdown';
import { BlackjackSettingsFormComponent } from '@components/pages/blackjack-settings-page/blackjack-settings-form/blackjack-settings-form.component';
import { ResplitControllerComponent } from './resplit-controller/resplit-controller.component';
import { PaginatorModule } from 'primeng/paginator';
import { ButtonModule } from 'primeng/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { UnsavedChangesTextHighlightDirective } from '@components/common/directives/unsaved-changes-text-highlight.directive';
import { ShowDirective } from '@components/common/directives/show.directive';
import { IfInvalidChangedTouchedDirective } from '@components/common/directives/if-invalid-changed-touched.directive';

@NgModule({
  declarations: [BlackjackSettingsFormComponent, ResplitControllerComponent],
  imports: [
    CommonModule,
    SliderModule,
    ReactiveFormsModule,
    InputSwitchModule,
    DropdownModule,
    FormsModule,
    PaginatorModule,
    ButtonModule,
    FontAwesomeModule,
    ConfirmDialogModule,
    UnsavedChangesTextHighlightDirective,
    ShowDirective,
    IfInvalidChangedTouchedDirective,
  ],
  exports: [BlackjackSettingsFormComponent],
})
export class BlackjackSettingsFormModule {}
