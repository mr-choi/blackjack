import { ResplitControllerComponent } from './resplit-controller.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { BlackjackSettingsFormModule } from '@components/pages/blackjack-settings-page/blackjack-settings-form/blackjack-settings-form.module';
import { Dropdown } from 'primeng/dropdown';
import { ChangeDetectorRef } from '@angular/core';
import { ShowDirective } from '@components/common/directives/show.directive';

describe('ResplitControllerComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(ResplitControllerComponent, BlackjackSettingsFormModule);
  });

  it('should show second field if resplit is set to limited', () => {
    const fixture = MockRender(ResplitControllerComponent);
    const component = fixture.point.componentInstance;
    const changeDetectorRef = ngMocks.get(fixture.point, ChangeDetectorRef);
    const resplitOptions = component.resplitOptions;

    const dropdown = ngMocks.find<Dropdown>('#resplit-controller-options');

    const showDirective = ngMocks.findInstance(
      '#resplit-controller-max-hands',
      ShowDirective,
    );

    ngMocks.change(dropdown, resplitOptions[0]);
    changeDetectorRef.markForCheck();
    fixture.detectChanges();
    expect(showDirective.appShow).toBe(false);

    ngMocks.change(dropdown, resplitOptions[1]);
    changeDetectorRef.markForCheck();
    fixture.detectChanges();
    expect(showDirective.appShow).toBe(false);

    ngMocks.change(dropdown, resplitOptions[2]);
    changeDetectorRef.markForCheck();
    fixture.detectChanges();
    expect(showDirective.appShow).toBe(true);
  });

  it('should set to correct resplit option based on external written value', () => {
    const fixture = MockRender(ResplitControllerComponent);
    const component = fixture.point.componentInstance;
    const resplitOptions = component.resplitOptions;

    component.writeValue(2);
    expect(component.form.value.resplitOption).toBe(resplitOptions[0]);
    expect(component.form.value.maxHands).toBe(null);

    component.writeValue(Number.MAX_SAFE_INTEGER);
    expect(component.form.value.resplitOption).toBe(resplitOptions[1]);
    expect(component.form.value.maxHands).toBe(null);

    component.writeValue(42);
    expect(component.form.value.resplitOption).toBe(resplitOptions[2]);
    expect(component.form.value.maxHands).toBe(42);

    component.writeValue(null);
    expect(component.form.value.resplitOption).toBe(resplitOptions[1]);
    expect(component.form.value.maxHands).toBe(null);
  });
});
