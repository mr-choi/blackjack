import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  OnInit,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  FormGroup,
} from '@ngneat/reactive-forms';
import { NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { filter, identity } from 'rxjs';

export type ResplitAllowedOption = {
  description: string;
  valueSupplier: () => number | null;
};

@Component({
  selector: 'app-resplit-controller',
  templateUrl: './resplit-controller.component.html',
  styleUrls: ['./resplit-controller.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ResplitControllerComponent,
      multi: true,
    },
  ],
})
export class ResplitControllerComponent
  extends ControlValueAccessor<number>
  implements OnInit
{
  readonly Number = Number;
  readonly resplitOptions: ResplitAllowedOption[] = [
    { description: 'Not allowed', valueSupplier: () => 2 },
    {
      description: 'Infinitely allowed',
      valueSupplier: () => Number.MAX_SAFE_INTEGER,
    },
    { description: 'Limited', valueSupplier: () => this.form.value.maxHands },
  ];
  readonly form = new FormGroup({
    resplitOption: new FormControl<ResplitAllowedOption>(
      this.resplitOptions[1],
    ),
    maxHands: new FormControl<number | null>(null, [
      Validators.required,
      Validators.min(2),
      Validators.max(Number.MAX_SAFE_INTEGER),
    ]),
  });

  constructor(private destroyRef: DestroyRef) {
    super();
  }

  ngOnInit(): void {
    this.form.valueChanges
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((resplitRule) => {
        if (this.onChange) {
          this.onChange(resplitRule.resplitOption.valueSupplier());
        }
      });
    this.form.touch$
      .pipe(takeUntilDestroyed(this.destroyRef), filter(identity))
      .subscribe(() => {
        if (this.onTouched) {
          this.onTouched();
        }
      });
  }

  writeValue(value: number | null): void {
    switch (value) {
      case 2:
        this.form.controls.resplitOption.setValue(this.resplitOptions[0]);
        this.form.controls.maxHands.setValue(null);
        break;
      case Number.MAX_SAFE_INTEGER:
      case null:
        this.form.controls.resplitOption.setValue(this.resplitOptions[1]);
        this.form.controls.maxHands.setValue(null);
        break;
      default:
        this.form.controls.resplitOption.setValue(this.resplitOptions[2]);
        this.form.controls.maxHands.setValue(value);
    }
    this.form.markAsPristine();
    this.form.markAsUntouched();
  }
}
