import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlackjackSettingsPageRoutingModule } from './blackjack-settings-page-routing.module';
import { BlackjackSettingsPageComponent } from './blackjack-settings-page.component';
import { TopBarComponent } from '@components/common/top-bar/top-bar.component';
import { FieldsetModule } from 'primeng/fieldset';
import { BlackjackSettingsFormModule } from './blackjack-settings-form/blackjack-settings-form.module';
import { ToastModule } from 'primeng/toast';
import { UnsavedChangesService } from '@services/util/unsaved-changes.service';
import { PlayersFormModule } from './players-form/players-form/players-form.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';

@NgModule({
  declarations: [BlackjackSettingsPageComponent],
  imports: [
    CommonModule,
    BlackjackSettingsPageRoutingModule,
    TopBarComponent,
    FieldsetModule,
    BlackjackSettingsFormModule,
    ToastModule,
    PlayersFormModule,
    ConfirmDialogModule,
    FontAwesomeModule,
  ],
  providers: [UnsavedChangesService, ConfirmationService, DialogService],
})
export class BlackjackSettingsPageModule {}
