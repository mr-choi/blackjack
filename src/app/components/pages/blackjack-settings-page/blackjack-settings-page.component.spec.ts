import { BlackjackSettingsPageComponent } from './blackjack-settings-page.component';
import { MockBuilder, MockInstance, MockRender } from 'ng-mocks';
import { BlackjackSettingsPageModule } from '@components/pages/blackjack-settings-page/blackjack-settings-page.module';

describe('BlackjackSettingsPageComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(
      BlackjackSettingsPageComponent,
      BlackjackSettingsPageModule,
    );
  });

  it('should create', () => {
    MockRender(BlackjackSettingsPageComponent);
  });
});
