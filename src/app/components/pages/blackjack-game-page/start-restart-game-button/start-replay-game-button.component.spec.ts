import { StartReplayGameButtonComponent } from './start-replay-game-button.component';
import {
  MockBuilder,
  MockInstance,
  MockProvider,
  MockRender,
  ngMocks,
} from 'ng-mocks';
import { BlackjackGamePageModule } from '@components/pages/blackjack-game-page/blackjack-game-page.module';
import {
  BlackjackGameManagementService,
  GamePhase,
} from '@services/blackjack-game-management.service';
import { of } from 'rxjs';
import { GameInProgressService } from '@services/util/game-in-progress.service';
import { DealCardsService } from '@services/deal-cards.service';
import { BET_FORM_CONTROLS_BY_PLAYER_NAME } from '@components/pages/blackjack-game-page/blackjack-game-page.component';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { Button } from 'primeng/button';

describe('StartRestartGameButtonComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    MockInstance(
      BET_FORM_CONTROLS_BY_PLAYER_NAME,
      () => new FormGroup<Record<string, FormControl<number | null>>>({}),
    );
    MockInstance(DealCardsService, 'canDealCards$', of(true));
    MockInstance(
      BlackjackGameManagementService,
      'gamePhase$',
      of<GamePhase>('BETTING'),
    );
    MockInstance(GameInProgressService, 'newGame', jest.fn());
    MockInstance(BlackjackGameManagementService, 'resetGame', jest.fn());
    return MockBuilder(
      StartReplayGameButtonComponent,
      BlackjackGamePageModule,
    ).provide(MockProvider(BET_FORM_CONTROLS_BY_PLAYER_NAME));
  });

  it('should start game when start button is pressed', () => {
    MockRender(StartReplayGameButtonComponent);

    const button = ngMocks.findInstance('#start-game-button', Button);
    button.onClick.emit();

    const gameInProgressService = ngMocks.get(GameInProgressService);
    expect(gameInProgressService.newGame).toHaveBeenCalled();
  });

  it('should reset game when replay button is pressed', () => {
    MockInstance(
      BlackjackGameManagementService,
      'gamePhase$',
      of<GamePhase>('FINISHING'),
    );
    MockRender(StartReplayGameButtonComponent);

    const button = ngMocks.findInstance('#replay-game-button', Button);
    button.onClick.emit();

    const blackjackGameManagementService = ngMocks.get(
      BlackjackGameManagementService,
    );
    expect(blackjackGameManagementService.resetGame).toHaveBeenCalled();
  });

  it('should show disabled start button if bet form is invalid', () => {
    MockInstance(
      BET_FORM_CONTROLS_BY_PLAYER_NAME,
      () =>
        new FormGroup<Record<string, FormControl<number | null>>>({}, () => ({
          error: true,
        })),
    );
    MockRender(StartReplayGameButtonComponent);

    const button = ngMocks.findInstance('#start-game-button', Button);
    expect(button.disabled).toBe(true);
  });

  it('should show disabled start button if unable to deal cards', () => {
    MockInstance(DealCardsService, 'canDealCards$', of(false));
    MockRender(StartReplayGameButtonComponent);

    const button = ngMocks.findInstance('#start-game-button', Button);
    expect(button.disabled).toBe(true);
  });

  it('should not show start and replay button if in playing phase', () => {
    MockInstance(
      BlackjackGameManagementService,
      'gamePhase$',
      of<GamePhase>('PLAYING'),
    );
    MockRender(StartReplayGameButtonComponent);

    const startButtons = ngMocks.findInstances('#start-game-button', Button);
    expect(startButtons).toHaveLength(0);

    const replayButtons = ngMocks.findInstances('#replay-game-button', Button);
    expect(replayButtons).toHaveLength(0);
  });
});
