import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { BlackjackGameManagementService } from '@services/blackjack-game-management.service';
import { faPlay, faRotateRight } from '@fortawesome/free-solid-svg-icons';
import { BET_FORM_CONTROLS_BY_PLAYER_NAME } from '@components/pages/blackjack-game-page/blackjack-game-page.component';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { DealCardsService } from '@services/deal-cards.service';
import { combineLatest, map } from 'rxjs';
import { GameInProgressService } from '@services/util/game-in-progress.service';

@Component({
  selector: 'app-start-replay-game-button',
  templateUrl: './start-replay-game-button.component.html',
  styleUrls: ['./start-replay-game-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StartReplayGameButtonComponent {
  readonly startGameIcon = faPlay;
  readonly replayIcon = faRotateRight;

  readonly gamePhase$ = this.blackjackGameManagementService.gamePhase$;
  readonly startAllowed$ = combineLatest([
    this.betFormControlsByPlayerName.valid$,
    this.dealCardsService.canDealCards$,
  ]).pipe(map(([validBets, canDealCards]) => validBets && canDealCards));

  constructor(
    private blackjackGameManagementService: BlackjackGameManagementService,
    @Inject(BET_FORM_CONTROLS_BY_PLAYER_NAME)
    private betFormControlsByPlayerName: FormGroup<
      Record<string, FormControl<number | null>>
    >,
    private dealCardsService: DealCardsService,
    private gameInProgressService: GameInProgressService,
  ) {}

  startGame() {
    this.gameInProgressService.newGame();
  }

  replayGame() {
    this.blackjackGameManagementService.resetGame();
  }
}
