import { ChangeDetectionStrategy, Component } from '@angular/core';
import { faFloppyDisk } from '@fortawesome/free-solid-svg-icons';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { Validators } from '@angular/forms';
import {
  PlayersService,
  STARTING_BALANCE,
} from '@services/config/players.service';
import { HumanPlayer } from '@model/player';
import { BlackjackGameManagementService } from '@services/blackjack-game-management.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-welcome-dialog',
  templateUrl: './welcome-dialog.component.html',
  styleUrls: ['./welcome-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WelcomeDialogComponent {
  readonly form = new FormGroup({
    name: new FormControl<string | null>(null, [
      Validators.required,
      Validators.pattern('.*\\S+.*'),
    ]),
  });

  protected readonly saveIcon = faFloppyDisk;

  constructor(
    private playersService: PlayersService,
    private blackjackGameManagementService: BlackjackGameManagementService,
    private dynamicDialogRef: DynamicDialogRef,
  ) {}

  savePlayer(event?: Event) {
    event?.preventDefault();
    if (this.form.invalid) {
      return;
    }
    const trimmedName = this.form.value.name?.trim() ?? null;
    if (!trimmedName) {
      return;
    }
    this.playersService.setPlayers([
      new HumanPlayer(trimmedName, STARTING_BALANCE),
    ]);
    this.blackjackGameManagementService.resetGame();
    this.dynamicDialogRef.close();
  }
}
