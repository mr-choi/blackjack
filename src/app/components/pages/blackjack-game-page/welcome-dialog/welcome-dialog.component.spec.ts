import { WelcomeDialogComponent } from './welcome-dialog.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { BlackjackGamePageModule } from '@components/pages/blackjack-game-page/blackjack-game-page.module';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import {
  PlayersService,
  STARTING_BALANCE,
} from '@services/config/players.service';
import { BlackjackGameManagementService } from '@services/blackjack-game-management.service';
import { Button } from 'primeng/button';
import { HumanPlayer } from '@model/player';
import { ChangeDetectorRef } from '@angular/core';
import { IfInvalidChangedTouchedDirective } from '@components/common/directives/if-invalid-changed-touched.directive';

describe('WelcomeDialogComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    MockInstance(PlayersService, 'setPlayers', jest.fn());
    MockInstance(BlackjackGameManagementService, 'resetGame', jest.fn());
    MockInstance(DynamicDialogRef, 'close', jest.fn());
    return MockBuilder(WelcomeDialogComponent, [
      BlackjackGamePageModule,
      DynamicDialogRef,
    ]).keep(IfInvalidChangedTouchedDirective);
  });

  it('should be able to save new human player with default balance', () => {
    MockRender(WelcomeDialogComponent);

    const nameFormFieldElement = ngMocks.find('#welcome-dialog-form-name');
    ngMocks.change(nameFormFieldElement, '  Player 1  ');

    const saveButton = ngMocks.findInstance(
      '#welcome-dialog-form-save',
      Button,
    );
    saveButton.onClick.emit();

    const playersService = ngMocks.get(PlayersService);
    const blackjackGameManagementService = ngMocks.get(
      BlackjackGameManagementService,
    );
    const dynamicDialogRef = ngMocks.get(DynamicDialogRef);

    expect(playersService.setPlayers).toHaveBeenCalledWith([
      new HumanPlayer('Player 1', STARTING_BALANCE),
    ]);
    expect(blackjackGameManagementService.resetGame).toHaveBeenCalled();
    expect(dynamicDialogRef.close).toHaveBeenCalled();
  });

  it('should not be able to save new human player if name is invalid', () => {
    const fixture = MockRender(WelcomeDialogComponent);
    const changeDetectorRef = ngMocks.get(fixture.point, ChangeDetectorRef);

    fixture.point.componentInstance.form.markAllAsDirty();
    fixture.point.componentInstance.form.markAllAsTouched();

    changeDetectorRef.markForCheck();
    fixture.detectChanges();

    let saveButton = ngMocks.findInstance('#welcome-dialog-form-save', Button);
    expect(saveButton.disabled).toBe(true);

    let hintElement = ngMocks.find('#welcome-dialog-hint');
    expect(ngMocks.formatText(hintElement)).toBe('This field is required.');

    const nameFormFieldElement = ngMocks.find('#welcome-dialog-form-name');
    ngMocks.change(nameFormFieldElement, '   ');

    changeDetectorRef.markForCheck();
    fixture.detectChanges();

    saveButton = ngMocks.findInstance('#welcome-dialog-form-save', Button);
    expect(saveButton.disabled).toBe(true);

    hintElement = ngMocks.find('#welcome-dialog-hint');
    expect(ngMocks.formatText(hintElement)).toBe(
      'An input with only spaces is not allowed.',
    );
  });
});
