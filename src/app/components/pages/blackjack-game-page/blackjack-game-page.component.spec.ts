import { BlackjackGamePageComponent } from './blackjack-game-page.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { BlackjackGamePageModule } from '@components/pages/blackjack-game-page/blackjack-game-page.module';
import { BlackjackGameManagementService } from '@services/blackjack-game-management.service';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { CPUPlayer, Player } from '@model/player';
import { Hand } from '@model/hand';
import { of } from 'rxjs';
import { DialogService } from 'primeng/dynamicdialog';

describe('BlackjackGamePageComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    MockInstance(DialogService, 'open', jest.fn());
    return MockBuilder(BlackjackGamePageComponent, [
      BlackjackGamePageModule,
      DialogService,
    ]).mock(BlackjackGameManagementService);
  });

  it('should start dialog if there are no players', () => {
    const bankLinkItem = new PlayerHandLinkItem(
      Player.BANK,
      new Hand(false),
      null,
    );
    MockInstance(
      BlackjackGameManagementService,
      'playerHandLink$',
      of(new PlayerHandLink(bankLinkItem, bankLinkItem)),
    );
    MockRender(BlackjackGamePageComponent);

    const dialogService = ngMocks.get(DialogService);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should no start dialog if there are players', () => {
    const bankLinkItem = new PlayerHandLinkItem(
      Player.BANK,
      new Hand(false),
      null,
    );
    const playerLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      new Hand(false),
      bankLinkItem,
    );
    MockInstance(
      BlackjackGameManagementService,
      'playerHandLink$',
      of(new PlayerHandLink(playerLinkItem, bankLinkItem)),
    );
    MockRender(BlackjackGamePageComponent);

    const dialogService = ngMocks.get(DialogService);
    expect(dialogService.open).not.toHaveBeenCalled();
  });
});
