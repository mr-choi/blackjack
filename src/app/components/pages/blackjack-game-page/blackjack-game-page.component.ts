import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  OnInit,
} from '@angular/core';
import { LeftButtonTopBar } from '@components/common/top-bar/top-bar.component';
import { faGear, faWarning } from '@fortawesome/free-solid-svg-icons';
import { BlackjackGameManagementService } from '@services/blackjack-game-management.service';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { DialogService } from 'primeng/dynamicdialog';
import { WelcomeDialogComponent } from '@components/pages/blackjack-game-page/welcome-dialog/welcome-dialog.component';
import { take } from 'rxjs';

export const BET_FORM_CONTROLS_BY_PLAYER_NAME = new InjectionToken<
  FormGroup<Record<string, FormControl<number | null>>>
>('Bet form controls by player');

@Component({
  selector: 'app-blackjack-game-page',
  templateUrl: './blackjack-game-page.component.html',
  styleUrls: ['./blackjack-game-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: BET_FORM_CONTROLS_BY_PLAYER_NAME,
      useValue: new FormGroup<Record<string, FormControl<number | null>>>({}),
    },
  ],
})
export class BlackjackGamePageComponent implements OnInit {
  readonly LEFT_BUTTON_TOP_BAR: LeftButtonTopBar = {
    label: 'Settings',
    icon: faGear,
    urlToNavigate: '/settings',
  };

  readonly playerHandLink$ =
    this.blackjackGameManagementService.playerHandLink$;

  constructor(
    private blackjackGameManagementService: BlackjackGameManagementService,
    private dialogService: DialogService,
  ) {}

  ngOnInit() {
    this.playerHandLink$.pipe(take(1)).subscribe((playerHandLink) => {
      if (playerHandLink.firstLinkItem === playerHandLink.bankLinkItem) {
        this.dialogService.open(WelcomeDialogComponent, {
          width: '720px',
          header: 'Welcome to blackjack!',
        });
      }
    });
  }

  protected readonly faWarning = faWarning;
}
