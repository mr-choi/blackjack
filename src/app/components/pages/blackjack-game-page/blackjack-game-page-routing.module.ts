import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlackjackGamePageComponent } from './blackjack-game-page.component';
import { GameInProgressService } from '@services/util/game-in-progress.service';

const routes: Routes = [
  {
    title: 'Blackjack',
    path: '',
    component: BlackjackGamePageComponent,
    canActivate: [() => inject(GameInProgressService).canActivate()],
    canDeactivate: [() => inject(GameInProgressService).canDeactivate()],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlackjackGamePageRoutingModule {}
