import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlackjackGamePlayerComponent } from './blackjack-game-player.component';
import { CardModule } from 'primeng/card';
import { CardsWithInfoComponent } from './cards-with-info/cards-with-info.component';
import { CardComponent } from '@components/pages/blackjack-game-page/blackjack-game-player/cards-with-info/card/card.component';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { BadgeModule } from 'primeng/badge';
import { BetInputComponent } from './bet-input/bet-input.component';
import { InputNumberModule } from 'primeng/inputnumber';
import { ReactiveFormsModule } from '@angular/forms';
import { MoveInputComponent } from './move-input/move-input.component';
import { ButtonModule } from 'primeng/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TooltipModule } from 'primeng/tooltip';
import { InsuranceInputComponent } from './insurance-input/insurance-input.component';
import { BlockUIModule } from 'primeng/blockui';

@NgModule({
  declarations: [
    BlackjackGamePlayerComponent,
    CardsWithInfoComponent,
    CardComponent,
    BetInputComponent,
    MoveInputComponent,
    InsuranceInputComponent,
  ],
  imports: [
    CommonModule,
    CardModule,
    ProgressSpinnerModule,
    BadgeModule,
    InputNumberModule,
    ReactiveFormsModule,
    ButtonModule,
    FontAwesomeModule,
    TooltipModule,
    BlockUIModule,
  ],
  exports: [BlackjackGamePlayerComponent],
})
export class BlackjackGamePlayerModule {}
