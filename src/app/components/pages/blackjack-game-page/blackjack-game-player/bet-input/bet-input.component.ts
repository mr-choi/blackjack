import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { HumanPlayer } from '@model/player';
import { BetsByPlayerService } from '@services/bets-by-player.service';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { BehaviorSubject, map, share, take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { BET_FORM_CONTROLS_BY_PLAYER_NAME } from '@components/pages/blackjack-game-page/blackjack-game-page.component';
import { Validators } from '@angular/forms';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';

@Component({
  selector: 'app-bet-input',
  templateUrl: './bet-input.component.html',
  styleUrls: ['./bet-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BetInputComponent implements OnInit, OnDestroy {
  private maxBet$$ = new BehaviorSubject(Number.MAX_SAFE_INTEGER);

  get maxBet() {
    return this.maxBet$$.getValue();
  }

  @Input({ required: true }) humanPlayer!: HumanPlayer;

  formControl!: FormControl<number | null>;

  readonly minimumBet = this.blackjackSettingsService.getSettings().minimumBet;

  constructor(
    private blackjackSettingsService: BlackjackSettingsService,
    private betsByPlayerService: BetsByPlayerService,
    private destroyRef: DestroyRef,
    @Inject(BET_FORM_CONTROLS_BY_PLAYER_NAME)
    public betFormControlsByPlayerName: FormGroup<
      Record<string, FormControl<number | null>>
    >,
  ) {}

  ngOnInit(): void {
    this.formControl =
      this.betFormControlsByPlayerName.get(this.humanPlayer.name) ??
      this.getNewFormControl();

    this.betsByPlayerService.betsByPLayer$
      .pipe(take(1))
      .subscribe((betsByPlayer) =>
        this.formControl.setValue(betsByPlayer.get(this.humanPlayer) ?? null, {
          emitEvent: false,
        }),
      );

    this.formControl.valueChanges
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((bet) => {
        this.betsByPlayerService.changeBetForPlayer(this.humanPlayer, bet);
      });

    this.humanPlayer.balance$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        map((balance) => Math.floor(balance)),
        share({ connector: () => this.maxBet$$ }),
      )
      .subscribe((maxBet) => {
        this.formControl.clearValidators();
        this.formControl.addValidators([
          Validators.required,
          Validators.min(this.minimumBet),
          Validators.max(maxBet),
        ]);
        this.formControl.updateValueAndValidity();
      });
  }

  ngOnDestroy() {
    this.betFormControlsByPlayerName.removeControl(this.humanPlayer.name);
  }

  private getNewFormControl(): FormControl<number | null> {
    const formControl = new FormControl<number | null>(null, [
      Validators.required,
      Validators.min(1),
    ]);
    formControl.markAsDirty(); // We want to show validation messages immediately.
    this.betFormControlsByPlayerName.addControl(
      this.humanPlayer.name,
      formControl,
    );
    return formControl;
  }
}
