import { BetInputComponent } from './bet-input.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { BlackjackGamePlayerModule } from '@components/pages/blackjack-game-page/blackjack-game-player/blackjack-game-player.module';
import { BetsByPlayerService } from '@services/bets-by-player.service';
import { of } from 'rxjs';
import { HumanPlayer } from '@model/player';
import { BET_FORM_CONTROLS_BY_PLAYER_NAME } from '@components/pages/blackjack-game-page/blackjack-game-page.component';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { BlackjackSettingsService } from '@services/config/blackjack-settings.service';
import { ChangeDetectorRef } from '@angular/core';

describe('BetInputComponent', () => {
  MockInstance.scope();

  let map: Map<HumanPlayer, number | null>;

  beforeEach(() => {
    map = new Map<HumanPlayer, number | null>();
    MockInstance(BetsByPlayerService, 'betsByPLayer$', of(map));
    MockInstance(BetsByPlayerService, 'changeBetForPlayer', jest.fn());
    MockInstance(
      BET_FORM_CONTROLS_BY_PLAYER_NAME,
      () => new FormGroup<Record<string, FormControl<number | null>>>({}),
    );
    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        minimumBet: 1,
      }),
    );
    return MockBuilder(BetInputComponent, BlackjackGamePlayerModule);
  });

  it('should initialize with correct starting bet', () => {
    const humanPlayer: HumanPlayer = new HumanPlayer('Player 1', 123.5);
    map.set(humanPlayer, 10);

    const fixture = MockRender(BetInputComponent, {
      humanPlayer: humanPlayer,
    });

    expect(fixture.point.componentInstance.formControl.value).toBe(10);
  });

  it('should save whenever the user changes the bet input', () => {
    const humanPlayer: HumanPlayer = new HumanPlayer('Player 1', 123.5);
    map.set(humanPlayer, null);

    MockRender(BetInputComponent, {
      humanPlayer: humanPlayer,
    });

    ngMocks.change(ngMocks.find('.bet-input'), 20);

    const betsByPlayerService = ngMocks.get(BetsByPlayerService);
    expect(betsByPlayerService.changeBetForPlayer).toHaveBeenLastCalledWith(
      humanPlayer,
      20,
    );
  });

  it('should not allow bets lower than the minimum bet', () => {
    const humanPlayer: HumanPlayer = new HumanPlayer('Player 1', 123.5);
    map.set(humanPlayer, null);

    MockInstance(
      BlackjackSettingsService,
      'getSettings',
      jest.fn().mockReturnValue({
        minimumBet: 10,
      }),
    );

    const fixture = MockRender(BetInputComponent, {
      humanPlayer: humanPlayer,
    });
    const changeDetectorRef = ngMocks.get(fixture.point, ChangeDetectorRef);

    ngMocks.change(ngMocks.find('.bet-input'), 5);

    changeDetectorRef.markForCheck();
    fixture.detectChanges();

    const hintElement = ngMocks.find('.form-field-hint');
    expect(ngMocks.formatText(hintElement)).toBe(
      'Fill in a bet of at least 10.',
    );
  });

  it("should not allow bets higher than the player's balance", () => {
    const humanPlayer: HumanPlayer = new HumanPlayer('Player 1', 123.5);
    map.set(humanPlayer, null);

    const fixture = MockRender(BetInputComponent, {
      humanPlayer: humanPlayer,
    });
    const changeDetectorRef = ngMocks.get(fixture.point, ChangeDetectorRef);

    ngMocks.change(ngMocks.find('.bet-input'), 500);

    changeDetectorRef.markForCheck();
    fixture.detectChanges();

    const hintElement = ngMocks.find('.form-field-hint');
    expect(ngMocks.formatText(hintElement)).toBe('Bet exceeds balance.');
  });
});
