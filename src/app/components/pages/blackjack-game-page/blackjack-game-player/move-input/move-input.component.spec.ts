import { MoveInputComponent } from './move-input.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { PlayerHandLinkItem } from '@model/player-hand-link';
import { HumanPlayer, Player } from '@model/player';
import { Hand } from '@model/hand';
import { HumanPlayerDetermineMoveObject } from '@model/human-player-determine-move-object';
import { Move } from '@model/move';
import { PlayerDetermineMoveService } from '@services/determine-move/player-determine-move.service';
import { of } from 'rxjs';
import { BlackjackGamePlayerModule } from '@components/pages/blackjack-game-page/blackjack-game-player/blackjack-game-player.module';

describe('MoveInputComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(MoveInputComponent, BlackjackGamePlayerModule);
  });

  it('should show all allowed moves if player hand link items match', () => {
    const playerHandLinkItem = new PlayerHandLinkItem(
      new HumanPlayer('Player 1', 1000),
      new Hand(false),
      null,
    );
    const determineMoveObject = new HumanPlayerDetermineMoveObject(
      playerHandLinkItem,
      new Set<Move>([
        Move.HIT,
        Move.STAND,
        Move.DOUBLE_DOWN,
        Move.SPLIT,
        Move.SURRENDER,
      ]),
    );
    const spy = jest.spyOn(determineMoveObject, 'select');

    MockInstance(
      PlayerDetermineMoveService,
      'determineMoveObject$',
      of(determineMoveObject),
    );
    MockRender(MoveInputComponent, { playerHandLinkItem: playerHandLinkItem });

    const moveButtons = ngMocks.findAll('.move-input-button');
    expect(moveButtons).toHaveLength(5);

    const hitButton = ngMocks.find('.move-input-hit');
    const standButton = ngMocks.find('.move-input-stand');
    const doubleDownButton = ngMocks.find('.move-input-double-down');
    const splitButton = ngMocks.find('.move-input-split');
    const surrenderButton = ngMocks.find('.move-input-surrender');

    ngMocks.click(hitButton);
    expect(spy).toHaveBeenLastCalledWith(Move.HIT);
    ngMocks.click(standButton);
    expect(spy).toHaveBeenLastCalledWith(Move.STAND);
    ngMocks.click(doubleDownButton);
    expect(spy).toHaveBeenLastCalledWith(Move.DOUBLE_DOWN);
    ngMocks.click(splitButton);
    expect(spy).toHaveBeenLastCalledWith(Move.SPLIT);
    ngMocks.click(surrenderButton);
    expect(spy).toHaveBeenLastCalledWith(Move.SURRENDER);
  });

  it('should not show if the link item in the determine move object is different', () => {
    const playerHandLinkItem = new PlayerHandLinkItem(
      new HumanPlayer('Player 1', 1000),
      new Hand(false),
      null,
    );
    const determineMoveObject = new HumanPlayerDetermineMoveObject(
      playerHandLinkItem,
      new Set<Move>([
        Move.HIT,
        Move.STAND,
        Move.DOUBLE_DOWN,
        Move.SPLIT,
        Move.SURRENDER,
      ]),
    );

    MockInstance(
      PlayerDetermineMoveService,
      'determineMoveObject$',
      of(determineMoveObject),
    );
    MockRender(MoveInputComponent, {
      playerHandLinkItem: new PlayerHandLinkItem(
        Player.BANK,
        new Hand(false),
        null,
      ),
    });

    const formActionsRow = ngMocks.findAll('.form-actions-row');
    expect(formActionsRow).toHaveLength(0);
  });
});
