import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { PlayerHandLinkItem } from '@model/player-hand-link';
import { PlayerDetermineMoveService } from '@services/determine-move/player-determine-move.service';
import {
  faCheck,
  faCheckDouble,
  faCodeMerge,
  faPlus,
  faX,
} from '@fortawesome/free-solid-svg-icons';
import { Move } from '@model/move';

@Component({
  selector: 'app-move-input',
  templateUrl: './move-input.component.html',
  styleUrls: ['./move-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoveInputComponent {
  @Input({ required: true }) playerHandLinkItem!: PlayerHandLinkItem;

  readonly determineMoveObject$ =
    this.playerDetermineMoveService.determineMoveObject$;

  constructor(private playerDetermineMoveService: PlayerDetermineMoveService) {}

  readonly hitIcon = faPlus;
  readonly standIcon = faCheck;
  readonly doubleDownIcon = faCheckDouble;
  readonly splitIcon = faCodeMerge;
  readonly surrenderIcon = faX;

  readonly Move = Move;
}
