import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  ElementRef,
  Input,
  OnInit,
} from '@angular/core';
import { PlayerHandLinkItem } from '@model/player-hand-link';
import { HumanPlayer, PlayerVisitor } from '@model/player';
import {
  combineLatest,
  distinctUntilChanged,
  filter,
  map,
  Observable,
  shareReplay,
} from 'rxjs';
import { BlackjackGameManagementService } from '@services/blackjack-game-management.service';
import { InsuranceService } from '@services/insurance.service';
import { Result } from '@model/result';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import animateScrollTo from 'animated-scroll-to';

export type BalanceAndBetInfo = {
  balance: number;
  bet: number;
};

@Component({
  selector: 'app-blackjack-game-player',
  templateUrl: './blackjack-game-player.component.html',
  styleUrls: ['./blackjack-game-player.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlackjackGamePlayerComponent
  implements OnInit, PlayerVisitor<void, void>
{
  @Input({ required: true }) playerHandLinkItem!: PlayerHandLinkItem;

  title!: string;
  possibleBalanceAndBetInfo$?: Observable<BalanceAndBetInfo>;
  possibleHumanPlayer?: HumanPlayer;

  readonly isBettingPhase$: Observable<boolean> =
    this.blackjackGameManagementService.gamePhase$.pipe(
      map((phase) => phase === 'BETTING'),
    );

  possibleHighlightClass$!: Observable<string | null>;

  resultWithSeverity$!: Observable<{
    result: Result | null;
    severity: 'success' | 'info' | 'danger';
  }>;

  constructor(
    private blackjackGameManagementService: BlackjackGameManagementService,
    private insuranceService: InsuranceService,
    private destroyRef: DestroyRef,
    private elementRef: ElementRef,
  ) {}

  ngOnInit(): void {
    this.playerHandLinkItem.getPlayer().accept(this, null);
    this.resultWithSeverity$ = this.playerHandLinkItem.result$.pipe(
      map((result) => ({
        result: result,
        severity:
          result === Result.WIN
            ? 'success'
            : result === Result.DRAW
            ? 'info'
            : 'danger',
      })),
    );

    this.possibleHighlightClass$ = combineLatest({
      currentLinkItem: this.blackjackGameManagementService.currentLinkItem$,
      determineInsuranceObject: this.insuranceService.determineInsuranceObject$,
      newCardPending: this.playerHandLinkItem.getHand().newCardPending$,
    }).pipe(
      map(({ currentLinkItem, determineInsuranceObject, newCardPending }) => {
        if (
          currentLinkItem === this.playerHandLinkItem ||
          determineInsuranceObject?.humanPlayer ===
            this.playerHandLinkItem.getPlayer() ||
          newCardPending
        ) {
          return 'active-link-item';
        }
        return null;
      }),
    );

    this.possibleHighlightClass$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        distinctUntilChanged(),
        filter((x) => !!x),
      )
      .subscribe(() => {
        animateScrollTo(this.elementRef.nativeElement, {
          cancelOnUserAction: false,
          verticalOffset: -100,
        });
      });
  }

  visitBank(): void {
    this.title = 'Bank';
  }

  visitCPU(): void {
    this.title = 'CPU';
  }

  visitHuman(humanPlayer: HumanPlayer): void {
    this.title = humanPlayer.name;
    this.possibleHumanPlayer = humanPlayer;
    this.possibleBalanceAndBetInfo$ = combineLatest({
      balance: humanPlayer.balance$,
      bet: this.playerHandLinkItem.bet$,
    }).pipe(shareReplay({ bufferSize: 1, refCount: true }));
  }
}
