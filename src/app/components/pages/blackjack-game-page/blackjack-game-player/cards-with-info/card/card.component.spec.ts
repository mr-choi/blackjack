import { CardComponent } from './card.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';

describe('CardComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(CardComponent);
  });

  it('should have correct background image and project content', () => {
    const url = 'image.png';
    const content = 'content';
    const fixture = MockRender<CardComponent>(
      `<app-card [backgroundUrl]="url">${content}</app-card>`,
      { url: url },
    );

    expect(fixture.point.nativeElement.style.backgroundImage).toBe(
      `url(${url})`,
    );
    expect(ngMocks.formatText(fixture.point)).toBe(content);
  });
});
