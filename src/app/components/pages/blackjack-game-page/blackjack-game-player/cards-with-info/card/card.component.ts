import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {
  @Input() set backgroundUrl(backgroundUrl: string) {
    this.elementRef.nativeElement.style.backgroundImage = `url(${backgroundUrl})`;
  }

  constructor(private elementRef: ElementRef) {}
}
