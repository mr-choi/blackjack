import {
  CardsWithInfoComponent,
  URL_CARD_BACKGROUND,
} from './cards-with-info.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { Hand, MINIMUM_DELAY_PENDING_CARD } from '@model/hand';
import { PlayCard } from '@model/play-card';
import { CardComponent } from '@components/pages/blackjack-game-page/blackjack-game-player/cards-with-info/card/card.component';
import { Badge } from 'primeng/badge';
import { BlackjackGamePlayerModule } from '@components/pages/blackjack-game-page/blackjack-game-player/blackjack-game-player.module';
import { of } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';
import { ProgressSpinner } from 'primeng/progressspinner';

describe('CardsWithInfoComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(CardsWithInfoComponent, BlackjackGamePlayerModule);
  });

  it('should display all cards with correct value', () => {
    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: '6H.png', value: '6' }));
    hand.addCard(PlayCard.createFromJson({ image: 'KS.png', value: 'KING' }));

    MockRender(CardsWithInfoComponent, {
      hand: hand,
    });

    const cardComponents = ngMocks.findInstances(
      ngMocks.find('.cards-container'),
      CardComponent,
    );
    expect(cardComponents).toHaveLength(2);
    expect(cardComponents[0].backgroundUrl).toBe('6H.png');
    expect(cardComponents[1].backgroundUrl).toBe('KS.png');

    const cardTotalBadge = ngMocks.findInstance('.card-total', Badge);
    expect(cardTotalBadge.value).toBe('16');
    expect(cardTotalBadge.severity).toBe('info');
  });

  it('should display hole card correctly', () => {
    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: '6H.png', value: '6' }));
    hand.addHoleCard(
      PlayCard.createFromJson({ image: 'KS.png', value: 'KING' }),
    );

    MockRender(CardsWithInfoComponent, {
      hand: hand,
    });

    const cardComponents = ngMocks.findInstances(
      ngMocks.find('.cards-container'),
      CardComponent,
    );
    expect(cardComponents).toHaveLength(2);
    expect(cardComponents[0].backgroundUrl).toBe('6H.png');
    expect(cardComponents[1].backgroundUrl).toBe(URL_CARD_BACKGROUND);

    expect(
      ngMocks.findInstances(ngMocks.find('.cards-container'), ProgressSpinner),
    ).toHaveLength(0);

    const cardTotalBadge = ngMocks.findInstance('.card-total', Badge);
    expect(cardTotalBadge.value).toBe('6');
    expect(cardTotalBadge.severity).toBe('info');
  });

  it('should display correctly when drawing first card', fakeAsync(() => {
    const hand = new Hand(false);
    of(PlayCard.createFromJson({ image: '6H.png', value: '6' }))
      .pipe(hand.addDrawnCardToHand())
      .subscribe();

    const fixture = MockRender(CardsWithInfoComponent, {
      hand: hand,
    });

    const cardComponents = ngMocks.findInstances(
      ngMocks.find('.cards-container'),
      CardComponent,
    );
    expect(cardComponents).toHaveLength(1);
    expect(cardComponents[0].backgroundUrl).toBe(URL_CARD_BACKGROUND);

    ngMocks.findInstance(ngMocks.find('.cards-container'), ProgressSpinner);

    const cardTotalBadges = ngMocks.findInstances('.card-total', Badge);
    expect(cardTotalBadges).toHaveLength(0);

    tick(MINIMUM_DELAY_PENDING_CARD);
    fixture.detectChanges();

    const newCardComponents = ngMocks.findInstances(
      ngMocks.find('.cards-container'),
      CardComponent,
    );
    expect(newCardComponents).toHaveLength(1);
    expect(newCardComponents[0].backgroundUrl).toBe('6H.png');

    expect(
      ngMocks.findInstances(ngMocks.find('.cards-container'), ProgressSpinner),
    ).toHaveLength(0);

    const cardTotalBadge = ngMocks.findInstance('.card-total', Badge);
    expect(cardTotalBadge.value).toBe('6');
    expect(cardTotalBadge.severity).toBe('info');
  }));

  it('should display correctly for blackjack hand', () => {
    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: 'AH.png', value: 'ACE' }));
    hand.addCard(PlayCard.createFromJson({ image: 'KS.png', value: 'KING' }));

    MockRender(CardsWithInfoComponent, {
      hand: hand,
    });

    const cardTotalBadge = ngMocks.findInstance('.card-total', Badge);
    expect(cardTotalBadge.value).toBe('BLACKJACK!');
    expect(cardTotalBadge.severity).toBe('success');
  });

  it('should display correctly for busted hand', () => {
    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: '6H.png', value: '6' }));
    hand.addCard(PlayCard.createFromJson({ image: 'KS.png', value: 'KING' }));
    hand.addCard(PlayCard.createFromJson({ image: '9D.png', value: '9' }));

    MockRender(CardsWithInfoComponent, {
      hand: hand,
    });

    const cardTotalBadge = ngMocks.findInstance('.card-total', Badge);
    expect(cardTotalBadge.value).toBe('BUSTED! (25)');
    expect(cardTotalBadge.severity).toBe('danger');
  });

  it('should display correctly for soft totals', () => {
    const hand = new Hand(false);
    hand.addCard(PlayCard.createFromJson({ image: 'AH.png', value: 'ACE' }));
    hand.addCard(PlayCard.createFromJson({ image: '4S.png', value: '4' }));

    MockRender(CardsWithInfoComponent, {
      hand: hand,
    });

    const cardTotalBadge = ngMocks.findInstance('.card-total', Badge);
    expect(cardTotalBadge.value).toBe('15 | 5');
    expect(cardTotalBadge.severity).toBe('info');
  });
});
