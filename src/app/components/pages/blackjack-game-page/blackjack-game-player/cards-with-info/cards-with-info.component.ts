import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
} from '@angular/core';
import { Hand } from '@model/hand';
import { combineLatest, map, Observable } from 'rxjs';
import { PlayCard } from '@model/play-card';
import { BlockableUI } from 'primeng/api';

export const URL_CARD_BACKGROUND =
  'https://deckofcardsapi.com/static/img/back.png';

@Component({
  selector: 'app-cards-with-info',
  templateUrl: './cards-with-info.component.html',
  styleUrls: ['./cards-with-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardsWithInfoComponent implements BlockableUI {
  private _hand!: Hand;

  handData$!: Observable<{
    cards: PlayCard[];
    hasHoleCard: boolean;
    newCardPending: boolean;
    totalCardValueToDisplay: string;
    totalCardValueSeverity: 'info' | 'danger' | 'success';
  }>;

  readonly URL_CARD_BACKGROUND = URL_CARD_BACKGROUND;

  @Input({ required: true }) set hand(hand: Hand | null) {
    if (!hand) {
      hand = new Hand(false);
    }
    this._hand = hand;

    this.handData$ = combineLatest({
      cards: hand.cards$,
      hasHoleCard: hand.hasHoleCard$,
      newCardPending: hand.newCardPending$,
      totalCardValueToDisplay: hand.totalCardValue$.pipe(
        map((totalCardValue) =>
          this._hand.isBlackjack()
            ? 'BLACKJACK!'
            : totalCardValue.isSoft()
            ? `${totalCardValue.getTotal()} | ${totalCardValue.getTotal() - 10}`
            : totalCardValue.isBusted()
            ? `BUSTED! (${totalCardValue.getTotal()})`
            : `${totalCardValue.getTotal()}`,
        ),
      ),
      totalCardValueSeverity: hand.totalCardValue$.pipe(
        map((totalCardValue) =>
          totalCardValue.isBusted()
            ? 'danger'
            : this._hand.isBlackjack()
            ? 'success'
            : 'info',
        ),
      ),
    });
  }
  get hand() {
    return this._hand;
  }

  constructor(private elementRef: ElementRef) {}

  getBlockableElement(): HTMLElement {
    return this.elementRef.nativeElement;
  }
}
