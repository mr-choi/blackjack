import { BlackjackGamePlayerComponent } from './blackjack-game-player.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { BlackjackGamePlayerModule } from '@components/pages/blackjack-game-page/blackjack-game-player/blackjack-game-player.module';
import { PlayerHandLinkItem } from '@model/player-hand-link';
import { CPUPlayer, HumanPlayer, Player } from '@model/player';
import { Hand } from '@model/hand';
import { Card } from 'primeng/card';
import { PlayCard } from '@model/play-card';
import {
  BlackjackGameManagementService,
  GamePhase,
} from '@services/blackjack-game-management.service';
import { of } from 'rxjs';
import { BetInputComponent } from '@components/pages/blackjack-game-page/blackjack-game-player/bet-input/bet-input.component';
import { InsuranceService } from '@services/insurance.service';
import { HumanPlayerDetermineInsuranceObject } from '@model/human-player-determine-insurance-object';
import { Result } from '@model/result';
import { BlockUI } from 'primeng/blockui';
import { Badge } from 'primeng/badge';

describe('BlackjackGamePlayerComponent', () => {
  MockInstance.scope();

  let animateScrollSpy: jest.SpyInstance;

  beforeEach(() => {
    const animateScrollTo = jest.requireActual('animated-scroll-to');
    animateScrollSpy = jest.spyOn(animateScrollTo, 'default');
    MockInstance(
      BlackjackGameManagementService,
      'gamePhase$',
      of<GamePhase>('BETTING'),
    );
    MockInstance(BlackjackGameManagementService, 'currentLinkItem$', of(null));
    MockInstance(InsuranceService, 'determineInsuranceObject$', of(null));
    return MockBuilder(BlackjackGamePlayerComponent, [
      BlackjackGamePlayerModule,
      BlackjackGameManagementService,
    ]);
  });

  it('should create correctly for bank', () => {
    const playerHandLinkItem = new PlayerHandLinkItem(
      Player.BANK,
      new Hand(false),
      null,
    );

    MockRender(BlackjackGamePlayerComponent, {
      playerHandLinkItem: playerHandLinkItem,
    });

    const cardComponent = ngMocks.findInstance(Card);
    expect(cardComponent.header).toBe('Bank');
  });

  it('should create correctly for CPU', () => {
    const playerHandLinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      new Hand(false),
      null,
    );

    MockRender(BlackjackGamePlayerComponent, {
      playerHandLinkItem: playerHandLinkItem,
    });

    const cardComponent = ngMocks.findInstance(Card);
    expect(cardComponent.header).toBe('CPU');
  });

  it('should create correctly for human player for betting phase', () => {
    const playerHandLinkItem = new PlayerHandLinkItem(
      new HumanPlayer('Player 1', 123),
      new Hand(false),
      null,
    );
    playerHandLinkItem.setBet(10);

    MockRender(BlackjackGamePlayerComponent, {
      playerHandLinkItem: playerHandLinkItem,
    });

    const cardComponent = ngMocks.findInstance(Card);
    expect(cardComponent.header).toBe('Player 1');
    expect(cardComponent.styleClass).toBeUndefined();

    ngMocks.render(
      cardComponent,
      ngMocks.findTemplateRef(ngMocks.find(Card), ['pTemplate', 'subtitle']),
    );
    ngMocks.render(
      cardComponent,
      ngMocks.findTemplateRef(ngMocks.find(Card), ['pTemplate', 'footer']),
    );

    const betInfo = ngMocks.find('.bet-info');
    expect(ngMocks.formatText(betInfo)).toBe('Bet: 10');

    const balanceInfo = ngMocks.find('.balance-info');
    expect(ngMocks.formatText(balanceInfo)).toBe('Balance: 123');

    const betInput = ngMocks.findInstance(BetInputComponent);
    expect(betInput.humanPlayer).toBe(playerHandLinkItem.getPlayer());
  });

  it('should highlight human player if it is his turn', () => {
    const playerHandLinkItem = new PlayerHandLinkItem(
      new HumanPlayer('Player 1', 123),
      new Hand(false),
      null,
    );
    MockInstance(
      BlackjackGameManagementService,
      'currentLinkItem$',
      of(playerHandLinkItem),
    );
    MockRender(BlackjackGamePlayerComponent, {
      playerHandLinkItem: playerHandLinkItem,
    });

    const cardComponent = ngMocks.findInstance(Card);
    expect(cardComponent.styleClass).toBe('active-link-item');
    expect(animateScrollSpy).toHaveBeenCalled();
  });

  it('should highlight human player if asked for insurance', () => {
    const humanPlayer = new HumanPlayer('Player 1', 123);
    const playerHandLinkItem = new PlayerHandLinkItem(
      humanPlayer,
      new Hand(false),
      null,
    );
    MockInstance(
      InsuranceService,
      'determineInsuranceObject$',
      of(new HumanPlayerDetermineInsuranceObject(humanPlayer)),
    );
    MockRender(BlackjackGamePlayerComponent, {
      playerHandLinkItem: playerHandLinkItem,
    });

    const cardComponent = ngMocks.findInstance(Card);
    expect(cardComponent.styleClass).toBe('active-link-item');
    expect(animateScrollSpy).toHaveBeenCalled();
  });

  it('should highlight player if it is drawing a card', () => {
    const humanPlayer = new HumanPlayer('Player 1', 123);
    const playerHandLinkItem = new PlayerHandLinkItem(
      humanPlayer,
      new Hand(false),
      null,
    );
    of(PlayCard.createFromJson({ image: '2', value: '2' }))
      .pipe(playerHandLinkItem.getHand().addDrawnCardToHand())
      .subscribe();
    MockInstance(
      InsuranceService,
      'determineInsuranceObject$',
      of(new HumanPlayerDetermineInsuranceObject(humanPlayer)),
    );
    MockRender(BlackjackGamePlayerComponent, {
      playerHandLinkItem: playerHandLinkItem,
    });

    const cardComponent = ngMocks.findInstance(Card);
    expect(cardComponent.styleClass).toBe('active-link-item');
    expect(animateScrollSpy).toHaveBeenCalled();
  });

  it('should show block ui with result if available', () => {
    const player = new CPUPlayer();
    const playerHandLinkItem = new PlayerHandLinkItem(
      player,
      new Hand(false),
      null,
    );
    playerHandLinkItem.setResult(Result.WIN);

    const fixture = MockRender(BlackjackGamePlayerComponent, {
      playerHandLinkItem: playerHandLinkItem,
    });

    const blockUIElement = ngMocks.find(BlockUI);

    let badge = ngMocks.findInstance(blockUIElement, Badge);
    expect(badge.value).toBe(Result.WIN.message);
    expect(badge.severity).toBe('success');

    playerHandLinkItem.setResult(Result.DRAW);
    fixture.detectChanges();

    badge = ngMocks.findInstance(blockUIElement, Badge);
    expect(badge.value).toBe(Result.DRAW.message);
    expect(badge.severity).toBe('info');

    playerHandLinkItem.setResult(Result.LOSE);
    fixture.detectChanges();

    badge = ngMocks.findInstance(blockUIElement, Badge);
    expect(badge.value).toBe(Result.LOSE.message);
    expect(badge.severity).toBe('danger');
  });
});
