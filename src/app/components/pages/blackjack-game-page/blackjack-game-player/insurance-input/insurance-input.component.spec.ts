import { InsuranceInputComponent } from './insurance-input.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { BlackjackGamePlayerModule } from '@components/pages/blackjack-game-page/blackjack-game-player/blackjack-game-player.module';
import { InsuranceService } from '@services/insurance.service';
import { of } from 'rxjs';
import { HumanPlayerDetermineInsuranceObject } from '@model/human-player-determine-insurance-object';
import { HumanPlayer } from '@model/player';
import { Button } from 'primeng/button';

describe('InsuranceInputComponent', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(InsuranceInputComponent, BlackjackGamePlayerModule);
  });

  it('should be possible to select yes or no when asked for insurance', () => {
    const humanPlayer = new HumanPlayer('Player 1', 1000);
    const determineInsuranceObject = new HumanPlayerDetermineInsuranceObject(
      humanPlayer,
    );

    const spy = jest.spyOn(determineInsuranceObject, 'setWantsInsurance');
    MockInstance(
      InsuranceService,
      'determineInsuranceObject$',
      of(determineInsuranceObject),
    );
    MockRender(InsuranceInputComponent, { humanPlayer: humanPlayer });

    const yesButton = ngMocks.findInstance(
      '.insurance-input-yes-button',
      Button,
    );
    yesButton.onClick.emit();
    expect(spy).toHaveBeenLastCalledWith(true);

    const noButton = ngMocks.findInstance('.insurance-input-no-button', Button);
    noButton.onClick.emit();
    expect(spy).toHaveBeenLastCalledWith(false);
  });

  it('should not show insurance prompt if meant for other player', () => {
    const humanPlayer = new HumanPlayer('Player 1', 1000);
    const determineInsuranceObject = new HumanPlayerDetermineInsuranceObject(
      humanPlayer,
    );

    MockInstance(
      InsuranceService,
      'determineInsuranceObject$',
      of(determineInsuranceObject),
    );
    MockRender(InsuranceInputComponent, {
      humanPlayer: new HumanPlayer('Player 2', 1000),
    });

    expect(ngMocks.findAll('.form-field-row')).toHaveLength(0);
  });
});
