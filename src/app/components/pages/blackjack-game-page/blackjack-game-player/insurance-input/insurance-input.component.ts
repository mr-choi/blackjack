import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { InsuranceService } from '@services/insurance.service';
import { faCheck, faX } from '@fortawesome/free-solid-svg-icons';
import { HumanPlayer } from '@model/player';

@Component({
  selector: 'app-insurance-input',
  templateUrl: './insurance-input.component.html',
  styleUrls: ['./insurance-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InsuranceInputComponent {
  @Input({ required: true }) humanPlayer!: HumanPlayer;

  readonly determineInsuranceObject$ =
    this.insuranceService.determineInsuranceObject$;

  constructor(private insuranceService: InsuranceService) {}

  readonly yesIcon = faCheck;
  readonly noIcon = faX;
}
