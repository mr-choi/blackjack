import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlackjackGamePageRoutingModule } from './blackjack-game-page-routing.module';
import { BlackjackGamePageComponent } from './blackjack-game-page.component';
import { TopBarComponent } from '@components/common/top-bar/top-bar.component';
import { CardModule } from 'primeng/card';
import { ForEachPlayerHandLinkItemDirective } from '@components/common/directives/for-each-player-hand-link-item.directive';
import { BlackjackGamePlayerModule } from './blackjack-game-player/blackjack-game-player.module';
import { StartReplayGameButtonComponent } from './start-restart-game-button/start-replay-game-button.component';
import { ButtonModule } from 'primeng/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GameInProgressService } from '@services/util/game-in-progress.service';
import { WelcomeDialogComponent } from './welcome-dialog/welcome-dialog.component';
import { DropdownModule } from 'primeng/dropdown';
import { IfInvalidChangedTouchedDirective } from '@components/common/directives/if-invalid-changed-touched.directive';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';

@NgModule({
  declarations: [
    BlackjackGamePageComponent,
    StartReplayGameButtonComponent,
    WelcomeDialogComponent,
  ],
  imports: [
    CommonModule,
    BlackjackGamePageRoutingModule,
    TopBarComponent,
    CardModule,
    ForEachPlayerHandLinkItemDirective,
    BlackjackGamePlayerModule,
    ButtonModule,
    FontAwesomeModule,
    DropdownModule,
    IfInvalidChangedTouchedDirective,
    InputNumberModule,
    InputTextModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    ToastModule,
  ],
  providers: [
    GameInProgressService,
    MessageService,
    ConfirmationService,
    DialogService,
  ],
})
export class BlackjackGamePageModule {}
