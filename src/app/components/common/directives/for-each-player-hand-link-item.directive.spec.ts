import { ForEachPlayerHandLinkItemDirective } from './for-each-player-hand-link-item.directive';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { NgFor } from '@angular/common';
import { CPUPlayer, HumanPlayer, Player } from '@model/player';
import { Hand } from '@model/hand';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';

describe('ForEachPlayerHandLinkItemDirective', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(ForEachPlayerHandLinkItemDirective).keep(NgFor);
  });

  it('should iterate over player hand link items, putting the bank in front, and update when new hand is inserted', () => {
    const bankLinkItem = new PlayerHandLinkItem(
      Player.BANK,
      new Hand(false),
      null,
    );
    const player2LinkItem = new PlayerHandLinkItem(
      new CPUPlayer(),
      new Hand(false),
      bankLinkItem,
    );
    const player1LinkItem = new PlayerHandLinkItem(
      new HumanPlayer('Player 1', 123),
      new Hand(false),
      player2LinkItem,
    );
    const playerHandLink = new PlayerHandLink(player1LinkItem, bankLinkItem);

    const fixture = MockRender(
      '<span *appForEachPlayerHandLinkItem="let item of playerHandLink">{{ item.getPlayer().type }}</span>',
      {
        playerHandLink: playerHandLink,
      },
    );

    let items = ngMocks
      .findAll('span')
      .map((element) => ngMocks.formatText(element));
    expect(items).toEqual(['bank', 'human', 'cpu']);

    player1LinkItem.insertNewHand(new Hand(true));
    fixture.detectChanges();

    items = ngMocks
      .findAll('span')
      .map((element) => ngMocks.formatText(element));
    expect(items).toEqual(['bank', 'human', 'human', 'cpu']);
  });
});
