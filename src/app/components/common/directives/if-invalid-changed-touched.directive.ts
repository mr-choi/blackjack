import { DestroyRef, Directive, Input } from '@angular/core';
import { NgIf } from '@angular/common';
import {
  combineLatest,
  distinctUntilChanged,
  map,
  startWith,
  Subscription,
} from 'rxjs';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Directive({
  selector: '[appIfInvalidChangedTouched]',
  standalone: true,
  hostDirectives: [
    {
      directive: NgIf,
      inputs: [
        'ngIfThen: appIfInvalidChangedTouchedThen',
        'ngIfElse: appIfInvalidChangedTouchedElse',
      ],
    },
  ],
})
export class IfInvalidChangedTouchedDirective {
  private currentControlSubscription?: Subscription;

  @Input() set appIfInvalidChangedTouched(
    control: FormControl<unknown> | FormGroup<Record<string, unknown>>,
  ) {
    if (this.currentControlSubscription) {
      this.currentControlSubscription.unsubscribe();
    }
    this.currentControlSubscription = combineLatest({
      invalid: control.invalid$.pipe(startWith(control.invalid)),
      dirty: control.dirty$.pipe(startWith(control.dirty)),
      touched: control.touch$.pipe(startWith(control.touched)),
    })
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        map((state) => state.invalid && state.dirty && state.touched),
        distinctUntilChanged(),
      )
      .subscribe((show) => (this.ngIf.ngIf = show));
  }

  constructor(
    private ngIf: NgIf,
    private destroyRef: DestroyRef,
  ) {}
}
