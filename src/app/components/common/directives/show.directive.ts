import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appShow]',
  standalone: true,
})
export class ShowDirective {
  @Input() appShow = true;
  @HostBinding('style.display') get display() {
    return this.appShow ? null : 'none';
  }
}
