import { IfInvalidChangedTouchedDirective } from './if-invalid-changed-touched.directive';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { FormControl } from '@ngneat/reactive-forms';
import { Validators } from '@angular/forms';
import { NgIf } from '@angular/common';

describe('IfInvalidChangedTouchedDirective', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(IfInvalidChangedTouchedDirective).keep(NgIf);
  });

  it('should only show if control is invalid, dirty, and touched', () => {
    const control = new FormControl<string>('', Validators.required);
    const fixture = MockRender(
      '<ng-container *appIfInvalidChangedTouched="control">visible</ng-container>',
      {
        control: control,
      },
    );

    expect(ngMocks.formatText(fixture.point)).toBe('');

    control.markAllAsTouched();
    expect(ngMocks.formatText(fixture.point)).toBe('');

    control.markAsDirty();
    expect(ngMocks.formatText(fixture.point)).toBe('visible');

    control.setValue('valid');
    expect(ngMocks.formatText(fixture.point)).toBe('');
  });
});
