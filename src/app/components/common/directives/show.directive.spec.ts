import { ShowDirective } from './show.directive';
import { MockBuilder, MockRender, ngMocks } from 'ng-mocks';

describe('ShowDirective', () => {
  beforeEach(() => {
    return MockBuilder(ShowDirective);
  });

  it('should not display if set to false', () => {
    const params = { show: true };
    const fixture = MockRender<ShowDirective>(
      '<span [appShow]="show">Text</span>',
      params,
    );
    const showDirective = ngMocks.findInstance(ShowDirective);
    expect(showDirective.display).toBeNull();

    params.show = false;
    fixture.detectChanges();
    expect(showDirective.display).toBe('none');
  });
});
