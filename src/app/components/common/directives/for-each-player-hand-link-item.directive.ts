import { DestroyRef, Directive, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { PlayerHandLink, PlayerHandLinkItem } from '@model/player-hand-link';
import { take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Directive({
  selector: '[appForEachPlayerHandLinkItem][appForEachPlayerHandLinkItemOf]',
  standalone: true,
  hostDirectives: [NgFor],
})
export class ForEachPlayerHandLinkItemDirective {
  @Input() set appForEachPlayerHandLinkItemOf(
    playerHandLink: PlayerHandLink | null,
  ) {
    if (!playerHandLink) {
      return;
    }
    // The bank will be put in front.
    const playerHandLinkItemArray: PlayerHandLinkItem[] = [
      playerHandLink.bankLinkItem,
    ];
    for (
      let item: PlayerHandLinkItem | null = playerHandLink.firstLinkItem;
      item?.getNext();
      item = item.getNext()
    ) {
      playerHandLinkItemArray.push(item);
    }
    this.ngFor.ngForOf = playerHandLinkItemArray;

    playerHandLink.newHandInserted$
      .pipe(takeUntilDestroyed(this.destroyRef), take(1))
      .subscribe(() => (this.appForEachPlayerHandLinkItemOf = playerHandLink));
  }

  constructor(
    private ngFor: NgFor<PlayerHandLinkItem>,
    private destroyRef: DestroyRef,
  ) {}
}
