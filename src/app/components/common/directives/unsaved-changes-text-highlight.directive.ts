import {
  DestroyRef,
  Directive,
  HostBinding,
  Input,
  OnInit,
} from '@angular/core';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { filter, startWith } from 'rxjs';

@Directive({
  selector: '[appUnsavedChangesTextHighlight]',
  standalone: true,
})
export class UnsavedChangesTextHighlightDirective implements OnInit {
  @HostBinding('class.text-primary')
  @HostBinding('class.underline')
  get shouldHighlight(): boolean {
    return (
      !!this.appUnsavedChangesTextHighlight &&
      this.appUnsavedChangesTextHighlight.value !== this.lastPristineValue
    );
  }

  @Input()
  appUnsavedChangesTextHighlight?:
    | FormControl<unknown>
    | FormGroup<Record<string, unknown>>;

  lastPristineValue?: unknown;

  constructor(private destroyRef: DestroyRef) {}

  ngOnInit(): void {
    this.appUnsavedChangesTextHighlight?.dirty$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        filter((dirty) => !dirty),
        startWith(false),
      )
      .subscribe(() => {
        this.lastPristineValue = this.appUnsavedChangesTextHighlight?.value;
      });
  }
}
