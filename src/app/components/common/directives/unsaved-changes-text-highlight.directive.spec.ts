import { UnsavedChangesTextHighlightDirective } from './unsaved-changes-text-highlight.directive';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { FormControl } from '@ngneat/reactive-forms';

describe('UnsavedChangesTextHighlightDirective', () => {
  MockInstance.scope();

  beforeEach(() => {
    return MockBuilder(UnsavedChangesTextHighlightDirective);
  });

  it('should highlight if text is changed from initial pristine value', () => {
    const formControl = new FormControl<number>(1);
    MockRender<UnsavedChangesTextHighlightDirective>(
      `<span [appUnsavedChangesTextHighlight]="control"></span>`,
      { control: formControl },
    );
    const unsavedChangesTextHighlightDirective = ngMocks.findInstance(
      UnsavedChangesTextHighlightDirective,
    );

    expect(unsavedChangesTextHighlightDirective.shouldHighlight).toBe(false);

    formControl.setValue(2);
    formControl.markAsDirty();
    expect(unsavedChangesTextHighlightDirective.shouldHighlight).toBe(true);

    formControl.markAsPristine();
    expect(unsavedChangesTextHighlightDirective.shouldHighlight).toBe(false);
  });
});
