import { LeftButtonTopBar, TopBarComponent } from './top-bar.component';
import { MockBuilder, MockInstance, MockRender, ngMocks } from 'ng-mocks';
import { ActivatedRoute, Router } from '@angular/router';
import { Button } from 'primeng/button';
import { RouterTestingModule } from '@angular/router/testing';
import { EMPTY, of } from 'rxjs';

describe('TopBarComponent', () => {
  MockInstance.scope();

  let pageTitle: string;

  beforeEach(() => {
    pageTitle = 'Blackjack - Page';

    MockInstance(Router, 'navigateByUrl', jest.fn());
    MockInstance(Router, 'events', EMPTY);
    MockInstance(ActivatedRoute, 'title', of(pageTitle));
    return MockBuilder(TopBarComponent, RouterTestingModule);
  });

  it('should have no left button if not provided', () => {
    MockRender(TopBarComponent);
    const buttons = ngMocks.findAll('#top-bar-left-button');
    expect(buttons).toHaveLength(0);
  });

  it('should have the title of the route', () => {
    MockRender(TopBarComponent);
    const topBarTitleSpanTag = ngMocks.find('div.p-toolbar-group-center span');
    expect(ngMocks.formatText(topBarTitleSpanTag)).toBe(pageTitle);
  });

  it('should have button that navigates to correct page if provided', () => {
    const leftButtonTopBar: LeftButtonTopBar = {
      label: 'Label',
      urlToNavigate: '/path',
    };

    MockRender(TopBarComponent, { possibleLeftButton: leftButtonTopBar });
    const buttonElement = ngMocks.find<Button>('#top-bar-left-button');
    const labelOfButtonElement = ngMocks.find(buttonElement, 'span');

    expect(ngMocks.formatText(labelOfButtonElement)).toBe(
      leftButtonTopBar.label,
    );

    const router = ngMocks.get(Router);
    buttonElement.componentInstance.onClick.emit();
    expect(router.navigateByUrl).toHaveBeenCalledWith(
      leftButtonTopBar.urlToNavigate,
    );
  });
});
