import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationSkipped,
  NavigationStart,
  Router,
} from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { filter, map, Observable } from 'rxjs';
import { ProgressBarModule } from 'primeng/progressbar';

export type LeftButtonTopBar = {
  label: string;
  icon?: IconDefinition;
  urlToNavigate: string;
};

@Component({
  selector: 'app-top-bar',
  standalone: true,
  imports: [
    CommonModule,
    ToolbarModule,
    ButtonModule,
    FontAwesomeModule,
    ProgressBarModule,
  ],
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBarComponent {
  title$: Observable<string | undefined> = this.activatedRoute.title.pipe(
    map((possibleTitle) => possibleTitle ?? 'Blackjack'),
  );

  loadingRoute$: Observable<boolean> = this.router.events.pipe(
    filter(
      (event) =>
        event instanceof NavigationStart ||
        event instanceof NavigationEnd ||
        event instanceof NavigationCancel ||
        event instanceof NavigationSkipped ||
        event instanceof NavigationError,
    ),
    map((event) => event instanceof NavigationStart),
  );

  @Input() possibleLeftButton?: LeftButtonTopBar;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {}

  navigate() {
    if (this.possibleLeftButton?.urlToNavigate)
      this.router.navigateByUrl(this.possibleLeftButton.urlToNavigate);
  }
}
