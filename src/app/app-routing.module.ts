import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () =>
      import(
        './components/pages/blackjack-game-page/blackjack-game-page.module'
      ).then((m) => m.BlackjackGamePageModule),
  },
  {
    path: 'settings',
    loadChildren: () =>
      import(
        './components/pages/blackjack-settings-page/blackjack-settings-page.module'
      ).then((m) => m.BlackjackSettingsPageModule),
  },
  {
    path: '**',
    redirectTo: '/',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
