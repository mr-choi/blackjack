import { RunHelpers, TestScheduler } from 'rxjs/internal/testing/TestScheduler';

export function runMarbleTest<T>(callback: (helpers: RunHelpers) => T): T {
  return new TestScheduler((actual, expected) =>
    expect(actual).toEqual(expected),
  ).run(callback);
}
