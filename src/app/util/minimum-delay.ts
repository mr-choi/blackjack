import {
  asyncScheduler,
  delayWhen,
  MonoTypeOperatorFunction,
  pipe,
  tap,
  timer,
} from 'rxjs';

/**
 * Delays emissions at least the provided amount of milliseconds since subscription.
 * @param minDelay the minimum delay in milliseconds
 */
export function minimumDelay<T>(minDelay: number): MonoTypeOperatorFunction<T> {
  let startTime: number;
  return pipe(
    delayWhen(() =>
      timer(
        Math.max(0, minDelay + startTime - asyncScheduler.now()),
        asyncScheduler,
      ),
    ),
    tap({ subscribe: () => (startTime = asyncScheduler.now()) }),
  );
}
